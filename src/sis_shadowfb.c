/*
 * SiS driver internal shadowfb subsystem for RandR rotation support
 *
 * Based on shadow.c which is
 * Copyright (C) 1999.  The XFree86 Project Inc.
 * Written by Mark Vojkovich (mvojkovi@ucsd.edu)
 * RENDER support - Nolan Leake (nolan@vmware.com)
 *
 * Adapted for sis driver specific needs by Thomas Winischhofer
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sis.h"

#ifdef SISISXORGPOST17
#include <xorg/privates.h>
#endif

#ifdef SIS_INCL_RRROT

#include <X11/X.h>
#include <X11/Xproto.h>
#include "misc.h"
#include "pixmapstr.h"
#ifdef SISISXORG6899900
#include <X11/fonts/font.h>
#else
#include "font.h"
#endif
#include "mi.h"
#include "scrnintstr.h"
#include "windowstr.h"
#include "gcstruct.h"
#include "dixfontstr.h"
#ifdef SISISXORG6899900
#include <X11/fonts/fontstruct.h>
#else
#include "fontstruct.h"
#endif
#include "xf86str.h"

#ifdef RENDER
# include "picturestr.h"
#endif

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#if 0
#define SHDEBUG(x) x
#else
#define SHDEBUG(x)
#endif

static void SiSShadowRestoreAreas(
    PixmapPtr pPixmap,
    RegionPtr prgn,
    int       xorg,
    int       yorg,
    WindowPtr pWin
);
static void SiSShadowPaintWindow(
    WindowPtr pWin,
    RegionPtr prgn,
    int what
);
static void SiSShadowCopyWindow(
    WindowPtr pWin,
    DDXPointRec ptOldOrg,
    RegionPtr prgn
);
static Bool SiSShadowCreateGC(GCPtr pGC);
static Bool SiSShadowModifyPixmapHeader(
    PixmapPtr pPixmap,
    int width,
    int height,
    int depth,
    int bitsPerPixel,
    int devKind,
    pointer pPixData
);

static Bool SiSShadowEnterVT(int index, int flags);
static void SiSShadowLeaveVT(int index, int flags);

#ifdef RENDER
static void SiSShadowComposite(
    CARD8 op,
    PicturePtr pSrc,
    PicturePtr pMask,
    PicturePtr pDst,
    INT16 xSrc,
    INT16 ySrc,
    INT16 xMask,
    INT16 yMask,
    INT16 xDst,
    INT16 yDst,
    CARD16 width,
    CARD16 height
);
#endif /* RENDER */

typedef struct {
  ScrnInfoPtr 				pScrn;
  SiSRefreshAreaFuncPtr			postRefresh;
#ifndef SISISXORGPOST112
  PaintWindowBackgroundProcPtr		PaintWindowBackground;
  PaintWindowBorderProcPtr		PaintWindowBorder;
#endif
  CopyWindowProcPtr			CopyWindow;
  CreateGCProcPtr			CreateGC;
#ifndef SISISXORGPOST112
  BackingStoreRestoreAreasProcPtr	RestoreAreas;
#endif
  ModifyPixmapHeaderProcPtr		ModifyPixmapHeader;
#ifdef RENDER
  CompositeProcPtr			Composite;
#endif /* RENDER */
  Bool					(*EnterVT)(int, int);
  void					(*LeaveVT)(int, int);
  Bool					vtSema;
  Bool					enabled;
  CARD8					*FB;
} SiSShadowScreenRec, *SiSShadowScreenPtr;

typedef struct {
   GCOps   *ops;
   GCFuncs *funcs;
} SiSShadowGCRec, * SiSShadowGCPtr;

static DevPrivateKeyRec SiSShadowScreenIndex;
static DevPrivateKeyRec SiSShadowGCIndex;
static DevPrivateKey SiSShadowScreenKey = &SiSShadowScreenIndex;
static DevPrivateKey SiSShadowGCKey = &SiSShadowGCIndex;
static unsigned long SiSShadowGeneration = 0;

#define SIS_GET_SCREEN_PRIVATE(pScreen) \
	(SiSShadowScreenPtr)dixLookupPrivate(&((pScreen)->devPrivates), SiSShadowScreenKey)

#define SIS_GET_GC_PRIVATE(pGC) \
	(SiSShadowGCPtr)dixLookupPrivate(&((pGC)->devPrivates), SiSShadowGCKey)

#define SIS_SHADOW_GC_FUNC_PROLOGUE(pGC) \
    SiSShadowGCPtr pGCPriv = SIS_GET_GC_PRIVATE(pGC); \
    (pGC)->funcs = pGCPriv->funcs; \
    if(pGCPriv->ops) \
        (pGC)->ops = pGCPriv->ops

#define SIS_SHADOW_GC_FUNC_EPILOGUE(pGC) \
    pGCPriv->funcs = (pGC)->funcs; \
    (pGC)->funcs = &SiSShadowGCFuncs; \
    if(pGCPriv->ops) { \
        pGCPriv->ops = (pGC)->ops; \
        (pGC)->ops = &SiSShadowGCOps; \
    }

#define SIS_SHADOW_GC_OP_PROLOGUE(pGC) \
    SiSShadowScreenPtr pPriv = SIS_GET_SCREEN_PRIVATE(pGC->pScreen); \
    SiSShadowGCPtr pGCPriv = SIS_GET_GC_PRIVATE(pGC); \
    GCFuncs *oldFuncs = pGC->funcs; \
    pGC->funcs = pGCPriv->funcs; \
    pGC->ops = pGCPriv->ops

#define SIS_SHADOW_GC_OP_EPILOGUE(pGC) \
    pGCPriv->ops = pGC->ops; \
    pGC->funcs = oldFuncs; \
    pGC->ops   = &SiSShadowGCOps

#define IS_VISIBLE(pWin) (pPriv->vtSema && \
    (((WindowPtr)pWin)->visibility != VisibilityFullyObscured))

#define TRIM_BOX(box, pGC) { \
    BoxPtr extents = &pGC->pCompositeClip->extents;\
    if(box.x1 < extents->x1) box.x1 = extents->x1; \
    if(box.x2 > extents->x2) box.x2 = extents->x2; \
    if(box.y1 < extents->y1) box.y1 = extents->y1; \
    if(box.y2 > extents->y2) box.y2 = extents->y2; \
    }

#define TRANSLATE_BOX(box, pDraw) { \
    box.x1 += pDraw->x; \
    box.x2 += pDraw->x; \
    box.y1 += pDraw->y; \
    box.y2 += pDraw->y; \
    }

#define TRIM_AND_TRANSLATE_BOX(box, pDraw, pGC) { \
    TRANSLATE_BOX(box, pDraw); \
    TRIM_BOX(box, pGC); \
    }

#define BOX_NOT_EMPTY(box) \
    (((box.x2 - box.x1) > 0) && ((box.y2 - box.y1) > 0))

Bool
SiSShadowFBInit(
    ScreenPtr		   pScreen,
    SiSRefreshAreaFuncPtr  postRefreshArea
){
    ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
    SISPtr pSiS = SISPTR(pScrn);
    SiSShadowScreenPtr pPriv;
    GCPtr pGC;
#ifdef RENDER
    PictureScreenPtr ps = GetPictureScreenIfSet(pScreen);
#endif /* RENDER */

    pSiS->SiSShadowFbInit = FALSE;

    if(!postRefreshArea) return FALSE;


    if(SiSShadowGeneration != serverGeneration) {
#ifdef SISISXORGPOST112
	    if (!dixRegisterPrivateKey(SiSShadowScreenKey, PRIVATE_SCREEN, 0))
		return FALSE;

	    if (!dixRegisterPrivateKey
		(SiSShadowGCKey, PRIVATE_GC, sizeof(SiSShadowGCRec)))
		return FALSE;
#else
	    if(!dixAllocatePrivate(&(pScreen->devPrivates), SiSShadowScreenKey) ||
	       !dixRequestPrivate(SiSShadowGCKey, sizeof(SiSShadowGCRec)))
		return FALSE;
#endif
	    SiSShadowGeneration = serverGeneration;
    }

    if(!(pPriv = (SiSShadowScreenPtr)xalloc(sizeof(SiSShadowScreenRec))))
	return FALSE;

    dixSetPrivate(&(pScreen->devPrivates), SiSShadowScreenKey, (pointer)pPriv);

    pPriv->pScrn = pScrn;
    pPriv->postRefresh = postRefreshArea;
    pPriv->vtSema = TRUE;
    pPriv->enabled = FALSE;

    pPriv->EnterVT = pScrn->EnterVT;
    pPriv->LeaveVT = pScrn->LeaveVT;
    pScrn->EnterVT = SiSShadowEnterVT;
    pScrn->LeaveVT = SiSShadowLeaveVT;

#ifndef SISISXORGPOST112
    pPriv->PaintWindowBackground = pScreen->PaintWindowBackground;
    pPriv->PaintWindowBorder = pScreen->PaintWindowBorder;
#endif
    pPriv->CopyWindow = pScreen->CopyWindow;
    pPriv->CreateGC = pScreen->CreateGC;
#ifndef SISISXORGPOST112
    pPriv->RestoreAreas = pScreen->BackingStoreFuncs.RestoreAreas;
#endif
    pPriv->ModifyPixmapHeader = pScreen->ModifyPixmapHeader;

#ifndef SISISXORGPOST112
    pScreen->PaintWindowBackground = SiSShadowPaintWindow;
    pScreen->PaintWindowBorder = SiSShadowPaintWindow;
#endif
    pScreen->CopyWindow = SiSShadowCopyWindow;
    pScreen->CreateGC = SiSShadowCreateGC;
#ifndef SISISXORGPOST112
    pScreen->BackingStoreFuncs.RestoreAreas = SiSShadowRestoreAreas;
#endif
    pScreen->ModifyPixmapHeader = SiSShadowModifyPixmapHeader;

#ifdef RENDER
    if(ps) {
       pPriv->Composite = ps->Composite;
       ps->Composite = SiSShadowComposite;
    }
#endif

    pPriv->FB = (CARD8 *)pSiS->FbBase;

    pSiS->SiSShadowFbInit = TRUE;

    return TRUE;
}

void
SiSShadowFBDeInit(ScreenPtr pScreen)
{
    ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
    SiSShadowScreenPtr pPriv = SIS_GET_SCREEN_PRIVATE(pScreen);
    SISPtr pSiS = SISPTR(pScrn);
#ifdef RENDER
    PictureScreenPtr ps = GetPictureScreenIfSet(pScreen);
#endif /* RENDER */

    if(!pSiS->SiSShadowFbInit) return;

    if(!pPriv) return;

    pScrn->EnterVT = pPriv->EnterVT;
    pScrn->LeaveVT = pPriv->LeaveVT;

#ifndef SISISXORGPOST112
    pScreen->PaintWindowBackground = pPriv->PaintWindowBackground;
    pScreen->PaintWindowBorder = pPriv->PaintWindowBorder;
#endif
    pScreen->CopyWindow = pPriv->CopyWindow;
    pScreen->CreateGC = pPriv->CreateGC;
#ifndef SISISXORGPOST112
    pScreen->BackingStoreFuncs.RestoreAreas = pPriv->RestoreAreas;
#endif
    pScreen->ModifyPixmapHeader = pPriv->ModifyPixmapHeader;

#ifdef RENDER
    if(ps) {
       ps->Composite = pPriv->Composite;
    }
#endif

    xfree((pointer)pPriv);
}

Bool
SiSShadowFBEnableDisable(
    ScreenPtr		pScreen,
    Bool		enable
){
    ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
    SISPtr pSiS = SISPTR(pScrn);
    SiSShadowScreenPtr pPriv;

    if(!pSiS->SiSShadowFbInit) return FALSE;

    pPriv = SIS_GET_SCREEN_PRIVATE(pScreen);

    if(!pPriv) return FALSE;

    pPriv->enabled = enable;

    return TRUE;
}

/**********************************************************/

static Bool
SiSShadowEnterVT(int index, int flags)
{
    ScrnInfoPtr pScrn = xf86Screens[index];
    SiSShadowScreenPtr pPriv = SIS_GET_SCREEN_PRIVATE(pScrn->pScreen);

    if((*pPriv->EnterVT)(index, flags)) {
	pPriv->vtSema = TRUE;
        return TRUE;
    }

    return FALSE;
}

static void
SiSShadowLeaveVT(int index, int flags)
{
    SiSShadowScreenPtr pPriv = SIS_GET_SCREEN_PRIVATE(xf86Screens[index]->pScreen);

    pPriv->vtSema = FALSE;

    (*pPriv->LeaveVT)(index, flags);
}

/**********************************************************/

static Bool
SiSIsFB(DrawablePtr pDraw, SiSShadowScreenPtr pPriv)
{
    PixmapPtr pPixmap;

    if(pDraw->type == DRAWABLE_WINDOW)
	pPixmap = (*pDraw->pScreen->GetWindowPixmap)((WindowPtr)pDraw);
    else
	pPixmap = (PixmapPtr)pDraw;

    if(!pPixmap) return FALSE;

    return (pPixmap->devPrivate.ptr == pPriv->FB);
}

#ifndef SISISXORGPOST112
static void
SiSShadowRestoreAreas(
    PixmapPtr pPixmap,
    RegionPtr prgn,
    int       xorg,
    int       yorg,
    WindowPtr pWin
){
    ScreenPtr pScreen = pWin->drawable.pScreen;
    SiSShadowScreenPtr pPriv = SIS_GET_SCREEN_PRIVATE(pScreen);
    int num = 0;

    if(pPriv->enabled && pPriv->vtSema) {
       num = REGION_NUM_RECTS(prgn);
    }

    pScreen->BackingStoreFuncs.RestoreAreas = pPriv->RestoreAreas;
    (*pScreen->BackingStoreFuncs.RestoreAreas)(pPixmap, prgn, xorg, yorg, pWin);
    pScreen->BackingStoreFuncs.RestoreAreas = SiSShadowRestoreAreas;

    if(num && pPriv->postRefresh) {
        SHDEBUG(ErrorF("SiSShadowRestoreAreas %d / %d %d / %d\n", num,
        			pWin->drawable.type, DRAWABLE_WINDOW,
        			pWin->redirectDraw));
	(*pPriv->postRefresh)(pPriv->pScrn, num, REGION_RECTS(prgn));
    }
}

static void
SiSShadowPaintWindow(
  WindowPtr pWin,
  RegionPtr prgn,
  int what
){
    ScreenPtr pScreen = pWin->drawable.pScreen;
    SiSShadowScreenPtr pPriv = SIS_GET_SCREEN_PRIVATE(pScreen);
    int num = 0;

    if(pPriv->enabled && pPriv->vtSema && SiSIsFB((DrawablePtr)pWin, pPriv)) {
       num = REGION_NUM_RECTS(prgn);
    }

    if(what == PW_BACKGROUND) {
	pScreen->PaintWindowBackground = pPriv->PaintWindowBackground;
	(*pScreen->PaintWindowBackground)(pWin, prgn, what);
	pScreen->PaintWindowBackground = SiSShadowPaintWindow;
    } else {
	pScreen->PaintWindowBorder = pPriv->PaintWindowBorder;
	(*pScreen->PaintWindowBorder)(pWin, prgn, what);
	pScreen->PaintWindowBorder = SiSShadowPaintWindow;
    }

    if(num && pPriv->postRefresh) {
        SHDEBUG(ErrorF("SiSShadowPaintWindow %d what %d (%d) / %d %d / %d\n", num,
        			what, PW_BACKGROUND,
        			pWin->drawable.type, DRAWABLE_WINDOW,
        			pWin->redirectDraw));
        (*pPriv->postRefresh)(pPriv->pScrn, num, REGION_RECTS(prgn));
    }
}
#endif

static void
SiSShadowCopyWindow(
   WindowPtr pWin,
   DDXPointRec ptOldOrg,
   RegionPtr prgn
){
    ScreenPtr pScreen = pWin->drawable.pScreen;
    SiSShadowScreenPtr pPriv = SIS_GET_SCREEN_PRIVATE(pScreen);
    RegionRec rgnDst;
    int num = 0;

    if(pPriv->enabled && pPriv->vtSema && SiSIsFB((DrawablePtr)pWin, pPriv)) {
        REGION_NULL(pWin->drawable.pScreen, &rgnDst);
	REGION_COPY(pWin->drawable.pScreen, &rgnDst, prgn);

        REGION_TRANSLATE(pWin->drawable.pScreen, &rgnDst,
                         pWin->drawable.x - ptOldOrg.x,
                         pWin->drawable.y - ptOldOrg.y);
        REGION_INTERSECT(pScreen, &rgnDst, &pWin->borderClip, &rgnDst);

        if(!(num = REGION_NUM_RECTS(&rgnDst))) {
            REGION_UNINIT(pWin->drawable.pScreen, &rgnDst);
        }
    }

    pScreen->CopyWindow = pPriv->CopyWindow;
    (*pScreen->CopyWindow) (pWin, ptOldOrg, prgn);
    pScreen->CopyWindow = SiSShadowCopyWindow;

    if(num) {
        SHDEBUG(ErrorF("SiSShadowCopyWindow %d / %d %d / %d\n", num,
        			pWin->drawable.type, DRAWABLE_WINDOW,
        			pWin->redirectDraw));
        if(pPriv->postRefresh)
            (*pPriv->postRefresh)(pPriv->pScrn, num, REGION_RECTS(&rgnDst));
        REGION_UNINIT(pWin->drawable.pScreen, &rgnDst);
    }
}

static Bool
SiSShadowModifyPixmapHeader(
    PixmapPtr pPixmap,
    int width,
    int height,
    int depth,
    int bitsPerPixel,
    int devKind,
    pointer pPixData
)
{
    ScreenPtr pScreen;
    ScrnInfoPtr pScrn;
    SiSShadowScreenPtr pPriv;
    PixmapPtr pScreenPix;
    Bool retval;

    if(!pPixmap)
	return FALSE;

    pScreen = pPixmap->drawable.pScreen;
    pScrn = xf86Screens[pScreen->myNum];

    pScreenPix = (*pScreen->GetScreenPixmap)(pScreen);

#ifndef SISISXORGPOST112
    if(pPixmap == pScreenPix && !pScrn->vtSema)
	pScreenPix->devPrivate = pScrn->pixmapPrivate;
#endif

    pPriv = SIS_GET_SCREEN_PRIVATE(pScreen);

    pScreen->ModifyPixmapHeader = pPriv->ModifyPixmapHeader;
    retval = (*pScreen->ModifyPixmapHeader)(pPixmap,
		width, height, depth, bitsPerPixel, devKind, pPixData);
    pScreen->ModifyPixmapHeader = SiSShadowModifyPixmapHeader;

#ifndef SISISXORGPOST112
    if(pPixmap == pScreenPix && !pScrn->vtSema) {
	pScrn->pixmapPrivate = pScreenPix->devPrivate;
	pScreenPix->devPrivate.ptr = 0;
    }
#endif
    return retval;
}

#ifdef RENDER
static void
SiSShadowComposite(
    CARD8 op,
    PicturePtr pSrc,
    PicturePtr pMask,
    PicturePtr pDst,
    INT16 xSrc,
    INT16 ySrc,
    INT16 xMask,
    INT16 yMask,
    INT16 xDst,
    INT16 yDst,
    CARD16 width,
    CARD16 height
){
    ScreenPtr pScreen = pDst->pDrawable->pScreen;
    SiSShadowScreenPtr pPriv = SIS_GET_SCREEN_PRIVATE(pScreen);
    PictureScreenPtr ps = GetPictureScreen(pScreen);
    BoxRec box;
    BoxPtr extents;
    Bool boxNotEmpty = FALSE;

    if(pPriv->enabled && pPriv->vtSema && SiSIsFB(pDst->pDrawable, pPriv) &&
    			pDst->pDrawable->type == DRAWABLE_WINDOW) {

	box.x1 = pDst->pDrawable->x + xDst;
	box.y1 = pDst->pDrawable->y + yDst;
	box.x2 = box.x1 + width;
	box.y2 = box.y1 + height;

	extents = &pDst->pCompositeClip->extents;
	if(box.x1 < extents->x1) box.x1 = extents->x1;
	if(box.x2 > extents->x2) box.x2 = extents->x2;
	if(box.y1 < extents->y1) box.y1 = extents->y1;
	if(box.y2 > extents->y2) box.y2 = extents->y2;

	if(BOX_NOT_EMPTY(box)) {
	    boxNotEmpty = TRUE;
	    /* Sync here; otherwise window moving (eg) causes
	     * ugly artifacts. TODO: Need a general strategy
	     * on this. Rotation never syncs, composite is
	     * purely CPU (currently). Therefore, composite
	     * shuffles data while a rotated blit for the
	     * previous image is still on-going, and this
	     * blit eventually takes newly composited data
	     * as source data.
	     */
	    (SISPTR(pPriv->pScrn)->SyncAccel)(pPriv->pScrn);
	}
    }

    ps->Composite = pPriv->Composite;
    (*ps->Composite)(op, pSrc, pMask, pDst, xSrc, ySrc,
		     xMask, yMask, xDst, yDst, width, height);
    ps->Composite = SiSShadowComposite;

    if(pPriv->postRefresh && boxNotEmpty) {
        SHDEBUG(ErrorF("Composite %d-%d - %d-%d\n", box.x1, box.x2, box.y1, box.y2));
        (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }
}
#endif /* RENDER */

/**********************************************************/

static void SiSShadowValidateGC(GCPtr, unsigned long, DrawablePtr);
static void SiSShadowChangeGC(GCPtr, unsigned long);
static void SiSShadowCopyGC(GCPtr, unsigned long, GCPtr);
static void SiSShadowDestroyGC(GCPtr);
static void SiSShadowChangeClip(GCPtr, int, pointer, int);
static void SiSShadowDestroyClip(GCPtr);
static void SiSShadowCopyClip(GCPtr, GCPtr);

GCFuncs SiSShadowGCFuncs = {
    SiSShadowValidateGC, SiSShadowChangeGC, SiSShadowCopyGC, SiSShadowDestroyGC,
    SiSShadowChangeClip, SiSShadowDestroyClip, SiSShadowCopyClip
};

extern GCOps SiSShadowGCOps;

static Bool
SiSShadowCreateGC(GCPtr pGC)
{
    ScreenPtr pScreen = pGC->pScreen;
    SiSShadowScreenPtr pPriv = SIS_GET_SCREEN_PRIVATE(pScreen);
    SiSShadowGCPtr pGCPriv = SIS_GET_GC_PRIVATE(pGC);
    Bool ret;

    pScreen->CreateGC = pPriv->CreateGC;
    if((ret = (*pScreen->CreateGC)(pGC))) {
	pGCPriv->ops = NULL;
	pGCPriv->funcs = pGC->funcs;
	pGC->funcs = &SiSShadowGCFuncs;
    }
    pScreen->CreateGC = SiSShadowCreateGC;

    return ret;
}

static void
SiSShadowValidateGC(
   GCPtr         pGC,
   unsigned long changes,
   DrawablePtr   pDraw
){
    SIS_SHADOW_GC_FUNC_PROLOGUE (pGC);
    (*pGC->funcs->ValidateGC)(pGC, changes, pDraw);
    if(pDraw->type == DRAWABLE_WINDOW)
	pGCPriv->ops = pGC->ops;  /* just so it's not NULL */
    else
	pGCPriv->ops = NULL;
    SIS_SHADOW_GC_FUNC_EPILOGUE (pGC);
}


static void
SiSShadowDestroyGC(GCPtr pGC)
{
    SIS_SHADOW_GC_FUNC_PROLOGUE (pGC);
    (*pGC->funcs->DestroyGC)(pGC);
    SIS_SHADOW_GC_FUNC_EPILOGUE (pGC);
}

static void
SiSShadowChangeGC (
    GCPtr	    pGC,
    unsigned long   mask
){
    SIS_SHADOW_GC_FUNC_PROLOGUE (pGC);
    (*pGC->funcs->ChangeGC) (pGC, mask);
    SIS_SHADOW_GC_FUNC_EPILOGUE (pGC);
}

static void
SiSShadowCopyGC(
    GCPtr	    pGCSrc,
    unsigned long   mask,
    GCPtr	    pGCDst
){
    SIS_SHADOW_GC_FUNC_PROLOGUE (pGCDst);
    (*pGCDst->funcs->CopyGC) (pGCSrc, mask, pGCDst);
    SIS_SHADOW_GC_FUNC_EPILOGUE (pGCDst);
}

static void
SiSShadowChangeClip(
    GCPtr   pGC,
    int		type,
    pointer	pvalue,
    int		nrects
){
    SIS_SHADOW_GC_FUNC_PROLOGUE (pGC);
    (*pGC->funcs->ChangeClip) (pGC, type, pvalue, nrects);
    SIS_SHADOW_GC_FUNC_EPILOGUE (pGC);
}

static void
SiSShadowCopyClip(GCPtr pgcDst, GCPtr pgcSrc)
{
    SIS_SHADOW_GC_FUNC_PROLOGUE (pgcDst);
    (* pgcDst->funcs->CopyClip)(pgcDst, pgcSrc);
    SIS_SHADOW_GC_FUNC_EPILOGUE (pgcDst);
}

static void
SiSShadowDestroyClip(GCPtr pGC)
{
    SIS_SHADOW_GC_FUNC_PROLOGUE (pGC);
    (* pGC->funcs->DestroyClip)(pGC);
    SIS_SHADOW_GC_FUNC_EPILOGUE (pGC);
}

/**********************************************************/

static void
SiSShadowFillSpans(
    DrawablePtr pDraw,
    GC		*pGC,
    int		nInit,
    DDXPointPtr pptInit,
    int 	*pwidthInit,
    int 	fSorted
){
    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && nInit) {

	DDXPointPtr ppt = pptInit;
	int *pwidth = pwidthInit;
	int i = nInit;
	BoxRec box;
        Bool boxNotEmpty = FALSE;

	box.x1 = ppt->x;
	box.x2 = box.x1 + *pwidth;
	box.y2 = box.y1 = ppt->y;

	while(--i) {
	   ppt++;
	   pwidth++;
	   if(box.x1 > ppt->x) box.x1 = ppt->x;
	   if(box.x2 < (ppt->x + *pwidth))
		box.x2 = ppt->x + *pwidth;
	   if(box.y1 > ppt->y) box.y1 = ppt->y;
	   else if(box.y2 < ppt->y) box.y2 = ppt->y;
	}

	box.y2++;

        if(!pGC->miTranslate) {
           TRANSLATE_BOX(box, pDraw);
        }
        TRIM_BOX(box, pGC);

	if(BOX_NOT_EMPTY(box)) {
            boxNotEmpty = TRUE;
        }

	(*pGC->ops->FillSpans)(pDraw, pGC, nInit, pptInit, pwidthInit, fSorted);

        if(boxNotEmpty && pPriv->postRefresh) {
           SHDEBUG(ErrorF("SiSShadowFillSpans %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
	   (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
	}

    } else {

	(*pGC->ops->FillSpans)(pDraw, pGC, nInit, pptInit, pwidthInit, fSorted);

    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static void
SiSShadowSetSpans(
    DrawablePtr		pDraw,
    GCPtr		pGC,
    char		*pcharsrc,
    DDXPointPtr 	pptInit,
    int			*pwidthInit,
    int			nspans,
    int			fSorted
){
    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && nspans) {

	DDXPointPtr ppt = pptInit;
	int *pwidth = pwidthInit;
	int i = nspans;
	BoxRec box;
        Bool boxNotEmpty = FALSE;

	box.x1 = ppt->x;
	box.x2 = box.x1 + *pwidth;
	box.y2 = box.y1 = ppt->y;

	while(--i) {
	   ppt++;
	   pwidth++;
	   if(box.x1 > ppt->x) box.x1 = ppt->x;
	   if(box.x2 < (ppt->x + *pwidth))
		box.x2 = ppt->x + *pwidth;
	   if(box.y1 > ppt->y) box.y1 = ppt->y;
	   else if(box.y2 < ppt->y) box.y2 = ppt->y;
	}

	box.y2++;

        if(!pGC->miTranslate) {
           TRANSLATE_BOX(box, pDraw);
        }
        TRIM_BOX(box, pGC);

	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }

	(*pGC->ops->SetSpans)(pDraw, pGC, pcharsrc, pptInit,
				pwidthInit, nspans, fSorted);

	if(boxNotEmpty && pPriv->postRefresh) {
	   SHDEBUG(ErrorF("SiSShadowSetSpans %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
	   (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
        }

    } else {

	(*pGC->ops->SetSpans)(pDraw, pGC, pcharsrc, pptInit,
				pwidthInit, nspans, fSorted);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static void
SiSShadowPutImage(
    DrawablePtr pDraw,
    GCPtr	pGC,
    int		depth,
    int x, int y, int w, int h,
    int		leftPad,
    int		format,
    char 	*pImage
){
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw)) {
	box.x1 = x + pDraw->x;
	box.x2 = box.x1 + w;
	box.y1 = y + pDraw->y;
	box.y2 = box.y1 + h;

	TRIM_BOX(box, pGC);

	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    (*pGC->ops->PutImage)(pDraw, pGC, depth, x, y, w, h,
		leftPad, format, pImage);

    if(boxNotEmpty && pPriv->postRefresh) {
        SHDEBUG(ErrorF("SiSShadowPutImage %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
        (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static RegionPtr
SiSShadowCopyArea(
    DrawablePtr pSrc,
    DrawablePtr pDst,
    GC *pGC,
    int srcx, int srcy,
    int width, int height,
    int dstx, int dsty
){
    RegionPtr ret;
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDst, pPriv) && IS_VISIBLE(pDst)) {
	box.x1 = dstx + pDst->x;
	box.x2 = box.x1 + width;
	box.y1 = dsty + pDst->y;
	box.y2 = box.y1 + height;

	TRIM_BOX(box, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    ret = (*pGC->ops->CopyArea)(pSrc, pDst,
            pGC, srcx, srcy, width, height, dstx, dsty);

    if(boxNotEmpty && pPriv->postRefresh) {
        SHDEBUG(ErrorF("SiSShadowCopyArea %d / %d %d\n", 1,
        			pDst->type, DRAWABLE_WINDOW));
        (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);

    return ret;
}

static RegionPtr
SiSShadowCopyPlane(
    DrawablePtr	pSrc,
    DrawablePtr	pDst,
    GCPtr pGC,
    int	srcx, int srcy,
    int	width, int height,
    int	dstx, int dsty,
    unsigned long bitPlane
){
    RegionPtr ret;
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDst, pPriv) && IS_VISIBLE(pDst)) {
	box.x1 = dstx + pDst->x;
	box.x2 = box.x1 + width;
	box.y1 = dsty + pDst->y;
	box.y2 = box.y1 + height;

	TRIM_BOX(box, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    ret = (*pGC->ops->CopyPlane)(pSrc, pDst,
	       pGC, srcx, srcy, width, height, dstx, dsty, bitPlane);

    if(boxNotEmpty && pPriv->postRefresh) {
        SHDEBUG(ErrorF("SiSShadowCopyPlane %d / %d %d\n", 1,
        			pDst->type, DRAWABLE_WINDOW));
        (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);

    return ret;
}

static void
SiSShadowPolyPoint(
    DrawablePtr pDraw,
    GCPtr pGC,
    int mode,
    int nptInit,
    xPoint *pptInit
){
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && nptInit) {
        xPoint *ppt = pptInit;
        int npt = nptInit;

	box.x2 = box.x1 = pptInit->x;
	box.y2 = box.y1 = pptInit->y;

	/* this could be slow if the points were spread out */

	while(--npt) {
	   ppt++;
	   if(box.x1 > ppt->x) box.x1 = ppt->x;
	   else if(box.x2 < ppt->x) box.x2 = ppt->x;
	   if(box.y1 > ppt->y) box.y1 = ppt->y;
	   else if(box.y2 < ppt->y) box.y2 = ppt->y;
	}

	box.x2++;
	box.y2++;

	TRIM_AND_TRANSLATE_BOX(box, pDraw, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    (*pGC->ops->PolyPoint)(pDraw, pGC, mode, nptInit, pptInit);

    if(boxNotEmpty && pPriv->postRefresh) {
        SHDEBUG(ErrorF("SiSShadowPolyPoint %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
        (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static void
SiSShadowPolylines(
    DrawablePtr pDraw,
    GCPtr	pGC,
    int		mode,
    int		nptInit,
    DDXPointPtr pptInit
){
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && nptInit) {
        DDXPointPtr ppt = pptInit;
        int npt = nptInit;
	int extra = pGC->lineWidth >> 1;

	box.x2 = box.x1 = pptInit->x;
	box.y2 = box.y1 = pptInit->y;

	if(npt > 1) {
	   if(pGC->joinStyle == JoinMiter)
		extra = 6 * pGC->lineWidth;
	   else if(pGC->capStyle == CapProjecting)
		extra = pGC->lineWidth;
        }

	if(mode == CoordModePrevious) {
	   int x = box.x1;
	   int y = box.y1;
	   while(--npt) {
		ppt++;
		x += ppt->x;
		y += ppt->y;
		if(box.x1 > x) box.x1 = x;
		else if(box.x2 < x) box.x2 = x;
		if(box.y1 > y) box.y1 = y;
		else if(box.y2 < y) box.y2 = y;
	    }
	} else {
	   while(--npt) {
		ppt++;
		if(box.x1 > ppt->x) box.x1 = ppt->x;
		else if(box.x2 < ppt->x) box.x2 = ppt->x;
		if(box.y1 > ppt->y) box.y1 = ppt->y;
		else if(box.y2 < ppt->y) box.y2 = ppt->y;
	    }
	}

	box.x2++;
	box.y2++;

	if(extra) {
	   box.x1 -= extra;
	   box.x2 += extra;
	   box.y1 -= extra;
	   box.y2 += extra;
        }

	TRIM_AND_TRANSLATE_BOX(box, pDraw, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    (*pGC->ops->Polylines)(pDraw, pGC, mode, nptInit, pptInit);

    if(boxNotEmpty && pPriv->postRefresh) {
        SHDEBUG(ErrorF("SiSShadowPolylines %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
       (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static void
SiSShadowPolySegment(
    DrawablePtr	pDraw,
    GCPtr	pGC,
    int		nsegInit,
    xSegment	*pSegInit
){
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && nsegInit) {
	int extra = pGC->lineWidth;
        xSegment *pSeg = pSegInit;
        int nseg = nsegInit;

        if(pGC->capStyle != CapProjecting)
	   extra >>= 1;

	if(pSeg->x2 > pSeg->x1) {
	    box.x1 = pSeg->x1;
	    box.x2 = pSeg->x2;
	} else {
	    box.x2 = pSeg->x1;
	    box.x1 = pSeg->x2;
	}

	if(pSeg->y2 > pSeg->y1) {
	    box.y1 = pSeg->y1;
	    box.y2 = pSeg->y2;
	} else {
	    box.y2 = pSeg->y1;
	    box.y1 = pSeg->y2;
	}

	while(--nseg) {
	    pSeg++;
	    if(pSeg->x2 > pSeg->x1) {
		if(pSeg->x1 < box.x1) box.x1 = pSeg->x1;
		if(pSeg->x2 > box.x2) box.x2 = pSeg->x2;
	    } else {
		if(pSeg->x2 < box.x1) box.x1 = pSeg->x2;
		if(pSeg->x1 > box.x2) box.x2 = pSeg->x1;
	    }
	    if(pSeg->y2 > pSeg->y1) {
		if(pSeg->y1 < box.y1) box.y1 = pSeg->y1;
		if(pSeg->y2 > box.y2) box.y2 = pSeg->y2;
	    } else {
		if(pSeg->y2 < box.y1) box.y1 = pSeg->y2;
		if(pSeg->y1 > box.y2) box.y2 = pSeg->y1;
	    }
	}

	box.x2++;
	box.y2++;

	if(extra) {
	   box.x1 -= extra;
	   box.x2 += extra;
	   box.y1 -= extra;
	   box.y2 += extra;
        }

	TRIM_AND_TRANSLATE_BOX(box, pDraw, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    (*pGC->ops->PolySegment)(pDraw, pGC, nsegInit, pSegInit);

    if(boxNotEmpty && pPriv->postRefresh) {
       SHDEBUG(ErrorF("SiSShadowPolySegment %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
       (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static void
SiSShadowPolyRectangle(
    DrawablePtr  pDraw,
    GCPtr        pGC,
    int	         nRectsInit,
    xRectangle  *pRectsInit
){
    BoxRec box;
    BoxPtr pBoxInit = NULL;
    Bool boxNotEmpty = FALSE;
    int num = 0;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && nRectsInit) {
        xRectangle *pRects = pRectsInit;
        int nRects = nRectsInit;

	if(nRects >= 32) {
	    int extra = pGC->lineWidth >> 1;

	    box.x1 = pRects->x;
	    box.x2 = box.x1 + pRects->width;
	    box.y1 = pRects->y;
	    box.y2 = box.y1 + pRects->height;

	    while(--nRects) {
		pRects++;
		if(box.x1 > pRects->x) box.x1 = pRects->x;
		if(box.x2 < (pRects->x + pRects->width))
			box.x2 = pRects->x + pRects->width;
		if(box.y1 > pRects->y) box.y1 = pRects->y;
		if(box.y2 < (pRects->y + pRects->height))
			box.y2 = pRects->y + pRects->height;
	    }

	    if(extra) {
		box.x1 -= extra;
		box.x2 += extra;
		box.y1 -= extra;
		box.y2 += extra;
	    }

	    box.x2++;
	    box.y2++;

	    TRIM_AND_TRANSLATE_BOX(box, pDraw, pGC);
	    if(BOX_NOT_EMPTY(box)) {
                boxNotEmpty = TRUE;
            }

	} else {

	    BoxPtr pbox;
	    int offset1, offset2, offset3;

	    offset2 = pGC->lineWidth;
	    if(!offset2) offset2 = 1;
	    offset1 = offset2 >> 1;
	    offset3 = offset2 - offset1;

	    pBoxInit = (BoxPtr)ALLOCATE_LOCAL(nRects * 4 * sizeof(BoxRec));
	    pbox = pBoxInit;

	    while(nRects--) {
		pbox->x1 = pRects->x - offset1;
		pbox->y1 = pRects->y - offset1;
		pbox->x2 = pbox->x1 + pRects->width + offset2;
		pbox->y2 = pbox->y1 + offset2;
		TRIM_AND_TRANSLATE_BOX((*pbox), pDraw, pGC);
		if(BOX_NOT_EMPTY((*pbox))) {
		   num++;
		   pbox++;
		}

		pbox->x1 = pRects->x - offset1;
		pbox->y1 = pRects->y + offset3;
		pbox->x2 = pbox->x1 + offset2;
		pbox->y2 = pbox->y1 + pRects->height - offset2;
		TRIM_AND_TRANSLATE_BOX((*pbox), pDraw, pGC);
		if(BOX_NOT_EMPTY((*pbox))) {
		   num++;
		   pbox++;
		}

		pbox->x1 = pRects->x + pRects->width - offset1;
		pbox->y1 = pRects->y + offset3;
		pbox->x2 = pbox->x1 + offset2;
		pbox->y2 = pbox->y1 + pRects->height - offset2;
		TRIM_AND_TRANSLATE_BOX((*pbox), pDraw, pGC);
		if(BOX_NOT_EMPTY((*pbox))) {
		   num++;
		   pbox++;
		}

		pbox->x1 = pRects->x - offset1;
		pbox->y1 = pRects->y + pRects->height - offset1;
		pbox->x2 = pbox->x1 + pRects->width + offset2;
		pbox->y2 = pbox->y1 + offset2;
		TRIM_AND_TRANSLATE_BOX((*pbox), pDraw, pGC);
		if(BOX_NOT_EMPTY((*pbox))) {
		   num++;
		   pbox++;
		}

		pRects++;
	    }

	    if(!num) {
                DEALLOCATE_LOCAL(pBoxInit);
            }
	}
    }

    (*pGC->ops->PolyRectangle)(pDraw, pGC, nRectsInit, pRectsInit);

    if(boxNotEmpty && pPriv->postRefresh) {
       SHDEBUG(ErrorF("SiSShadowPolyRectangle1 %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
       (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    } else if(num) {
       if(pPriv->postRefresh) {
          SHDEBUG(ErrorF("SiSShadowPolyRectangle2 %d / %d %d\n", num,
        			pDraw->type, DRAWABLE_WINDOW));
          (*pPriv->postRefresh)(pPriv->pScrn, num, pBoxInit);
       }
       DEALLOCATE_LOCAL(pBoxInit);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);

}

static void
SiSShadowPolyArc(
    DrawablePtr	pDraw,
    GCPtr	pGC,
    int		narcsInit,
    xArc	*parcsInit
){
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && narcsInit) {
        int narcs = narcsInit;
        xArc *parcs = parcsInit;
        int extra = pGC->lineWidth >> 1;

	box.x1 = parcs->x;
	box.x2 = box.x1 + parcs->width;
	box.y1 = parcs->y;
	box.y2 = box.y1 + parcs->height;

	/* should I break these up instead ? */

	while(--narcs) {
	   parcs++;
	   if(box.x1 > parcs->x) box.x1 = parcs->x;
	   if(box.x2 < (parcs->x + parcs->width))
		box.x2 = parcs->x + parcs->width;
	   if(box.y1 > parcs->y) box.y1 = parcs->y;
	   if(box.y2 < (parcs->y + parcs->height))
		box.y2 = parcs->y + parcs->height;
        }

	if(extra) {
	   box.x1 -= extra;
	   box.x2 += extra;
	   box.y1 -= extra;
	   box.y2 += extra;
        }

	box.x2++;
	box.y2++;

	TRIM_AND_TRANSLATE_BOX(box, pDraw, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    (*pGC->ops->PolyArc)(pDraw, pGC, narcsInit, parcsInit);

    if(boxNotEmpty && pPriv->postRefresh) {
       SHDEBUG(ErrorF("SiSShadowPolyArc %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
       (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);

}

static void
SiSShadowFillPolygon(
    DrawablePtr	pDraw,
    GCPtr	pGC,
    int		shape,
    int		mode,
    int		count,
    DDXPointPtr	pptInit
){
    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && (count > 2)) {

	DDXPointPtr ppt = pptInit;
	int i = count;
	BoxRec box;
        Bool boxNotEmpty = FALSE;

	box.x2 = box.x1 = ppt->x;
	box.y2 = box.y1 = ppt->y;

	if(mode != CoordModeOrigin) {
	   int x = box.x1;
	   int y = box.y1;
	   while(--i) {
		ppt++;
		x += ppt->x;
		y += ppt->y;
		if(box.x1 > x) box.x1 = x;
		else if(box.x2 < x) box.x2 = x;
		if(box.y1 > y) box.y1 = y;
		else if(box.y2 < y) box.y2 = y;
	    }
	} else {
	   while(--i) {
		ppt++;
		if(box.x1 > ppt->x) box.x1 = ppt->x;
		else if(box.x2 < ppt->x) box.x2 = ppt->x;
		if(box.y1 > ppt->y) box.y1 = ppt->y;
		else if(box.y2 < ppt->y) box.y2 = ppt->y;
	    }
	}

	box.x2++;
	box.y2++;

	TRIM_AND_TRANSLATE_BOX(box, pDraw, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }

	(*pGC->ops->FillPolygon)(pDraw, pGC, shape, mode, count, pptInit);

        if(boxNotEmpty && pPriv->postRefresh) {
           SHDEBUG(ErrorF("SiSShadowFillPolygon %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
           (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
        }

    } else {

	(*pGC->ops->FillPolygon)(pDraw, pGC, shape, mode, count, pptInit);

    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}


static void
SiSShadowPolyFillRect(
    DrawablePtr	pDraw,
    GCPtr	pGC,
    int		nRectsInit,
    xRectangle	*pRectsInit
){
    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && nRectsInit) {

	BoxRec box;
        Bool boxNotEmpty = FALSE;
	xRectangle *pRects = pRectsInit;
	int nRects = nRectsInit;

	box.x1 = pRects->x;
	box.x2 = box.x1 + pRects->width;
	box.y1 = pRects->y;
	box.y2 = box.y1 + pRects->height;

	while(--nRects) {
	    pRects++;
	    if(box.x1 > pRects->x) box.x1 = pRects->x;
	    if(box.x2 < (pRects->x + pRects->width))
		box.x2 = pRects->x + pRects->width;
	    if(box.y1 > pRects->y) box.y1 = pRects->y;
	    if(box.y2 < (pRects->y + pRects->height))
		box.y2 = pRects->y + pRects->height;
	}

	/* cfb messes with the pRectsInit so we have to do our
	   calculations first */

	TRIM_AND_TRANSLATE_BOX(box, pDraw, pGC);
	if(BOX_NOT_EMPTY(box)) {
            boxNotEmpty = TRUE;
        }

	(*pGC->ops->PolyFillRect)(pDraw, pGC, nRectsInit, pRectsInit);

        if(boxNotEmpty && pPriv->postRefresh) {
            SHDEBUG(ErrorF("SiSShadowPolyFillRect %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
            (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
        }

    } else {

	(*pGC->ops->PolyFillRect)(pDraw, pGC, nRectsInit, pRectsInit);

    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static void
SiSShadowPolyFillArc(
    DrawablePtr	pDraw,
    GCPtr	pGC,
    int		narcsInit,
    xArc	*parcsInit
){
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && narcsInit) {
        xArc *parcs = parcsInit;
        int narcs = narcsInit;

	box.x1 = parcs->x;
	box.x2 = box.x1 + parcs->width;
	box.y1 = parcs->y;
	box.y2 = box.y1 + parcs->height;

	/* should I break these up instead ? */

	while(--narcs) {
	   parcs++;
	   if(box.x1 > parcs->x) box.x1 = parcs->x;
	   if(box.x2 < (parcs->x + parcs->width))
		box.x2 = parcs->x + parcs->width;
	   if(box.y1 > parcs->y) box.y1 = parcs->y;
	   if(box.y2 < (parcs->y + parcs->height))
		box.y2 = parcs->y + parcs->height;
        }

	TRIM_AND_TRANSLATE_BOX(box, pDraw, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    (*pGC->ops->PolyFillArc)(pDraw, pGC, narcsInit, parcsInit);

    if(boxNotEmpty && pPriv->postRefresh) {
       SHDEBUG(ErrorF("SiSShadowPolyFillArc %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
       (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static void
SiSShadowTextExtent(FontPtr pFont, int count, char* chars,
			FontEncoding fontEncoding, BoxPtr box)
{
    unsigned long n, i;
    int w;
    CharInfoPtr charinfo[255];	/* encoding only has 1 byte for count */

    GetGlyphs(pFont, (unsigned long)count, (unsigned char *)chars,
	      fontEncoding, &n, charinfo);
    w = 0;
    for (i=0; i < n; i++) {
        w += charinfo[i]->metrics.characterWidth;
    }
    if(i) {
    	w += charinfo[i - 1]->metrics.rightSideBearing;
    }

    box->x1 = 0;
    if(n) {
	if(charinfo[0]->metrics.leftSideBearing < 0) {
            box->x1 = charinfo[0]->metrics.leftSideBearing;
        }
    }
    box->x2 = w;
    box->y1 = -FONTMAXBOUNDS(pFont,ascent);
    box->y2 = FONTMAXBOUNDS(pFont,descent);
}

static void
SiSShadowFontToBox(BoxPtr BB, DrawablePtr pDrawable, GCPtr pGC, int x, int y,
		int count, char *chars, int wide)
{
    FontPtr pFont;

    pFont = pGC->font;

    if(pFont->info.constantWidth) {

        int ascent, descent, left, right = 0;

	ascent = MAX(pFont->info.fontAscent, pFont->info.maxbounds.ascent);
	descent = MAX(pFont->info.fontDescent, pFont->info.maxbounds.descent);
	left = pFont->info.maxbounds.leftSideBearing;
	if(count > 0) {
	    right = (count - 1) * pFont->info.maxbounds.characterWidth;
	}
	right += pFont->info.maxbounds.rightSideBearing;
	BB->x1 =
	    MAX(pDrawable->x + x - left, (REGION_EXTENTS(pGC->pScreen,
		&((WindowPtr) pDrawable)->winSize))->x1);
	BB->y1 =
	    MAX(pDrawable->y + y - ascent,
	    (REGION_EXTENTS(pGC->pScreen,
             &((WindowPtr) pDrawable)->winSize))->y1);
	BB->x2 =
	    MIN(pDrawable->x + x + right,
	    (REGION_EXTENTS(pGC->pScreen,
             &((WindowPtr) pDrawable)->winSize))->x2);
	BB->y2 =
	    MIN(pDrawable->y + y + descent,
	    (REGION_EXTENTS(pGC->pScreen,
             &((WindowPtr) pDrawable)->winSize))->y2);

    } else {

    	SiSShadowTextExtent(pFont, count, chars, wide ? (FONTLASTROW(pFont) == 0)
                         ? Linear16Bit : TwoD16Bit : Linear8Bit, BB);
	BB->x1 =
	    MAX(pDrawable->x + x + BB->x1, (REGION_EXTENTS(pGC->pScreen,
		&((WindowPtr) pDrawable)->winSize))->x1);
	BB->y1 =
	    MAX(pDrawable->y + y + BB->y1,
	    (REGION_EXTENTS(pGC->pScreen,
             &((WindowPtr) pDrawable)->winSize))->y1);
	BB->x2 =
	    MIN(pDrawable->x + x + BB->x2,
	    (REGION_EXTENTS(pGC->pScreen,
	     &((WindowPtr) pDrawable)->winSize))->x2);
	BB->y2 =
	    MIN(pDrawable->y + y + BB->y2,
	    (REGION_EXTENTS(pGC->pScreen,
	     &((WindowPtr) pDrawable)->winSize))->y2);
    }
}

static int
SiSShadowPolyText8(
    DrawablePtr pDraw,
    GCPtr	pGC,
    int		x,
    int 	y,
    int 	count,
    char	*chars
){
    int width;
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw)) {
        SiSShadowFontToBox(&box, pDraw, pGC, x, y, count, chars, 0);
        TRIM_BOX(box, pGC);
        if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    width = (*pGC->ops->PolyText8)(pDraw, pGC, x, y, count, chars);

    if(boxNotEmpty && pPriv->postRefresh) {
       SHDEBUG(ErrorF("SiSShadowPolyText8 %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
       (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);

    return width;
}

static int
SiSShadowPolyText16(
    DrawablePtr pDraw,
    GCPtr	pGC,
    int		x,
    int		y,
    int 	count,
    unsigned short *chars
){
    int width;
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw)) {
        SiSShadowFontToBox(&box, pDraw, pGC, x, y, count, (char*)chars, 1);
        TRIM_BOX(box, pGC);
        if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    width = (*pGC->ops->PolyText16)(pDraw, pGC, x, y, count, chars);

    if(boxNotEmpty && pPriv->postRefresh) {
       SHDEBUG(ErrorF("SiSShadowPolyText16 %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
       (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);

    return width;
}

static void
SiSShadowImageText8(
    DrawablePtr pDraw,
    GCPtr	pGC,
    int		x,
    int		y,
    int 	count,
    char	*chars
){
    BoxRec box;
    Bool boxNotEmpty = FALSE;
    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && count) {
	int top, bot, Min, Max;

	top = max(FONTMAXBOUNDS(pGC->font, ascent), FONTASCENT(pGC->font));
	bot = max(FONTMAXBOUNDS(pGC->font, descent), FONTDESCENT(pGC->font));

	Min = count * FONTMINBOUNDS(pGC->font, characterWidth);
	if(Min > 0) Min = 0;
	Max = count * FONTMAXBOUNDS(pGC->font, characterWidth);
	if(Max < 0) Max = 0;

	/* ugh */
	box.x1 = pDraw->x + x + Min +
		FONTMINBOUNDS(pGC->font, leftSideBearing);
	box.x2 = pDraw->x + x + Max +
		FONTMAXBOUNDS(pGC->font, rightSideBearing);

	box.y1 = pDraw->y + y - top;
	box.y2 = pDraw->y + y + bot;

	TRIM_BOX(box, pGC);
	if(BOX_NOT_EMPTY(box)) {
            boxNotEmpty = TRUE;
        }
    }

    (*pGC->ops->ImageText8)(pDraw, pGC, x, y, count, chars);

    if(boxNotEmpty && pPriv->postRefresh) {
        SHDEBUG(ErrorF("SiSShadowImageText8 %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
        (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static void
SiSShadowImageText16(
    DrawablePtr pDraw,
    GCPtr	pGC,
    int		x,
    int		y,
    int 	count,
    unsigned short *chars
){
    BoxRec box;
    Bool boxNotEmpty = FALSE;
    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && count) {
	int top, bot, Min, Max;

	top = max(FONTMAXBOUNDS(pGC->font, ascent), FONTASCENT(pGC->font));
	bot = max(FONTMAXBOUNDS(pGC->font, descent), FONTDESCENT(pGC->font));

	Min = count * FONTMINBOUNDS(pGC->font, characterWidth);
	if(Min > 0) Min = 0;
	Max = count * FONTMAXBOUNDS(pGC->font, characterWidth);
	if(Max < 0) Max = 0;

	/* ugh */
	box.x1 = pDraw->x + x + Min +
		FONTMINBOUNDS(pGC->font, leftSideBearing);
	box.x2 = pDraw->x + x + Max +
		FONTMAXBOUNDS(pGC->font, rightSideBearing);

	box.y1 = pDraw->y + y - top;
	box.y2 = pDraw->y + y + bot;

	TRIM_BOX(box, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    (*pGC->ops->ImageText16)(pDraw, pGC, x, y, count, chars);

    if(boxNotEmpty && pPriv->postRefresh) {
       SHDEBUG(ErrorF("SiSShadowImageText16 %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
       (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static void
SiSShadowImageGlyphBlt(
    DrawablePtr pDraw,
    GCPtr pGC,
    int x, int y,
    unsigned int nglyphInit,
    CharInfoPtr *ppciInit,
    pointer pglyphBase
){
    BoxRec box;
    Bool boxNotEmpty = FALSE;
    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && nglyphInit) {
        CharInfoPtr *ppci = ppciInit;
        unsigned int nglyph = nglyphInit;
	int top, bot, width = 0;

	top = max(FONTMAXBOUNDS(pGC->font, ascent), FONTASCENT(pGC->font));
	bot = max(FONTMAXBOUNDS(pGC->font, descent), FONTDESCENT(pGC->font));

	box.x1 = ppci[0]->metrics.leftSideBearing;
	if(box.x1 > 0) box.x1 = 0;
	box.x2 = ppci[nglyph - 1]->metrics.rightSideBearing -
		ppci[nglyph - 1]->metrics.characterWidth;
	if(box.x2 < 0) box.x2 = 0;

	box.x2 += pDraw->x + x;
	box.x1 += pDraw->x + x;

	while(nglyph--) {
	    width += (*ppci)->metrics.characterWidth;
	    ppci++;
	}

	if(width > 0)
	   box.x2 += width;
	else
	   box.x1 += width;

	box.y1 = pDraw->y + y - top;
	box.y2 = pDraw->y + y + bot;

	TRIM_BOX(box, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    (*pGC->ops->ImageGlyphBlt)(pDraw, pGC, x, y, nglyphInit,
					ppciInit, pglyphBase);

    if(boxNotEmpty && pPriv->postRefresh) {
       SHDEBUG(ErrorF("SiSShadowImageGlyphBlt %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
       (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static void
SiSShadowPolyGlyphBlt(
    DrawablePtr pDraw,
    GCPtr pGC,
    int x, int y,
    unsigned int nglyphInit,
    CharInfoPtr *ppciInit,
    pointer pglyphBase
){
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw) && nglyphInit) {
        CharInfoPtr *ppci = ppciInit;
        unsigned int nglyph = nglyphInit;

	/* ugh */
	box.x1 = pDraw->x + x + ppci[0]->metrics.leftSideBearing;
	box.x2 = pDraw->x + x + ppci[nglyph - 1]->metrics.rightSideBearing;

	if(nglyph > 1) {
	    int width = 0;

	    while(--nglyph) {
		width += (*ppci)->metrics.characterWidth;
		ppci++;
	    }

	    if(width > 0) box.x2 += width;
	    else box.x1 += width;
	}

	box.y1 = pDraw->y + y - FONTMAXBOUNDS(pGC->font, ascent);
	box.y2 = pDraw->y + y + FONTMAXBOUNDS(pGC->font, descent);

	TRIM_BOX(box, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    (*pGC->ops->PolyGlyphBlt)(pDraw, pGC, x, y, nglyphInit,
				ppciInit, pglyphBase);

    if(boxNotEmpty && pPriv->postRefresh) {
       SHDEBUG(ErrorF("SiSShadowPolyGlyphBlt %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
       (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}

static void
SiSShadowPushPixels(
    GCPtr	pGC,
    PixmapPtr	pBitMap,
    DrawablePtr pDraw,
    int	dx, int dy, int xOrg, int yOrg
){
    BoxRec box;
    Bool boxNotEmpty = FALSE;

    SIS_SHADOW_GC_OP_PROLOGUE(pGC);

    if(pPriv->enabled && SiSIsFB(pDraw, pPriv) && IS_VISIBLE(pDraw)) {
	box.x1 = xOrg;
	box.y1 = yOrg;

        if(!pGC->miTranslate) {
           box.x1 += pDraw->x;
           box.y1 += pDraw->y;
        }

	box.x2 = box.x1 + dx;
	box.y2 = box.y1 + dy;

	TRIM_BOX(box, pGC);
	if(BOX_NOT_EMPTY(box)) {
           boxNotEmpty = TRUE;
        }
    }

    (*pGC->ops->PushPixels)(pGC, pBitMap, pDraw, dx, dy, xOrg, yOrg);

    if(boxNotEmpty && pPriv->postRefresh) {
       SHDEBUG(ErrorF("SiSShadowPushPixels %d / %d %d\n", 1,
        			pDraw->type, DRAWABLE_WINDOW));
       (*pPriv->postRefresh)(pPriv->pScrn, 1, &box);
    }

    SIS_SHADOW_GC_OP_EPILOGUE(pGC);
}


GCOps SiSShadowGCOps = {
    SiSShadowFillSpans, SiSShadowSetSpans,
    SiSShadowPutImage, SiSShadowCopyArea,
    SiSShadowCopyPlane, SiSShadowPolyPoint,
    SiSShadowPolylines, SiSShadowPolySegment,
    SiSShadowPolyRectangle, SiSShadowPolyArc,
    SiSShadowFillPolygon, SiSShadowPolyFillRect,
    SiSShadowPolyFillArc, SiSShadowPolyText8,
    SiSShadowPolyText16, SiSShadowImageText8,
    SiSShadowImageText16, SiSShadowImageGlyphBlt,
    SiSShadowPolyGlyphBlt, SiSShadowPushPixels,
#ifdef NEED_LINEHELPER
    NULL,
#endif
#ifndef SISISXORGPOST112
    {NULL}		/* devPrivate */
#endif
};

#else	/* SIS_INCL_RRROT */
    int i;
#endif	/* SIS_INCL_RRROT */


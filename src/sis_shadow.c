/*
 * Copyright (C) 1999-2004 by The XFree86 Project, Inc.
 * based on code written by Mark Vojkovich
 * Copyright (C) 2003-2005 Thomas Winischhofer
 *
 * Licensed under the following terms:
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appears in all copies and that both that copyright
 * notice and this permission notice appear in supporting documentation, and
 * and that the name of the copyright holder not be used in advertising
 * or publicity pertaining to distribution of the software without specific,
 * written prior permission. The copyright holder makes no representations
 * about the suitability of this software for any purpose.  It is provided
 * "as is" without expressed or implied warranty.
 *
 * THE COPYRIGHT HOLDER DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO
 * EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sis.h"
#include "servermd.h"

void SISPointerMoved(int index, int x, int y);
void SISRefreshArea(ScrnInfoPtr pScrn, int num, BoxPtr pbox);
void SISRefreshAreaReflect(ScrnInfoPtr pScrn, int num, BoxPtr pbox);
void SISRefreshArea8(ScrnInfoPtr pScrn, int num, BoxPtr pbox);
void SISRefreshArea16(ScrnInfoPtr pScrn, int num, BoxPtr pbox);
void SISRefreshArea24(ScrnInfoPtr pScrn, int num, BoxPtr pbox);
void SISRefreshArea32(ScrnInfoPtr pScrn, int num, BoxPtr pbox);

#ifdef SIS_INCL_RRROT
int  SiS_GetAvailableRotations(ScrnInfoPtr pScrn);
Bool SiS_SetRotation(ScrnInfoPtr pScrn, int rot, int rate, int width, int height);
void SISRandRRefreshArea(ScrnInfoPtr pScrn, int num, BoxPtr pbox);
void SISDoRefreshArea(ScrnInfoPtr pScrn);
void SiSMergeBoxMissed(SISPtr pSiS, int num, BoxPtr pbox);

extern unsigned int SISAllocateFBMemory(ScrnInfoPtr pScrn, void **handle, int bytesize);
extern void	    SISFreeFBMemory(ScrnInfoPtr pScrn, void **handle);
#endif

void
SISPointerMoved(int index, int x, int y)
{
    ScrnInfoPtr pScrn = xf86Screens[index];
    SISPtr pSiS = SISPTR(pScrn);
    Bool framechanged = FALSE;

    /* Beware: This is executed asynchronously. */

    if(pSiS->DGAactive)
       return;

#ifdef SIS_INCL_RRROT
    if(pSiS->Rotate || pSiS->RRRotate & (RR_Rotate_90 | RR_Rotate_270)) {
#else
    if(pSiS->Rotate) {
#endif
       if(pScrn->frameX0 > x) {
	  pScrn->frameX0 = x;
	  pScrn->frameX1 = x + pScrn->currentMode->VDisplay - 1;
	  framechanged = TRUE ;
       }

       if(pScrn->frameX1 < x) {
	  pScrn->frameX1 = x + 1;
	  pScrn->frameX0 = x - pScrn->currentMode->VDisplay + 1;
	  framechanged = TRUE ;
       }

       if(pScrn->frameY0 > y) {
	  pScrn->frameY0 = y;
	  pScrn->frameY1 = y + pScrn->currentMode->HDisplay - 1;
	  framechanged = TRUE;
       }

       if(pScrn->frameY1 < y) {
	  pScrn->frameY1 = y;
	  pScrn->frameY0 = y - pScrn->currentMode->HDisplay + 1;
	  framechanged = TRUE;
       }

       if(framechanged && pScrn->AdjustFrame)
	  pScrn->AdjustFrame(pScrn->scrnIndex, pScrn->frameX0, pScrn->frameY0, 0);

    } else {

       (*pSiS->PointerMoved)(index, x, y);

    }
}

/* Refresh area (unreflected, unrotated) */

void
SISRefreshArea(ScrnInfoPtr pScrn, int num, BoxPtr pbox)
{
    SISPtr pSiS = SISPTR(pScrn);
    int    width, height, Bpp, FBPitch;
    CARD8  *src, *dst;

    Bpp = pSiS->CurrentLayout.bytesPerPixel;
    FBPitch = BitmapBytePad(pScrn->displayWidth * pSiS->CurrentLayout.bitsPerPixel);

    while(num--) {

       width = (pbox->x2 - pbox->x1) * Bpp;
       height = pbox->y2 - pbox->y1;
       src = pSiS->ShadowPtr + (pbox->y1 * pSiS->ShadowPitch) +  (pbox->x1 * Bpp);
       dst = pSiS->FbBase + (pbox->y1 * FBPitch) + (pbox->x1 * Bpp);

       while(height--) {
          SiSMemCopyToVideoRam(pSiS, dst, src, width);
	  dst += FBPitch;
	  src += pSiS->ShadowPitch;
       }

       pbox++;
    }
}

/* RefreshArea for reflection */

static void
SISRealRefreshAreaReflect(ScrnInfoPtr pScrn, int num, BoxPtr pbox,
			CARD8 *sourcePtr, CARD8 *destPtr, int srcPitch, int dstWidth)
{
    SISPtr pSiS = SISPTR(pScrn);
    int    width, height, Bpp, FBPitch, twidth;
    CARD8  *src, *dst, *tdst, *tsrc;
    CARD16 *tdst16, *tsrc16;
    CARD32 *tdst32, *tsrc32;

    Bpp = pSiS->CurrentLayout.bytesPerPixel;
    FBPitch = dstWidth * Bpp;

    while(num--) {
       width = (pbox->x2 - pbox->x1) * Bpp;
       height = pbox->y2 - pbox->y1;
       src = sourcePtr + (pbox->y1 * srcPitch) + (pbox->x1 * Bpp);
       dst = destPtr;
       switch(pSiS->Reflect) {
       case 1:	/* x */
	  dst += (pbox->y1 * FBPitch) + ((pScrn->virtualX - pbox->x1 - 1) * Bpp);
	  switch(Bpp) {
	     case 1:
		while(height--) {
		   tdst = dst;
		   tsrc = src;
		   twidth = width;
		   while(twidth--) *tdst-- = *tsrc++;
		   dst += FBPitch;
		   src += srcPitch;
		}
		break;
	     case 2:
		width >>= 1;
		while(height--) {
		   tdst16 = (CARD16 *)dst;
		   tsrc16 = (CARD16 *)src;
		   twidth = width;
		   while(twidth--) *tdst16-- = *tsrc16++;
		   dst += FBPitch;
		   src += srcPitch;
		}
		break;
	     case 4:
		width >>= 2;
		while(height--) {
		   tdst32 = (CARD32 *)dst;
		   tsrc32 = (CARD32 *)src;
		   twidth = width;
		   while(twidth--) *tdst32-- = *tsrc32++;
		   dst += FBPitch;
		   src += srcPitch;
		}
	  }
	  break;
       case 2:	/* y */
	  dst += ((pScrn->virtualY - pbox->y1 - 1) * FBPitch) + (pbox->x1 * Bpp);
	  while(height--) {
	     SiSMemCopyToVideoRam(pSiS, dst, src, width);
	     dst -= FBPitch;
	     src += srcPitch;
	  }
	  break;
       case 3:	/* x + y */
	  dst += ((pScrn->virtualY - pbox->y1 - 1) * FBPitch) + ((pScrn->virtualX - pbox->x1 - 1) * Bpp);
	  switch(Bpp) {
	     case 1:
		while(height--) {
		   tdst = dst;
		   tsrc = src;
		   twidth = width;
		   while(twidth--) *tdst-- = *tsrc++;
		   dst -= FBPitch;
		   src += srcPitch;
		}
		break;
	     case 2:
	        width >>= 1;
		while(height--) {
		   tdst16 = (CARD16 *)dst;
		   tsrc16 = (CARD16 *)src;
		   twidth = width;
		   while(twidth--) *tdst16-- = *tsrc16++;
		   dst -= FBPitch;
		   src += srcPitch;
		}
		break;
	     case 4:
		width >>= 2;
		while(height--) {
		   tdst32 = (CARD32 *)dst;
		   tsrc32 = (CARD32 *)src;
		   twidth = width;
		   while(twidth--) *tdst32-- = *tsrc32++;
		   dst -= FBPitch;
		   src += srcPitch;
		}
		break;
	  }
       }
       pbox++;
    }
}

void
SISRefreshAreaReflect(ScrnInfoPtr pScrn, int num, BoxPtr pbox)
{
    SISPtr pSiS = SISPTR(pScrn);

    SISRealRefreshAreaReflect(pScrn, num, pbox, (CARD8 *)pSiS->ShadowPtr,
    		(CARD8 *)pSiS->FbBase, pSiS->ShadowPitch, pScrn->displayWidth);
}

/* RefreshArea()s for rotation */

static void
SISRealRefreshArea8(ScrnInfoPtr pScrn, int num, BoxPtr pbox,
		CARD8 *sourcePtr, CARD8 *destPtr, int srcPitch, int dstWidth)
{
    SISPtr pSiS = SISPTR(pScrn);
    int    count, width, height, y1, y2;
    CARD8  *dstPtr, *srcPtr, *src;
    CARD32 *dst;

    srcPitch *= (-pSiS->Rotate);

    while(num--) {
       width = pbox->x2 - pbox->x1;
       y1 = pbox->y1 & ~3;
       y2 = (pbox->y2 + 3) & ~3;
       height = (y2 - y1) >> 2;  /* in dwords */

       if(pSiS->Rotate == 1) {
	  dstPtr = destPtr + (pbox->x1 * dstWidth) + pScrn->virtualX - y2;
	  srcPtr = sourcePtr + ((1 - y2) * srcPitch) + pbox->x1;
       } else {
	  dstPtr = destPtr +  ((pScrn->virtualY - pbox->x2) * dstWidth) + y1;
	  srcPtr = sourcePtr + (y1 * srcPitch) + pbox->x2 - 1;
       }

       while(width--) {
	  src = srcPtr;
	  dst = (CARD32 *)dstPtr;
	  count = height;
	  while(count--) {
	     *(dst++) = src[0]                    |
		        (src[srcPitch]     <<  8) |
		        (src[srcPitch * 2] << 16) |
			(src[srcPitch * 3] << 24);
	     src += (srcPitch * 4);
	  }
	  srcPtr += pSiS->Rotate;
	  dstPtr += dstWidth;
       }

       pbox++;
    }
}

void
SISRefreshArea8(ScrnInfoPtr pScrn, int num, BoxPtr pbox)
{
    SISPtr pSiS = SISPTR(pScrn);

    SISRealRefreshArea8(pScrn, num, pbox, (CARD8 *)pSiS->ShadowPtr,
	(CARD8 *)pSiS->FbBase, pSiS->ShadowPitch, pScrn->displayWidth);
}

static void
SISRealRefreshArea16(ScrnInfoPtr pScrn, int num, BoxPtr pbox,
		CARD16 *sourcePtr, CARD16 *destPtr, int srcPitch, int dstWidth)
{
    SISPtr pSiS = SISPTR(pScrn);
    int count, width, height, y1, y2;
    CARD16 *dstPtr, *srcPtr, *src;
    CARD32 *dst;

    srcPitch = -pSiS->Rotate * srcPitch >> 1;

    while(num--) {
       width = pbox->x2 - pbox->x1;
       y1 = pbox->y1 & ~1;
       y2 = (pbox->y2 + 1) & ~1;
       height = (y2 - y1) >> 1;  /* in dwords */

       if(pSiS->Rotate == 1) {
	  dstPtr = destPtr + (pbox->x1 * dstWidth) + pScrn->virtualX - y2;
	  srcPtr = sourcePtr + ((1 - y2) * srcPitch) + pbox->x1;
       } else {
	  dstPtr = destPtr + ((pScrn->virtualY - pbox->x2) * dstWidth) + y1;
	  srcPtr = sourcePtr + (y1 * srcPitch) + pbox->x2 - 1;
       }

       while(width--) {
	  src = srcPtr;
	  dst = (CARD32 *)dstPtr;
	  count = height;
	  while(count--) {
	     *(dst++) = src[0] | (src[srcPitch] << 16);
	     src += (srcPitch * 2);
	  }
	  srcPtr += pSiS->Rotate;
	  dstPtr += dstWidth;
       }

       pbox++;
    }
}

void
SISRefreshArea16(ScrnInfoPtr pScrn, int num, BoxPtr pbox)
{
    SISPtr pSiS = SISPTR(pScrn);

    SISRealRefreshArea16(pScrn, num, pbox, (CARD16 *)pSiS->ShadowPtr,
	(CARD16 *)pSiS->FbBase, pSiS->ShadowPitch, pScrn->displayWidth);
}

/* this one could be faster */
void
SISRefreshArea24(ScrnInfoPtr pScrn, int num, BoxPtr pbox)
{
    SISPtr pSiS = SISPTR(pScrn);
    int    count, width, height, y1, y2, dstPitch, srcPitch;
    CARD8  *dstPtr, *srcPtr, *src;
    CARD32 *dst;

    dstPitch = BitmapBytePad(pScrn->displayWidth * 24);
    srcPitch = -pSiS->Rotate * pSiS->ShadowPitch;

    while(num--) {
       width = pbox->x2 - pbox->x1;
       y1 = pbox->y1 & ~3;
       y2 = (pbox->y2 + 3) & ~3;
       height = (y2 - y1) >> 2;  /* blocks of 3 dwords */

       if(pSiS->Rotate == 1) {
	  dstPtr = pSiS->FbBase + (pbox->x1 * dstPitch) + ((pScrn->virtualX - y2) * 3);
	  srcPtr = pSiS->ShadowPtr + ((1 - y2) * srcPitch) + (pbox->x1 * 3);
       } else {
	  dstPtr = pSiS->FbBase + ((pScrn->virtualY - pbox->x2) * dstPitch) + (y1 * 3);
	  srcPtr = pSiS->ShadowPtr + (y1 * srcPitch) + (pbox->x2 * 3) - 3;
       }

       while(width--) {
	  src = srcPtr;
	  dst = (CARD32 *)dstPtr;
	  count = height;
	  while(count--) {
	     dst[0] = src[0]         |
		      (src[1] << 8)  |
		      (src[2] << 16) |
		      (src[srcPitch] << 24);
	     dst[1] = src[srcPitch + 1]         |
		      (src[srcPitch + 2] << 8)  |
		      (src[srcPitch * 2] << 16) |
		      (src[(srcPitch * 2) + 1] << 24);
	     dst[2] = src[(srcPitch * 2) + 2]         |
		      (src[srcPitch * 3] << 8)        |
		      (src[(srcPitch * 3) + 1] << 16) |
		      (src[(srcPitch * 3) + 2] << 24);
	     dst += 3;
	     src += (srcPitch << 2);
	  }
	  srcPtr += pSiS->Rotate * 3;
	  dstPtr += dstPitch;
       }

       pbox++;
    }
}

static void
SISRealRefreshArea32(ScrnInfoPtr pScrn, int num, BoxPtr pbox,
		CARD32 *sourcePtr, CARD32 *destPtr, int srcPitch, int dstWidth)
{
    SISPtr pSiS = SISPTR(pScrn);
    int    count, width, height;
    CARD32 *dstPtr, *srcPtr, *src, *dst;

    srcPitch = -pSiS->Rotate * srcPitch >> 2;

    while(num--) {
       width = pbox->x2 - pbox->x1;
       height = pbox->y2 - pbox->y1;

       if(pSiS->Rotate == 1) {
	  dstPtr = destPtr + (pbox->x1 * dstWidth) + pScrn->virtualX - pbox->y2;
	  srcPtr = sourcePtr + ((1 - pbox->y2) * srcPitch) + pbox->x1;
       } else {
	  dstPtr = destPtr + ((pScrn->virtualY - pbox->x2) * dstWidth) + pbox->y1;
	  srcPtr = sourcePtr + (pbox->y1 * srcPitch) + pbox->x2 - 1;
       }

       while(width--) {
	  src = srcPtr;
	  dst = dstPtr;
	  count = height;
	  while(count--) {
	     *(dst++) = *src;
	     src += srcPitch;
	  }
	  srcPtr += pSiS->Rotate;
	  dstPtr += dstWidth;
       }

       pbox++;
    }
}

void
SISRefreshArea32(ScrnInfoPtr pScrn, int num, BoxPtr pbox)
{
    SISPtr pSiS = SISPTR(pScrn);

    SISRealRefreshArea32(pScrn, num, pbox, (CARD32 *)pSiS->ShadowPtr,
	(CARD32 *)pSiS->FbBase, pSiS->ShadowPitch, pScrn->displayWidth);
}

#ifdef SIS_INCL_RRROT
/********************************************/
/*          RandR rotation support          */
/********************************************/

static Bool
SiSHandleRotation(ScrnInfoPtr pScrn, int rot)
{
   SISPtr pSiS = SISPTR(pScrn);
   unsigned int shadowoffs;
   int newsize;

   if(rot == RR_Rotate_0) {

      pSiS->RRRotate = rot;

      pSiS->rrfboffs = 0;

      (*pScrn->AdjustFrame)(pScrn->pScreen->myNum, pScrn->frameX0, pScrn->frameY0, 0);

      SISFreeFBMemory(pScrn, &pSiS->rrmemhandle);

   } else {

      /* Allocate maximum possible amount of VRAM. Reason being that
       * if RandR later changes screen size without also changing
       * rotation, we are never informed. (So we don't use pScrn->virtual
       * but the values we saved at the end of ScreenInit().)
       *
       * The shadow framebuffer is always of virtualX * virtualY * bpp
       * even if rotated 90/270 degrees. The pitch is always the virtualX
       * value because RandR
       * - does not swap virtualX and virtualY (and this would not have
       *   any effect either, since we use our private values)
       * - the screen dimensions (= the way the source is painted into
       *   memory) changes as X any Y dimensions are swapped.
       * As a result, the pitch is ALWAYS virtualX.
       */

      pSiS->RRShadowWidth = (pSiS->virtualX + 7) & ~7;

      newsize = pSiS->RRShadowWidth * pSiS->CurrentLayout.bytesPerPixel * pSiS->virtualY;

      if(!(shadowoffs = SISAllocateFBMemory(pScrn, &pSiS->rrmemhandle, newsize)))
         return FALSE;

      pSiS->rrfboffs = shadowoffs >> 2;
      pSiS->RRShadowPtr = pSiS->FbBase + shadowoffs;
      pSiS->RRShadowPitch = pSiS->RRShadowWidth * pSiS->CurrentLayout.bytesPerPixel;

      memset(pSiS->RRShadowPtr, 0, newsize);

      pSiS->RRRotate = rot;

      (*pScrn->AdjustFrame)(pScrn->pScreen->myNum, pScrn->frameX0, pScrn->frameY0, 0);

   }

   /* Cause/force reload of cursor image: This is done by the hack below now */

   /* We need to update the Xinerama info here because
    * if the user only changes the rotation but not the
    * display mode, our switchmode() isn't called.
    */
#if defined(SISMERGED) && defined(SISXINERAMA)
   if(pSiS->MergedFB) {
      SiSUpdateXineramaScreenInfo(pScrn);
   }
#endif

   return TRUE;
}

static Bool
SiSCheckRotation(SISPtr pSiS)
{
   switch(pSiS->RRRotate) {
   case 0:
   case RR_Rotate_0:
      return TRUE;

   default:
      if(!pSiS->rrmemhandle) {
         return SiSHandleRotation(pSiS->pScrn, pSiS->RRRot);
      }
   }
   return TRUE;
}

static void
SISRealRandRRefreshArea(ScrnInfoPtr pScrn, BoxPtr pbox)
{
   SISPtr pSiS = SISPTR(pScrn);

   switch(pSiS->RRRotate) {

   case RR_Rotate_90:
      pSiS->Rotate = 1;
      if(!(*pSiS->AccelerateRotate90)(pScrn, 1, pbox,
					0, pSiS->rrfboffs << 2,
					pSiS->scrnOffset, pSiS->RRShadowPitch)) {
	 (*pSiS->SyncAccel)(pScrn);
	 switch(pSiS->CurrentLayout.bytesPerPixel) {
	 case 1:
	    SISRealRefreshArea8(pScrn, 1, pbox,
			(CARD8 *)pSiS->FbBase, (CARD8 *)pSiS->RRShadowPtr,
			pSiS->scrnOffset, pSiS->RRShadowWidth);
	    break;
	 case 2:
	    SISRealRefreshArea16(pScrn, 1, pbox,
			(CARD16 *)pSiS->FbBase, (CARD16 *)pSiS->RRShadowPtr,
			pSiS->scrnOffset, pSiS->RRShadowWidth);
	    break;
	 case 4:
	    SISRealRefreshArea32(pScrn, 1, pbox,
			(CARD32 *)pSiS->FbBase, (CARD32 *)pSiS->RRShadowPtr,
			pSiS->scrnOffset, pSiS->RRShadowWidth);
	    break;
	 }
      }
      pSiS->Rotate = 0;
      break;

   case RR_Rotate_180:
      pSiS->Reflect = 3;
      if(!(*pSiS->AccelerateReflect)(pScrn, 1, pbox,
					0, pSiS->rrfboffs << 2,
					pSiS->scrnOffset, pSiS->RRShadowPitch)) {
         (*pSiS->SyncAccel)(pScrn);
         SISRealRefreshAreaReflect(pScrn, 1, pbox,
			(CARD8 *)pSiS->FbBase, (CARD8 *)pSiS->RRShadowPtr,
			pSiS->scrnOffset, pSiS->RRShadowWidth);
      }
      pSiS->Reflect = 0;
      break;

   case RR_Rotate_270:
      pSiS->Rotate = -1;
      if(!(*pSiS->AccelerateRotate270)(pScrn, 1, pbox,
					0, pSiS->rrfboffs << 2,
					pSiS->scrnOffset, pSiS->RRShadowPitch)) {
	 (*pSiS->SyncAccel)(pScrn);
	 switch(pSiS->CurrentLayout.bytesPerPixel) {
	 case 1:
	    SISRealRefreshArea8(pScrn, 1, pbox,
			(CARD8 *)pSiS->FbBase, (CARD8 *)pSiS->RRShadowPtr,
			pSiS->scrnOffset, pSiS->RRShadowWidth);
	    break;
	 case 2:
	    SISRealRefreshArea16(pScrn, 1, pbox,
			(CARD16 *)pSiS->FbBase, (CARD16 *)pSiS->RRShadowPtr,
			pSiS->scrnOffset, pSiS->RRShadowWidth);
	    break;
	 case 4:
	    SISRealRefreshArea32(pScrn, 1, pbox,
			(CARD32 *)pSiS->FbBase, (CARD32 *)pSiS->RRShadowPtr,
			pSiS->scrnOffset, pSiS->RRShadowWidth);
	    break;
         }
      }
      pSiS->Rotate = 0;
      break;
   }
}

void
SiSMergeBoxMissed(SISPtr pSiS, int num, BoxPtr pbox)
{
   if(!num) return;

   if(!pSiS->RRHaveMissed) {
      pSiS->RRMissedBoxes.x1 = pbox->x1;
      pSiS->RRMissedBoxes.y1 = pbox->y1;
      pSiS->RRMissedBoxes.x2 = pbox->x2;
      pSiS->RRMissedBoxes.y2 = pbox->y2;
      pbox++;
      num--;
      pSiS->RRHaveMissed = TRUE;
   }

   while(num--) {
      if(pbox->y1 < pSiS->RRMissedBoxes.y1) {
         pSiS->RRMissedBoxes.y1 = pbox->y1;
      }
      if(pbox->x1 < pSiS->RRMissedBoxes.x1) {
         pSiS->RRMissedBoxes.x1 = pbox->x1;
      }
      if(pbox->y2 > pSiS->RRMissedBoxes.y2) {
         pSiS->RRMissedBoxes.y2 = pbox->y2;
      }
      if(pbox->x2 > pSiS->RRMissedBoxes.x2) {
         pSiS->RRMissedBoxes.x2 = pbox->x2;
      }
      pbox++;
   }
}

static Bool
SISCheckBox(SISPtr pSiS, BoxPtr pbox)
{
   /* Basically, there is no need to check the box
    * for validity, since our own shadowfb layer makes
    * sure that we draw only into the real front
    * buffer. This was a concern earlier with Composite
    * which uses the calls wrapped by shadowfb also
    * for rendering off-screen (which lead to "bad"
    * coordinates here). The remains below are only
    * an expression of paranoia.
    */
   if(pbox->x1 < 0) pbox->x1 = 0;
   if(pbox->y1 < 0) pbox->y1 = 0;
   if(pbox->x1 >= pbox->x2) return FALSE;
   if(pbox->y1 >= pbox->y2) return FALSE;
   return TRUE;
}

void
SISRandRRefreshArea(ScrnInfoPtr pScrn, int num, BoxPtr pbox)
{
   SISPtr pSiS = SISPTR(pScrn);

   switch(pSiS->RRRotate) {
   case 0:
   case RR_Rotate_0:
      return;

   default:
      SiSMergeBoxMissed(pSiS, num, pbox);
   }
}

void
SISDoRefreshArea(ScrnInfoPtr pScrn)
{
   SISPtr pSiS = SISPTR(pScrn);

   /* This is called from the BlockHandler */

   if(!pScrn->vtSema)
      return;

   switch(pSiS->RRRotate) {
   case 0:
   case RR_Rotate_0:
      return;

   default:
      if(!pSiS->RRHaveMissed) return;

      if(!(SiSCheckRotation(pSiS))) {
	 if(!pSiS->RRErrorShown) {
	    pSiS->RRErrorShown = TRUE;
	    xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		"*** Panic: Not enough video RAM for display rotation!\n");
	 }
	 return;
      }

      if(SISCheckBox(pSiS, &pSiS->RRMissedBoxes)) {
	 SISRealRandRRefreshArea(pScrn, &pSiS->RRMissedBoxes);
      }

      pSiS->RRHaveMissed = FALSE;
   }
}

int
SiS_GetAvailableRotations(ScrnInfoPtr pScrn)
{
   SISPtr pSiS = SISPTR(pScrn);

   if(pSiS->SupportRRRotation) {

      int flags = RR_Rotate_0 | RR_Rotate_180;

      if(pSiS->SupportRRRotation90270)
         flags |=  RR_Rotate_90 | RR_Rotate_270;

      return flags;

   } else {

      return RR_Rotate_0;

   }
}

Bool
SiS_SetRotation(ScrnInfoPtr pScrn, int rot, int rate, int width, int height)
{
   SISPtr pSiS = SISPTR(pScrn);
   ScreenPtr pScreen = screenInfo.screens[pScrn->scrnIndex];
   Bool ret = TRUE;

   pSiS->RRRot = rot;
   pSiS->RRWidth = width;
   pSiS->RRHeight = height;
   pSiS->RRRate = rate;

   pSiS->RRHaveMissed = FALSE;
   pSiS->RRErrorShown = FALSE;

   switch(rot) {
   case 0:
   case RR_Rotate_0:
      SiSShadowFBEnableDisable(pScreen, FALSE);
      break;
   default:
      SiSShadowFBEnableDisable(pScreen, TRUE);
   }

   if(!(SiSHandleRotation(pScrn, rot))) {
      SiSShadowFBEnableDisable(pScreen, FALSE);
      ret = FALSE;
   }

   /* What follows is a bad hack (part 1):
    * Preface: xf86Randr.c calls our RR_SET_CONFIG first,
    * and then xf86SwitchMode(). xf86SwitchMode() itself
    * calls our SISSwitchMode() if pScrn->currentMode
    * != the mode to set. Hence, if the user only changed
    * the rotation without at the same time changing the
    * display size, our SISSwitchMode() never gets called.
    * In order to force the cursor to be updated and to
    * recalc our DPI values (in MergedFB mode), we need to
    * force RandR to actually call our SwitchMode()
    * despite pScrn->currentMode being the current mode.
    * (We can't do this here because RandR would overwrite
    * the DPI values immediately.)
    */
   if(pScrn->currentMode == &pSiS->PseudoMode) {
      /* If we get called a second time without SISSwitchMode()
       * being called in the meantime, this means that something
       * went wrong in RRSetConfig and we need to reset our rotation.
       * Hence, we just reset pScrn->currentMode to its original
       * value and don't do anything further in the DPI regard.
       */
      pSiS->RotationChanged = FALSE;
      pScrn->currentMode = pSiS->CurrentLayout.mode;
   } else if(ret) {
      /* Otherwise, we copy the current mode to a different location
       * so that "mode != pScrn->currentMode" in xf86SwitchMode()
       * which leads to our very own SIS SwitchMode() actually being
       * called (which undoes this change, see sis_driver.c).
       */
      pSiS->RotationChanged = TRUE;
      memcpy(&pSiS->PseudoMode, pScrn->currentMode, sizeof(DisplayModeRec));
      pScrn->currentMode = &pSiS->PseudoMode;
   }

   return ret;
}

#endif






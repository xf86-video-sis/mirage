/*
 * Configurable compile-time options
 *
 * Copyright (C) 2001-2005 by Thomas Winischhofer, Vienna, Austria
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1) Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2) Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3) The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:   Thomas Winischhofer <thomas@winischhofer.net>
 *
 */

#undef SISDUALHEAD
#undef SISMERGED
#undef SISXINERAMA
#undef SIS_ARGB_CURSOR
#undef SISVRAMQ
#undef INCL_YUV_BLIT_ADAPTOR
#undef SIS_USE_XAA
#undef SIS_USE_EXA

/* Configurable stuff: ------------------------------------- */

#define SISDUALHEAD		/* Include Dual Head support  */

#define SISMERGED		/* Include Merged-FB support */

#undef SISXINERAMA
#ifdef SISMERGED
#define SISXINERAMA		/* Include SiS Pseudo-Xinerama support for MergedFB mode */
#endif

#if 1
#define SIS_ARGB_CURSOR		/* Include support for color hardware cursors */
#endif

#if 1
#define SISVRAMQ		/* Use VRAM queue mode support on 315+ series */
#endif

#undef INCL_YUV_BLIT_ADAPTOR
#ifdef SISVRAMQ
#if 1
#define INCL_YUV_BLIT_ADAPTOR	/* Include support for YUV->RGB blit adaptors (VRAM queue mode only) */
#endif
#endif

#if 1
#define SIS_USE_XAA		/* Include support for XAA */
#endif

#ifdef SISVRAMQ
#ifdef XORG_VERSION_CURRENT
#if defined(SIS_HAVE_EXA) || (defined(XF86EXA) && (XF86EXA != 0))
#if 1
#define SIS_USE_EXA		/* Include support for EXA */
#endif
#endif
#endif
#endif

#ifndef SIS_FREE_VERSION	/* ---------------------------------------- */

#undef SIS_INCL_MERGEDFB2
#undef SIS_INCL_DYNMODELIST
#undef SIS_INCL_RRROT
#undef SIS_INCL_RANDR_RESIZE
#undef SISDEINT
#undef SIS_INCL_CUSTOM_ICOP
#undef SIS_INCL_SCALING_VIDEOBLIT
#undef SIS_CP

#if 1
#define SIS_INCL_MERGEDFB2	/* Include support for advanced mergedfb mode */
#endif

#if 1
#define SIS_INCL_DYNMODELIST	/* Include support for dynamic modelist */
#endif

#if defined(SIS_HAVE_RR_FUNC) && defined(RANDR)
#if 1
#define SIS_INCL_RRROT		/* Include RandR rotation support */
#endif
#endif

#ifdef SIS_HAVE_RANDR_SIZE_PATCH
#ifdef SIS_INCL_DYNMODELIST
#if 1
#define SIS_INCL_RANDR_RESIZE	/* Include RandR resizing patch (aka screen growing support) */
#endif
#endif
#endif

#if 1
#define SIS_INCL_SCALING_VIDEOBLIT
#endif

#if 0
#define SISDEINT		/* Include Xv deinterlacer support (not functional yet!) */
#endif

#if 0
#define SIS_INCL_CUSTOM_ICOP	/* Include some custom machine code/data for ICOP 607x */
#endif

#if 0
#include "siscp.h"		/* Include TV copy protection code */
#endif

#ifdef SIS_INCL_RRROT
#define SIS_INCL_SBB
#endif

#ifdef SIS_INCL_SCALING_VIDEOBLIT
#ifndef SIS_INCL_SBB
#define SIS_INCL_SBB
#endif
#endif

#if defined(SIS_INCL_RRROT) || defined(SIS_INCL_RANDR_RESIZE)
#ifndef SIS_INCL_DYNMODELIST
#define SIS_INCL_DYNMODELIST
#endif
#endif

#define LICENSEE "X.org"
#define LICENCODED 0xaa,0x32,0xf9,0x8f,0xcc,0xd3,0xc4
#define LICCHECKSUM 230952
#define LICCHECKLIMIT 77280

#endif	/* --------------------------------------------------------- */
/* End of configurable stuff --------------------------------- */



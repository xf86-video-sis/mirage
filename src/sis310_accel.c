/*
 * 2D Acceleration for SiS 315, 330 and 340 series
 *
 * Copyright (C) 2001-2005 by Thomas Winischhofer, Vienna, Austria
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1) Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2) Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3) The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * Author:  	Thomas Winischhofer <thomas@winischhofer.net>
 *
 * 2003/08/18: Rewritten for using VRAM command queue
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sis.h"
#define SIS_NEED_MYMMIO
#define SIS_NEED_ACCELBUF
#include "sis_regs.h"
#include "sis310_accel.h"

#if 0
#define ACCELDEBUG
#endif

#define FBOFFSET 	(pSiS->dhmOffset)

#define DEV_HEIGHT	0xfff	/* "Device height of destination bitmap" */

#undef SIS_NEED_ARRAY

/* For XAA */

#ifdef SIS_USE_XAA

#define INCL_RENDER	/* Use/Don't use RENDER extension acceleration */

#ifdef INCL_RENDER
# ifdef RENDER
#  include "mipict.h"
#  include "dixstruct.h"
#  define SIS_NEED_ARRAY
#  undef SISNEWRENDER
#  ifdef XORG_VERSION_CURRENT
#   if XORG_VERSION_CURRENT > XORG_VERSION_NUMERIC(6,7,0,0,0)
#    define SISNEWRENDER
#   endif
#  endif
# endif
#endif

#endif /* XAA */

/* For EXA */

#ifdef SIS_USE_EXA
#if 0
#define SIS_HAVE_COMPOSITE		/* Have our own EXA composite */
#endif
#ifdef SIS_HAVE_COMPOSITE
#if 0
#ifndef SIS_NEED_ARRAY
#define SIS_NEED_ARRAY
#endif
#endif
#endif
#endif

#if defined(SIS_USE_XAA) && defined(INCL_RENDER) && defined(RENDER)  /* XAA */
static CARD32 SiSAlphaTextureFormats[2] = { PICT_a8      , 0 };
static CARD32 SiSTextureFormats[2]      = { PICT_a8r8g8b8, 0 };
#ifdef SISNEWRENDER
static CARD32 SiSDstTextureFormats16[2] = { PICT_r5g6b5  , 0 };
static CARD32 SiSDstTextureFormats32[3] = { PICT_x8r8g8b8, PICT_a8r8g8b8, 0 };
#endif
#endif

#ifdef SIS_USE_EXA		/* EXA */
void SiSScratchSave(ScreenPtr pScreen, ExaOffscreenArea *area);
Bool SiSUploadToScreen(PixmapPtr pDst, int x, int y, int w, int h, char *src, int src_pitch);
Bool SiSUploadToScratch(PixmapPtr pSrc, PixmapPtr pDst);
Bool SiSDownloadFromScreen(PixmapPtr pSrc, int x, int y, int w, int h, char *dst, int dst_pitch);
#endif /* EXA */

void SISWriteBlitPacket(SISPtr pSiS, CARD32 *packet);

extern unsigned char SiSGetCopyROP(int rop);
extern unsigned char SiSGetPatternROP(int rop);

volatile CARD32 dummybuf;

#ifdef SIS_NEED_ARRAY
#if XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,0,0,0)
#define SiSRenderOpsMAX 0x2b
#else
#define SiSRenderOpsMAX 0x0f
#endif
static const CARD8 SiSRenderOps[] = {	/* PictOpXXX 1 = supported, 0 = unsupported */
     1, 1, 1, 1,
     0, 0, 0, 0,
     0, 0, 0, 0,
     0, 0, 0, 0,
     1, 1, 1, 0,
     0, 0, 0, 0,
     0, 0, 0, 0,
     0, 0, 0, 0,
     1, 1, 1, 0,
     0, 0, 0, 0,
     0, 0, 0, 0,
     0, 0, 0, 0
};
#endif /* NEED ARRAY */

#ifdef SIS_NEED_ARRAY
static void
SiSCalcRenderAccelArray(ScrnInfoPtr pScrn)
{
	SISPtr  pSiS = SISPTR(pScrn);
#ifdef SISDUALHEAD
	SISEntPtr pSiSEnt = pSiS->entityPrivate;;
#endif

	if(((pScrn->bitsPerPixel == 16) || (pScrn->bitsPerPixel == 32)) && pSiS->doRender) {
	   int i, j;
#ifdef SISDUALHEAD
	   if(pSiSEnt) pSiS->RenderAccelArray = pSiSEnt->RenderAccelArray;
#endif
	   if(!pSiS->RenderAccelArray) {
	      if((pSiS->RenderAccelArray = xnfcalloc(65536, 1))) {
#ifdef SISDUALHEAD
	         if(pSiSEnt) pSiSEnt->RenderAccelArray = pSiS->RenderAccelArray;
#endif
		 for(i = 0; i < 256; i++) {
		    for(j = 0; j < 256; j++) {
		       pSiS->RenderAccelArray[(i << 8) + j] = (i * j) / 255;
		    }
		 }
	      }
	   }
	}
}
#endif

#ifdef SIS_USE_EXA
void
SiSScratchSave(ScreenPtr pScreen, ExaOffscreenArea *area)
{
	SISPtr pSiS = SISPTR(xf86Screens[pScreen->myNum]);

	pSiS->exa_scratch = NULL;
}
#endif

static void
SiSSync(ScrnInfoPtr pScrn)
{
	SISPtr pSiS = SISPTR(pScrn);

#ifdef SIS_USE_XAA
	if(!pSiS->useEXA) {
	   pSiS->DoColorExpand = FALSE;
	}
#endif

	pSiS->alphaBlitBusy = FALSE;

	SiSIdle
}

#ifdef SISVRAMQ
static CARD32
SISSiSUpdateQueue(SISPtr pSiS, CARD32 ttt, pointer tt)
{
	SiSUpdateQueue

	return ttt;
}
#endif

static void
SiSSyncAccel(ScrnInfoPtr pScrn)
{
	SISPtr pSiS = SISPTR(pScrn);

	if(!pSiS->NoAccel) SiSSync(pScrn);
}

static void
SiSInitializeAccelerator(ScrnInfoPtr pScrn)
{
	SISPtr  pSiS = SISPTR(pScrn);

#ifdef SIS_USE_XAA
	pSiS->DoColorExpand = FALSE;
#endif
	pSiS->alphaBlitBusy = FALSE;

	if(!pSiS->NoAccel) {

#ifndef SISVRAMQ
	   if(pSiS->ChipFlags & SiSCF_Integrated) {
	      CmdQueLen = 0;
	   } else {
	      CmdQueLen = ((128 * 1024) / 4) - 64;
	   }
#endif

#ifdef SISVRAMQ
	   if(pSiS->ChipFlags & SiSCF_DualPipe) {
	      SiSSync(pScrn);
	      SiSDualPipe(1);	/* 1 = disable, 0 = enable */
	      SiSSync(pScrn);
	   }
#endif

	}
}

static void
SiSSetupForScreenToScreenCopy(ScrnInfoPtr pScrn,
			int xdir, int ydir, int rop,
			unsigned int planemask, int trans_color)
{
	SISPtr  pSiS = SISPTR(pScrn);

#ifdef SISVRAMQ
	SiSSetupDSTColorDepth(pSiS->SiS310_AccelDepth);
	SiSCheckQueue(16 * 2);
	SiSSetupSRCPitchDSTRect(pSiS->scrnOffset, pSiS->scrnOffset, DEV_HEIGHT)
#else
	SiSSetupDSTColorDepth(pSiS->DstColor);
	SiSSetupSRCPitch(pSiS->scrnOffset)
	SiSSetupDSTRect(pSiS->scrnOffset, DEV_HEIGHT)
#endif

	if(trans_color != -1) {
	   SiSSetupROP(0x0A)
	   SiSSetupSRCTrans(trans_color)
	   SiSSetupCMDFlag(TRANSPARENT_BITBLT)
	} else {
	   SiSSetupROP(SiSGetCopyROP(rop))
	   /* Set command - not needed, both 0 */
	   /* SiSSetupCMDFlag(BITBLT | SRCVIDEO) */
	}

#ifndef SISVRAMQ
	SiSSetupCMDFlag(pSiS->SiS310_AccelDepth)
#endif

#ifdef SISVRAMQ
	SiSSyncWP
#endif

	/* The chip is smart enough to know the direction */
}

static void
SiSSubsequentScreenToScreenCopy(ScrnInfoPtr pScrn,
			int src_x, int src_y, int dst_x, int dst_y,
			int width, int height)
{
	SISPtr pSiS = SISPTR(pScrn);
	CARD32 srcbase, dstbase;
	int    mymin, mymax;

	srcbase = dstbase = 0;
	mymin = min(src_y, dst_y);
	mymax = max(src_y, dst_y);

	/* Libxaa.a has a bug: The tilecache cannot operate
	 * correctly if there are 512x512 slots, but no 256x256
	 * slots. This leads to catastrophic data fed to us.
	 * Filter this out here and warn the user.
	 * Fixed in 4.3.99.10 (?) and Debian's 4.3.0.1
	 */
#if (XF86_VERSION_CURRENT < XF86_VERSION_NUMERIC(4,3,99,10,0)) && (XF86_VERSION_CURRENT != XF86_VERSION_NUMERIC(4,3,0,1,0))
	if((src_x < 0)  ||
	   (dst_x < 0)  ||
	   (src_y < 0)  ||
	   (dst_y < 0)  ||
	   (width <= 0) ||
	   (height <= 0)) {
	   xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		"BitBlit fatal error: Illegal coordinates:\n");
	   xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
	        "Source x %d y %d, dest x %d y %d, width %d height %d\n",
			  src_x, src_y, dst_x, dst_y, width, height);
	   xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		"This is very probably caused by a known bug in libxaa.a.\n");
	   xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
		"Please update libxaa.a to avoid this error.\n");
	   return;
	}
#endif

	/* Although the chip knows the direction to use
	 * if the source and destination areas overlap,
	 * that logic fails if we fiddle with the bitmap
	 * addresses. Therefore, we check if the source
	 * and destination blitting areas overlap and
	 * adapt the bitmap addresses synchronously
	 * if the coordinates exceed the valid range.
	 * The the areas do not overlap, we do our
	 * normal check.
	 */
	if((mymax - mymin) < height) {
	   if((src_y >= 2048) || (dst_y >= 2048)) {
	      srcbase = pSiS->scrnOffset * mymin;
	      dstbase = pSiS->scrnOffset * mymin;
	      src_y -= mymin;
	      dst_y -= mymin;
	   }
	} else {
	   if(src_y >= 2048) {
	      srcbase = pSiS->scrnOffset * src_y;
	      src_y = 0;
	   }
	   if((dst_y >= pScrn->virtualY) || (dst_y >= 2048)) {
	      dstbase = pSiS->scrnOffset * dst_y;
	      dst_y = 0;
	   }
	}

	srcbase += FBOFFSET;
	dstbase += FBOFFSET;

#ifdef SISVRAMQ
	SiSCheckQueue(16 * 3);
	SiSSetupSRCDSTBase(srcbase, dstbase)
	SiSSetupSRCDSTXY(src_x, src_y, dst_x, dst_y)
	SiSSetRectDoCMD(width,height)
#else
	SiSSetupSRCBase(srcbase);
	SiSSetupDSTBase(dstbase);
	SiSSetupRect(width, height)
	SiSSetupSRCXY(src_x, src_y)
	SiSSetupDSTXY(dst_x, dst_y)
	SiSDoCMD
#endif
}

static void
SiSSetupForSolidFill(ScrnInfoPtr pScrn, int color,
			int rop, unsigned int planemask)
{
	SISPtr  pSiS = SISPTR(pScrn);

	if(pSiS->disablecolorkeycurrent || pSiS->nocolorkey) {
	   if((CARD32)color == pSiS->colorKey) {
	      rop = 5;  /* NOOP */
	   }
	}

#ifdef SISVRAMQ
	SiSSetupDSTColorDepth(pSiS->SiS310_AccelDepth);
	SiSCheckQueue(16 * 1);
	SiSSetupPATFGDSTRect(color, pSiS->scrnOffset, DEV_HEIGHT)
	SiSSetupROP(SiSGetPatternROP(rop))
	SiSSetupCMDFlag(PATFG)
	SiSSyncWP
#else
	SiSSetupPATFG(color)
	SiSSetupDSTRect(pSiS->scrnOffset, DEV_HEIGHT)
	SiSSetupDSTColorDepth(pSiS->DstColor);
	SiSSetupROP(SiSGetPatternROP(rop))
	SiSSetupCMDFlag(PATFG | pSiS->SiS310_AccelDepth)
#endif
}

static void
SiSSubsequentSolidFillRect(ScrnInfoPtr pScrn,
			int x, int y, int w, int h)
{
	SISPtr pSiS = SISPTR(pScrn);
	CARD32 dstbase = 0;

	if(y >= 2048) {
	   dstbase = pSiS->scrnOffset * y;
	   y = 0;
	}

	dstbase += FBOFFSET;

	pSiS->CommandReg &= ~(T_XISMAJORL | T_XISMAJORR |
	                      T_L_X_INC | T_L_Y_INC |
	                      T_R_X_INC | T_R_Y_INC |
			      TRAPAZOID_FILL);

	/* SiSSetupCMDFlag(BITBLT)  - BITBLT = 0 */

#ifdef SISVRAMQ
	SiSCheckQueue(16 * 2)
	SiSSetupDSTXYRect(x, y, w, h)
	SiSSetupDSTBaseDoCMD(dstbase)
#else
	SiSSetupDSTBase(dstbase)
	SiSSetupDSTXY(x, y)
	SiSSetupRect(w, h)
	SiSDoCMD
#endif
}

#ifdef SIS_USE_XAA  /* ---------------------------- XAA -------------------------- */

static void
SiSSetupForSolidLine(ScrnInfoPtr pScrn, int color, int rop,
			unsigned int planemask)
{
	SISPtr pSiS = SISPTR(pScrn);

#ifdef SISVRAMQ
	SiSSetupDSTColorDepth(pSiS->SiS310_AccelDepth);
	SiSCheckQueue(16 * 3);
	SiSSetupLineCountPeriod(1, 1)
	SiSSetupPATFGDSTRect(color, pSiS->scrnOffset, DEV_HEIGHT)
	SiSSetupROP(SiSGetPatternROP(rop))
	SiSSetupCMDFlag(PATFG | LINE)
	SiSSyncWP
#else
	SiSSetupLineCount(1)
	SiSSetupPATFG(color)
	SiSSetupDSTRect(pSiS->scrnOffset, DEV_HEIGHT)
	SiSSetupDSTColorDepth(pSiS->DstColor)
	SiSSetupROP(SiSGetPatternROP(rop))
	SiSSetupCMDFlag(PATFG | LINE | pSiS->SiS310_AccelDepth)
#endif
}

static void
SiSSubsequentSolidTwoPointLine(ScrnInfoPtr pScrn,
			int x1, int y1, int x2, int y2, int flags)
{
	SISPtr pSiS = SISPTR(pScrn);
	int    miny, maxy;
	CARD32 dstbase = 0;

	miny = (y1 > y2) ? y2 : y1;
	maxy = (y1 > y2) ? y1 : y2;
	if(maxy >= 2048) {
	   dstbase = pSiS->scrnOffset*miny;
	   y1 -= miny;
	   y2 -= miny;
	}

	dstbase += FBOFFSET;

	if(flags & OMIT_LAST) {
	   SiSSetupCMDFlag(NO_LAST_PIXEL)
	} else {
	   pSiS->CommandReg &= ~(NO_LAST_PIXEL);
	}

#ifdef SISVRAMQ
	SiSCheckQueue(16 * 2);
	SiSSetupX0Y0X1Y1(x1, y1, x2, y2)
	SiSSetupDSTBaseDoCMD(dstbase)
#else
	SiSSetupDSTBase(dstbase)
	SiSSetupX0Y0(x1, y1)
	SiSSetupX1Y1(x2, y2)
	SiSDoCMD
#endif
}

static void
SiSSubsequentSolidHorzVertLine(ScrnInfoPtr pScrn,
			int x, int y, int len, int dir)
{
	SISPtr pSiS = SISPTR(pScrn);
	CARD32 dstbase = 0;

	len--; /* starting point is included! */

	if((y >= 2048) || ((y + len) >= 2048)) {
	   dstbase = pSiS->scrnOffset * y;
	   y = 0;
	}

	dstbase += FBOFFSET;

#ifdef SISVRAMQ
	SiSCheckQueue(16 * 2);
	if(dir == DEGREES_0) {
	   SiSSetupX0Y0X1Y1(x, y, (x + len), y)
	} else {
	   SiSSetupX0Y0X1Y1(x, y, x, (y + len))
	}
	SiSSetupDSTBaseDoCMD(dstbase)
#else
	SiSSetupDSTBase(dstbase)
	SiSSetupX0Y0(x,y)
	if(dir == DEGREES_0) {
	   SiSSetupX1Y1(x + len, y);
	} else {
	   SiSSetupX1Y1(x, y + len);
	}
	SiSDoCMD
#endif
}

static void
SiSSetupForDashedLine(ScrnInfoPtr pScrn,
			int fg, int bg, int rop, unsigned int planemask,
			int length, unsigned char *pattern)
{
	SISPtr pSiS = SISPTR(pScrn);

#ifdef SISVRAMQ
	SiSSetupDSTColorDepth(pSiS->SiS310_AccelDepth);
	SiSCheckQueue(16 * 3);
	SiSSetupLineCountPeriod(1, (length - 1))
	SiSSetupStyle(*pattern,*(pattern + 4))
	SiSSetupPATFGDSTRect(fg, pSiS->scrnOffset, DEV_HEIGHT)
#else
	SiSSetupLineCount(1)
	SiSSetupDSTRect(pSiS->scrnOffset, DEV_HEIGHT)
	SiSSetupDSTColorDepth(pSiS->DstColor);
	SiSSetupStyleLow(*pattern)
	SiSSetupStyleHigh(*(pattern + 4))
	SiSSetupStylePeriod(length - 1);
	SiSSetupPATFG(fg)
#endif

	SiSSetupROP(SiSGetPatternROP(rop))

	SiSSetupCMDFlag(LINE | LINE_STYLE)

	if(bg != -1) {
	   SiSSetupPATBG(bg)
	} else {
	   SiSSetupCMDFlag(TRANSPARENT)
	}
#ifndef SISVRAMQ
	SiSSetupCMDFlag(pSiS->SiS310_AccelDepth)
#endif

#ifdef SISVRAMQ
        SiSSyncWP
#endif
}

static void
SiSSubsequentDashedTwoPointLine(ScrnInfoPtr pScrn,
			int x1, int y1, int x2, int y2,
			int flags, int phase)
{
	SISPtr pSiS = SISPTR(pScrn);
	CARD32 dstbase, miny, maxy;

	dstbase = 0;
	miny = (y1 > y2) ? y2 : y1;
	maxy = (y1 > y2) ? y1 : y2;
	if(maxy >= 2048) {
	   dstbase = pSiS->scrnOffset * miny;
	   y1 -= miny;
	   y2 -= miny;
	}

	dstbase += FBOFFSET;

	if(flags & OMIT_LAST) {
	   SiSSetupCMDFlag(NO_LAST_PIXEL)
	} else {
	   pSiS->CommandReg &= ~(NO_LAST_PIXEL);
	}

#ifdef SISVRAMQ
	SiSCheckQueue(16 * 2);
	SiSSetupX0Y0X1Y1(x1, y1, x2, y2)
	SiSSetupDSTBaseDoCMD(dstbase)
#else
	SiSSetupDSTBase(dstbase)
	SiSSetupX0Y0(x1, y1)
	SiSSetupX1Y1(x2, y2)
	SiSDoCMD
#endif
}

static void
SiSSetupForMonoPatternFill(ScrnInfoPtr pScrn,
			int patx, int paty, int fg, int bg,
			int rop, unsigned int planemask)
{
	SISPtr pSiS = SISPTR(pScrn);

#ifdef SISVRAMQ
	SiSSetupDSTColorDepth(pSiS->SiS310_AccelDepth);
	SiSCheckQueue(16 * 3);
	SiSSetupPATFGDSTRect(fg, pSiS->scrnOffset, DEV_HEIGHT)
#else
	SiSSetupDSTRect(pSiS->scrnOffset, DEV_HEIGHT)
	SiSSetupDSTColorDepth(pSiS->DstColor);
#endif

	SiSSetupMONOPAT(patx,paty)

	SiSSetupROP(SiSGetPatternROP(rop))

#ifdef SISVRAMQ
	SiSSetupCMDFlag(PATMONO)
#else
	SiSSetupPATFG(fg)
	SiSSetupCMDFlag(PATMONO | pSiS->SiS310_AccelDepth)
#endif

	if(bg != -1) {
	   SiSSetupPATBG(bg)
	} else {
	   SiSSetupCMDFlag(TRANSPARENT)
	}

#ifdef SISVRAMQ
	SiSSyncWP
#endif
}

static void
SiSSubsequentMonoPatternFill(ScrnInfoPtr pScrn,
			int patx, int paty,
			int x, int y, int w, int h)
{
	SISPtr pSiS = SISPTR(pScrn);
	CARD32 dstbase = 0;

	if(y >= 2048) {
	   dstbase = pSiS->scrnOffset * y;
	   y = 0;
	}

	dstbase += FBOFFSET;

	/* Clear commandReg because Setup can be used for Rect and Trap */
	pSiS->CommandReg &= ~(T_XISMAJORL | T_XISMAJORR |
			      T_L_X_INC | T_L_Y_INC |
			      T_R_X_INC | T_R_Y_INC |
			      TRAPAZOID_FILL);

#ifdef SISVRAMQ
	SiSCheckQueue(16 * 2);
	SiSSetupDSTXYRect(x,y,w,h)
	SiSSetupDSTBaseDoCMD(dstbase)
#else
	SiSSetupDSTBase(dstbase)
	SiSSetupDSTXY(x,y)
	SiSSetupRect(w,h)
	SiSDoCMD
#endif
}

#ifdef SISVRAMQ
static void
SiSSetupForColor8x8PatternFill(ScrnInfoPtr pScrn, int patternx, int patterny,
			int rop, unsigned int planemask, int trans_col)
{
	SISPtr pSiS = SISPTR(pScrn);
	int j = pSiS->CurrentLayout.bytesPerPixel;
	CARD32 *patadr = (CARD32 *)(pSiS->FbBase + (patterny * pSiS->scrnOffset) +
				(patternx * j));

	SiSSetupDSTColorDepth(pSiS->SiS310_AccelDepth);
	SiSCheckQueue(16 * 3);

	SiSSetupDSTRectBurstHeader(pSiS->scrnOffset, DEV_HEIGHT,
			PATTERN_REG, (pSiS->CurrentLayout.bitsPerPixel << 1))

	while(j--) {
	   SiSSetupPatternRegBurst(patadr[0],  patadr[1],  patadr[2],  patadr[3]);
	   SiSSetupPatternRegBurst(patadr[4],  patadr[5],  patadr[6],  patadr[7]);
	   SiSSetupPatternRegBurst(patadr[8],  patadr[9],  patadr[10], patadr[11]);
	   SiSSetupPatternRegBurst(patadr[12], patadr[13], patadr[14], patadr[15]);
	   patadr += 16;  /* = 64 due to (CARD32 *) */
	}

	SiSSetupROP(SiSGetPatternROP(rop))

	SiSSetupCMDFlag(PATPATREG)

	SiSSyncWP
}

static void
SiSSubsequentColor8x8PatternFillRect(ScrnInfoPtr pScrn, int patternx,
			int patterny, int x, int y, int w, int h)
{
	SISPtr pSiS = SISPTR(pScrn);
	CARD32 dstbase = 0;

	if(y >= 2048) {
	   dstbase = pSiS->scrnOffset * y;
	   y = 0;
	}

	dstbase += FBOFFSET;

	/* SiSSetupCMDFlag(BITBLT)  - BITBLT = 0 */

	SiSCheckQueue(16 * 2)
	SiSSetupDSTXYRect(x, y, w, h)
	SiSSetupDSTBaseDoCMD(dstbase)
}
#endif

#ifdef SISDUALHEAD
static void
SiSRestoreAccelState(ScrnInfoPtr pScrn)
{
	SISPtr pSiS = SISPTR(pScrn);

	pSiS->ColorExpandBusy = FALSE;
	pSiS->alphaBlitBusy = FALSE;
	SiSIdle
}
#endif

/* ---- RENDER ---- */

#if defined(RENDER) && defined(INCL_RENDER)
static void
SiSRenderCallback(ScrnInfoPtr pScrn)
{
	SISPtr pSiS = SISPTR(pScrn);

	if((currentTime.milliseconds > pSiS->RenderTime) && pSiS->AccelLinearScratch) {
	   xf86FreeOffscreenLinear(pSiS->AccelLinearScratch);
	   pSiS->AccelLinearScratch = NULL;
	}

	if(!pSiS->AccelLinearScratch) {
	   pSiS->RenderCallback = NULL;
	}
}

#define RENDER_DELAY 15000

static Bool
SiSAllocateLinear(ScrnInfoPtr pScrn, int sizeNeeded)
{
	SISPtr pSiS = SISPTR(pScrn);

	pSiS->RenderTime = currentTime.milliseconds + RENDER_DELAY;
	pSiS->RenderCallback = SiSRenderCallback;

	if(pSiS->AccelLinearScratch) {
	   if(pSiS->AccelLinearScratch->size >= sizeNeeded) {
	      return TRUE;
	   } else {
	      if(pSiS->alphaBlitBusy) {
	         pSiS->alphaBlitBusy = FALSE;
	         SiSIdle
	      }
	      if(xf86ResizeOffscreenLinear(pSiS->AccelLinearScratch, sizeNeeded)) {
		 return TRUE;
	      }
	      xf86FreeOffscreenLinear(pSiS->AccelLinearScratch);
	      pSiS->AccelLinearScratch = NULL;
	   }
	}

	pSiS->AccelLinearScratch = xf86AllocateOffscreenLinear(
				 	pScrn->pScreen, sizeNeeded, 32,
				 	NULL, NULL, NULL);

	return(pSiS->AccelLinearScratch != NULL);
}

static Bool
SiSSetupForCPUToScreenAlphaTexture(ScrnInfoPtr pScrn,
			int op, CARD16 red, CARD16 green,
			CARD16 blue, CARD16 alpha,
#ifdef SISNEWRENDER
			CARD32 alphaType, CARD32 dstType,
#else
			int alphaType,
#endif
			CARD8 *alphaPtr,
			int alphaPitch, int width,
			int height, int	flags)
{
	SISPtr pSiS = SISPTR(pScrn);
	unsigned char *renderaccelarray;
	CARD32 *dstPtr;
	int    x, pitch, sizeNeeded;
	int    sbpp = pSiS->CurrentLayout.bytesPerPixel;
	int    sbppshift = sbpp >> 1;	/* 8->0, 16->1, 32->2 */
	CARD8  myalpha;
	Bool   docopy = TRUE;

#ifdef ACCELDEBUG
	xf86DrvMsg(0, X_INFO, "AT(1): op %d t %x ARGB %x %x %x %x, w %d h %d pch %d\n",
		op, alphaType, /*dstType, */alpha, red, green, blue, width, height, alphaPitch);
#endif

	if((width > 2048) || (height > 2048)) return FALSE;

#ifdef SISVRAMQ
	if(op > SiSRenderOpsMAX) return FALSE;
	if(!SiSRenderOps[op])    return FALSE;
#else
	if(op != PictOpOver) return FALSE;
#endif

	if(!((renderaccelarray = pSiS->RenderAccelArray)))
	   return FALSE;

#ifdef ACCELDEBUG
	xf86DrvMsg(0, X_INFO, "AT(2): op %d t %x ARGB %x %x %x %x, w %d h %d pch %d\n",
		op, alphaType, alpha, red, green, blue, width, height, alphaPitch);
#endif

	pitch = (width + 31) & ~31;
	sizeNeeded = (pitch << 2) * height; /* Source a8 (=8bit), expand to A8R8G8B8 (=32bit) */

	if(!SiSAllocateLinear(pScrn, (sizeNeeded + sbpp - 1) >> sbppshift))
	   return FALSE;

	red &= 0xff00;
	green &= 0xff00;
	blue &= 0xff00;

#ifdef SISVRAMQ
	SiSSetupDSTColorDepth(pSiS->SiS310_AccelDepth);
	switch(op) {
	case PictOpClear:
#if XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,0,0,0)
	case PictOpDisjointClear:
	case PictOpConjointClear:
#endif
	   SiSSetupPATFGDSTRect(0, pSiS->scrnOffset, DEV_HEIGHT)
	   /* SiSSetupROP(0x00) - is already 0 */
	   SiSSetupCMDFlag(PATFG)
	   docopy = FALSE;
	   break;
	case PictOpSrc:
#if XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,0,0,0)
	case PictOpDisjointSrc:
	case PictOpConjointSrc:
#endif
	   SiSSetupSRCPitchDSTRect((pitch << 2), pSiS->scrnOffset, DEV_HEIGHT);
	   SiSSetupAlpha(0xff)
	   SiSSetupCMDFlag(ALPHA_BLEND | SRCVIDEO | A_NODESTALPHA)
	   break;
	case PictOpDst:
#if XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,0,0,0)
	case PictOpDisjointDst:
	case PictOpConjointDst:
#endif
	   SiSSetupSRCPitchDSTRect((pitch << 2), pSiS->scrnOffset, DEV_HEIGHT);
	   SiSSetupAlpha(0x00)
	   SiSSetupCMDFlag(ALPHA_BLEND | SRCVIDEO | A_CONSTANTALPHA)
	   docopy = FALSE;
	   break;
	case PictOpOver:
	   SiSSetupSRCPitchDSTRect((pitch << 2), pSiS->scrnOffset, DEV_HEIGHT);
	   SiSSetupCMDFlag(ALPHA_BLEND | SRCVIDEO | A_PERPIXELALPHA)
	   break;
	}
        SiSSyncWP
#else
	SiSSetupDSTColorDepth(pSiS->DstColor);
	SiSSetupSRCPitch((pitch << 2));
	SiSSetupDSTRect(pSiS->scrnOffset, DEV_HEIGHT)
	SiSSetupROP(0)
	SiSSetupCMDFlag(ALPHA_BLEND | SRCVIDEO | A_PERPIXELALPHA | pSiS->SiS310_AccelDepth)
#endif

	/* Don't need source for clear and dest */
	if(!docopy) return TRUE;

	dstPtr = (CARD32*)(pSiS->FbBase + (pSiS->AccelLinearScratch->offset << sbppshift));

	if(pSiS->alphaBlitBusy) {
	   pSiS->alphaBlitBusy = FALSE;
	   SiSIdle
	}

	if(alpha == 0xffff) {

	   while(height--) {
	      for(x = 0; x < width; x++) {
	         myalpha = alphaPtr[x];
	         dstPtr[x] = (renderaccelarray[red + myalpha] << 16)  |
			     (renderaccelarray[green + myalpha] << 8) |
			     renderaccelarray[blue + myalpha]         |
			     myalpha << 24;
	      }
	      dstPtr += pitch;
	      alphaPtr += alphaPitch;
	   }

	} else {

	   alpha &= 0xff00;

	   while(height--) {
	      for(x = 0; x < width; x++) {
	         myalpha = alphaPtr[x];
	         dstPtr[x] = (renderaccelarray[alpha + myalpha] << 24) |
			     (renderaccelarray[red + myalpha] << 16)   |
			     (renderaccelarray[green + myalpha] << 8)  |
			     renderaccelarray[blue + myalpha];
	      }
	      dstPtr += pitch;
	      alphaPtr += alphaPitch;
	   }

	}

	return TRUE;
}

static Bool
SiSSetupForCPUToScreenTexture(ScrnInfoPtr pScrn,
			int op,
#ifdef SISNEWRENDER
			CARD32 texType, CARD32 dstType,
#else
			int texType,
#endif
			CARD8 *texPtr,
			int texPitch, int width,
			int height, int	flags)
{
	SISPtr  pSiS = SISPTR(pScrn);
	CARD8   *dst;
	int     pitch, sizeNeeded;
	int     sbpp = pSiS->CurrentLayout.bytesPerPixel;
	int     sbppshift = sbpp >> 1;	          	  /* 8->0, 16->1, 32->2 */
	int     bppshift = PICT_FORMAT_BPP(texType) >> 4; /* 8->0, 16->1, 32->2 */
	Bool    docopy = TRUE;

#ifdef ACCELDEBUG
	xf86DrvMsg(0, X_INFO, "T: type %x op %d w %d h %d T-pitch %d\n",
		texType, op, width, height, texPitch);
#endif

#ifdef SISVRAMQ
	if(op > SiSRenderOpsMAX) return FALSE;
	if(!SiSRenderOps[op])    return FALSE;
#else
	if(op != PictOpOver) return FALSE;
#endif

	if((width > 2048) || (height > 2048)) return FALSE;

	pitch = (width + 31) & ~31;
	sizeNeeded = (pitch << bppshift) * height;

#ifdef ACCELDEBUG
	xf86DrvMsg(0, X_INFO, "T: %x op %x w %d h %d T-pitch %d size %d (%d %d %d)\n",
		texType, op, width, height, texPitch, sizeNeeded, sbpp, sbppshift, bppshift);
#endif

	if(!SiSAllocateLinear(pScrn, (sizeNeeded + sbpp - 1) >> sbppshift))
	   return FALSE;

	width <<= bppshift;  /* -> bytes (for engine and memcpy) */
	pitch <<= bppshift;  /* -> bytes */

#ifdef SISVRAMQ
	SiSSetupDSTColorDepth(pSiS->SiS310_AccelDepth);
	switch(op) {
	case PictOpClear:
#if XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,0,0,0)
	case PictOpDisjointClear:
	case PictOpConjointClear:
#endif
	   SiSSetupPATFGDSTRect(0, pSiS->scrnOffset, DEV_HEIGHT)
	   /* SiSSetupROP(0x00) - is already zero */
	   SiSSetupCMDFlag(PATFG)
	   docopy = FALSE;
	   break;
	case PictOpSrc:
#if XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,0,0,0)
	case PictOpDisjointSrc:
	case PictOpConjointSrc:
#endif
	   SiSSetupSRCPitchDSTRect(pitch, pSiS->scrnOffset, DEV_HEIGHT);
	   SiSSetupAlpha(0xff)
	   SiSSetupCMDFlag(ALPHA_BLEND | SRCVIDEO | A_NODESTALPHA)
	   break;
	case PictOpDst:
#if XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,0,0,0)
	case PictOpDisjointDst:
	case PictOpConjointDst:
#endif
	   SiSSetupSRCPitchDSTRect(pitch, pSiS->scrnOffset, DEV_HEIGHT);
	   SiSSetupAlpha(0x00)
	   SiSSetupCMDFlag(ALPHA_BLEND | SRCVIDEO | A_CONSTANTALPHA)
	   docopy = FALSE;
	   break;
	case PictOpOver:
	   SiSSetupSRCPitchDSTRect(pitch, pSiS->scrnOffset, DEV_HEIGHT);
	   SiSSetupAlpha(0x00)
	   SiSSetupCMDFlag(ALPHA_BLEND | SRCVIDEO | A_PERPIXELALPHA)
	   break;
	default:
	   return FALSE;
 	}
        SiSSyncWP
#else
	SiSSetupDSTColorDepth(pSiS->DstColor);
	SiSSetupSRCPitch(pitch);
	SiSSetupDSTRect(pSiS->scrnOffset, DEV_HEIGHT)
	SiSSetupAlpha(0x00)
	SiSSetupCMDFlag(ALPHA_BLEND | SRCVIDEO | A_PERPIXELALPHA | pSiS->SiS310_AccelDepth)
#endif

	/* Don't need source for clear and dest */
	if(!docopy) return TRUE;

	dst = (CARD8*)(pSiS->FbBase + (pSiS->AccelLinearScratch->offset << sbppshift));

	if(pSiS->alphaBlitBusy) {
	   pSiS->alphaBlitBusy = FALSE;
	   SiSIdle
	}

	while(height--) {
	   memcpy(dst, texPtr, width);
	   texPtr += texPitch;
	   dst += pitch;
	}

	return TRUE;
}

static void
SiSSubsequentCPUToScreenTexture(ScrnInfoPtr pScrn,
			int dst_x, int dst_y,
			int src_x, int src_y,
			int width, int height)
{
	SISPtr pSiS = SISPTR(pScrn);
	CARD32 srcbase, dstbase;

	srcbase = pSiS->AccelLinearScratch->offset << 1;
	if(pScrn->bitsPerPixel == 32) srcbase <<= 1;

#ifdef ACCELDEBUG
	xf86DrvMsg(0, X_INFO, "FIRE: scrbase %x dx %d dy %d w %d h %d\n",
		srcbase, dst_x, dst_y, width, height);
#endif

	dstbase = 0;
	if((dst_y >= pScrn->virtualY) || (dst_y >= 2048)) {
	   dstbase = pSiS->scrnOffset * dst_y;
	   dst_y = 0;
	}

	srcbase += FBOFFSET;
	dstbase += FBOFFSET;

#ifdef SISVRAMQ
	SiSCheckQueue(16 * 3)
	SiSSetupSRCDSTBase(srcbase,dstbase);
	SiSSetupSRCDSTXY(src_x, src_y, dst_x, dst_y)
	SiSSetRectDoCMD(width,height)
#else
	SiSSetupSRCBase(srcbase);
	SiSSetupDSTBase(dstbase);
	SiSSetupRect(width, height)
	SiSSetupSRCXY(src_x, src_y)
	SiSSetupDSTXY(dst_x, dst_y)
	SiSDoCMD
#endif
	pSiS->alphaBlitBusy = TRUE;
}
#endif  /* RENDER && INCL_RENDER */

#endif /* XAA */

#ifdef SIS_USE_EXA  /* ---------------------------- EXA -------------------------- */

static void
SiSEXASync(ScreenPtr pScreen, int marker)
{
	SISPtr pSiS = SISPTR(xf86Screens[pScreen->myNum]);

	SiSIdle
}

static Bool
SiSPrepareSolid(PixmapPtr pPixmap, int alu, Pixel planemask, Pixel fg)
{
	ScrnInfoPtr pScrn = xf86Screens[pPixmap->drawable.pScreen->myNum];
	SISPtr pSiS = SISPTR(pScrn);
	CARD16 pitch;

	/* Planemask not supported */
	if((planemask & ((1 << pPixmap->drawable.depth) - 1)) !=
				(1 << pPixmap->drawable.depth) - 1) {
	   return FALSE;
	}

	if((pPixmap->drawable.bitsPerPixel != 8) &&
	   (pPixmap->drawable.bitsPerPixel != 16) &&
	   (pPixmap->drawable.bitsPerPixel != 32))
	   return FALSE;

	/* Check that the pitch matches the hardware's requirements. Should
	 * never be a problem due to pixmapPitchAlign and fbScreenInit.
	 */
	if(((pitch = exaGetPixmapPitch(pPixmap)) & 3))
	   return FALSE;

	if(pSiS->disablecolorkeycurrent || pSiS->nocolorkey) {
	   if((CARD32)fg == pSiS->colorKey) {
	      /* NOOP - does not work: Pixmap is not neccessarily in the frontbuffer */
	      /* alu = 5; */
	      /* Fill it black; better than blue anyway */
	      fg = 0;
	   }
	}

	SiSSetupDSTColorDepth((pPixmap->drawable.bitsPerPixel >> 4) << 16);
	SiSCheckQueue(16 * 1);
	SiSSetupPATFGDSTRect(fg, pitch, DEV_HEIGHT)
	SiSSetupROP(SiSGetPatternROP(alu))
	SiSSetupCMDFlag(PATFG)
	SiSSyncWP

	pSiS->fillDstBase = (CARD32)exaGetPixmapOffset(pPixmap) + FBOFFSET;

	return TRUE;
}

static void
SiSSolid(PixmapPtr pPixmap, int x1, int y1, int x2, int y2)
{
	ScrnInfoPtr pScrn = xf86Screens[pPixmap->drawable.pScreen->myNum];
	SISPtr pSiS = SISPTR(pScrn);

	/* SiSSetupCMDFlag(BITBLT)  - BITBLT = 0 */

	SiSCheckQueue(16 * 2)
	SiSSetupDSTXYRect(x1, y1, x2-x1, y2-y1)
	SiSSetupDSTBaseDoCMD(pSiS->fillDstBase)
}

static void
SiSDoneSolid(PixmapPtr pPixmap)
{
}

static Bool
SiSPrepareCopy(PixmapPtr pSrcPixmap, PixmapPtr pDstPixmap, int xdir, int ydir,
					int alu, Pixel planemask)
{
	ScrnInfoPtr pScrn = xf86Screens[pDstPixmap->drawable.pScreen->myNum];
	SISPtr pSiS = SISPTR(pScrn);
	CARD32 srcbase, dstbase;
	CARD16 srcpitch, dstpitch;

	/* Planemask not supported */
	if((planemask & ((1 << pSrcPixmap->drawable.depth) - 1)) !=
				(1 << pSrcPixmap->drawable.depth) - 1) {
	   return FALSE;
	}

	if((pDstPixmap->drawable.bitsPerPixel != 8) &&
	   (pDstPixmap->drawable.bitsPerPixel != 16) &&
	   (pDstPixmap->drawable.bitsPerPixel != 32))
	   return FALSE;

	/* Check that the pitch matches the hardware's requirements. Should
	 * never be a problem due to pixmapPitchAlign and fbScreenInit.
	 */
	if((srcpitch = exaGetPixmapPitch(pSrcPixmap)) & 3)
	   return FALSE;
	if((dstpitch = exaGetPixmapPitch(pDstPixmap)) & 3)
	   return FALSE;

	srcbase = (CARD32)exaGetPixmapOffset(pSrcPixmap) + FBOFFSET;

	dstbase = (CARD32)exaGetPixmapOffset(pDstPixmap) + FBOFFSET;

	/* TODO: Will there eventually be overlapping blits?
	 * If so, good night. Then we must calculate new base addresses
	 * which are identical for source and dest, otherwise
	 * the chips direction-logic will fail. Certainly funny
	 * to re-calculate x and y then...
	 */

	SiSSetupDSTColorDepth((pDstPixmap->drawable.bitsPerPixel >> 4) << 16);
	SiSCheckQueue(16 * 3);
	SiSSetupSRCPitchDSTRect(srcpitch, dstpitch, DEV_HEIGHT)
	SiSSetupROP(SiSGetCopyROP(alu))
	SiSSetupSRCDSTBase(srcbase, dstbase)
	SiSSyncWP

	return TRUE;
}

static void
SiSCopy(PixmapPtr pDstPixmap, int srcX, int srcY, int dstX, int dstY, int width, int height)
{
	ScrnInfoPtr pScrn = xf86Screens[pDstPixmap->drawable.pScreen->myNum];
	SISPtr pSiS = SISPTR(pScrn);

	SiSCheckQueue(16 * 2);
	SiSSetupSRCDSTXY(srcX, srcY, dstX, dstY)
	SiSSetRectDoCMD(width, height)
}

static void
SiSDoneCopy(PixmapPtr pDstPixmap)
{
}

#ifdef SIS_HAVE_COMPOSITE
static Bool
SiSCheckComposite(int op, PicturePtr pSrcPicture, PicturePtr pMaskPicture,
				PicturePtr pDstPicture)
{
	ScrnInfoPtr pScrn = xf86Screens[pDstPicture->pDrawable->pScreen->myNum];
	SISPtr pSiS = SISPTR(pScrn);

	xf86DrvMsg(0, 0, "CC: %d Src %x (fi %d ca %d) Msk %x (%d %d) Dst %x (%d %d)\n",
		op, pSrcPicture->format, pSrcPicture->filter, pSrcPicture->componentAlpha,
		pMaskPicture ? pMaskPicture->format : 0x2011, pMaskPicture ? pMaskPicture->filter : -1,
			pMaskPicture ? pMaskPicture->componentAlpha : -1,
		pDstPicture->format, pDstPicture->filter, pDstPicture->componentAlpha);

	if(pSrcPicture->transform || (pMaskPicture && pMaskPicture->transform) || pDstPicture->transform) {
		xf86DrvMsg(0, 0, "CC: src tr %p msk %p dst %p  !!!!!!!!!!!!!!!\n",
			pSrcPicture->transform,
			pMaskPicture ? pMaskPicture->transform : 0,
			pDstPicture->transform);
        }

	return FALSE;
}

static Bool
SiSPrepareComposite(int op, PicturePtr pSrcPicture, PicturePtr pMaskPicture,
				PicturePtr pDstPicture, PixmapPtr pSrc, PixmapPtr pMask, PixmapPtr pDst)
{
#if 0
	ScrnInfoPtr pScrn = xf86Screens[pDst->drawable.pScreen->myNum];
	SISPtr pSiS = SISPTR(pScrn);
#endif
	return FALSE;
}

static void
SiSComposite(PixmapPtr pDst, int srcX, int srcY, int maskX, int maskY, int dstX, int dstY,
				int width, int height)
{
#if 0
	ScrnInfoPtr pScrn = xf86Screens[pDst->drawable.pScreen->myNum];
	SISPtr pSiS = SISPTR(pScrn);
#endif
}

static void
SiSDoneComposite(PixmapPtr pDst)
{
}
#endif

Bool
SiSUploadToScreen(PixmapPtr pDst, int x, int y, int w, int h, char *src, int src_pitch)
{
	ScrnInfoPtr pScrn = xf86Screens[pDst->drawable.pScreen->myNum];
	SISPtr pSiS = SISPTR(pScrn);
	unsigned char *dst = pDst->devPrivate.ptr;
	int dst_pitch = exaGetPixmapPitch(pDst);

	(pSiS->SyncAccel)(pScrn);

	if(pDst->drawable.bitsPerPixel < 8)
	   return FALSE;

	dst += (x * pDst->drawable.bitsPerPixel / 8) + (y * dst_pitch);
	while(h--) {
	   SiSMemCopyToVideoRam(pSiS, dst, (unsigned char *)src,
				(w * pDst->drawable.bitsPerPixel / 8));
	   src += src_pitch;
	   dst += dst_pitch;
	}

	return TRUE;
}

Bool
SiSUploadToScratch(PixmapPtr pSrc, PixmapPtr pDst)
{
	ScrnInfoPtr pScrn = xf86Screens[pSrc->drawable.pScreen->myNum];
	SISPtr pSiS = SISPTR(pScrn);
	unsigned char *src, *dst;
	int src_pitch = exaGetPixmapPitch(pSrc);
	int dst_pitch, size, w, h, bytes;

	w = pSrc->drawable.width;

#ifdef SISISXORGPOST70
	dst_pitch = ((w * (pSrc->drawable.bitsPerPixel >> 3)) +
		     pSiS->EXADriverPtr->pixmapPitchAlign - 1) &
		    ~(pSiS->EXADriverPtr->pixmapPitchAlign - 1);
#else
	dst_pitch = ((w * (pSrc->drawable.bitsPerPixel >> 3)) +
		     pSiS->EXADriverPtr->card.pixmapPitchAlign - 1) &
		    ~(pSiS->EXADriverPtr->card.pixmapPitchAlign - 1);
#endif

	size = dst_pitch * pSrc->drawable.height;

	if(size > pSiS->exa_scratch->size)
	   return FALSE;

#ifdef SISISXORGPOST70
	pSiS->exa_scratch_next = (pSiS->exa_scratch_next +
				  pSiS->EXADriverPtr->pixmapOffsetAlign - 1) &
				  ~(pSiS->EXADriverPtr->pixmapOffsetAlign - 1);
#else
	pSiS->exa_scratch_next = (pSiS->exa_scratch_next +
				  pSiS->EXADriverPtr->card.pixmapOffsetAlign - 1) &
				  ~(pSiS->EXADriverPtr->card.pixmapOffsetAlign - 1);
#endif

	if(pSiS->exa_scratch_next + size >
	   pSiS->exa_scratch->offset + pSiS->exa_scratch->size) {
#ifdef SISISXORGPOST70
	   (pSiS->EXADriverPtr->WaitMarker)(pSrc->drawable.pScreen, 0);
#else
	   (pSiS->EXADriverPtr->accel.WaitMarker)(pSrc->drawable.pScreen, 0);
#endif
	   pSiS->exa_scratch_next = pSiS->exa_scratch->offset;
	}

	memcpy(pDst, pSrc, sizeof(*pDst));
	pDst->devKind = dst_pitch;
#ifdef SISISXORGPOST70
	pDst->devPrivate.ptr = pSiS->EXADriverPtr->memoryBase + pSiS->exa_scratch_next;
#else
	pDst->devPrivate.ptr = pSiS->EXADriverPtr->card.memoryBase + pSiS->exa_scratch_next;
#endif

	pSiS->exa_scratch_next += size;

	src = pSrc->devPrivate.ptr;
	src_pitch = exaGetPixmapPitch(pSrc);
	dst = pDst->devPrivate.ptr;

	bytes = (src_pitch < dst_pitch) ? src_pitch : dst_pitch;

	h = pSrc->drawable.height;

	(pSiS->SyncAccel)(pScrn);

	while(h--) {
	   SiSMemCopyToVideoRam(pSiS, dst, src, size);
	   src += src_pitch;
	   dst += dst_pitch;
	}

	return TRUE;
}

Bool
SiSDownloadFromScreen(PixmapPtr pSrc, int x, int y, int w, int h, char *dst, int dst_pitch)
{
	ScrnInfoPtr pScrn = xf86Screens[pSrc->drawable.pScreen->myNum];
	SISPtr pSiS = SISPTR(pScrn);
	unsigned char *src = pSrc->devPrivate.ptr;
	int src_pitch = exaGetPixmapPitch(pSrc);
	int size = src_pitch < dst_pitch ? src_pitch : dst_pitch;

	(pSiS->SyncAccel)(pScrn);

	if(pSrc->drawable.bitsPerPixel < 8)
	   return FALSE;

	src += (x * pSrc->drawable.bitsPerPixel / 8) + (y * src_pitch);
	while(h--) {
	   SiSMemCopyFromVideoRam(pSiS, (unsigned char *)dst, src, size);
	   src += src_pitch;
	   dst += dst_pitch;
	}

	return TRUE;
}
#endif /* EXA */

/* Helper for xv video blitter and rotation */

void
SISWriteBlitPacket(SISPtr pSiS, CARD32 *packet)
{
	CARD32 dummybuf;

	SiSWritePacketPart(packet[0], packet[1], packet[2], packet[3]);
	SiSWritePacketPart(packet[4], packet[5], packet[6], packet[7]);
	SiSWritePacketPart(packet[8], packet[9], packet[10], packet[11]);
	SiSWritePacketPart(packet[12], packet[13], packet[14], packet[15]);
	SiSWritePacketPart(packet[16], packet[17], packet[18], packet[19]);
	SiSSyncWP;
	(void)dummybuf; /* Suppress compiler warning */
}

#ifdef SIS_INCL_RRROT

static void
SISWriteBlitPacket3(SISPtr pSiS, CARD32 *packet)
{
	CARD32 dummybuf;

	SiSWritePacketPart(packet[0], packet[1], packet[2], packet[3]);
	SiSWritePacketPart(packet[4], packet[5], packet[6], packet[7]);
	SiSWritePacketPart(packet[8], packet[9], packet[10], packet[11]);
	SiSSyncWP;
	(void)dummybuf; /* Suppress compiler warning */
}

/* Acceleration for rotation and reflection */

static Bool
SISAccelerateReflect(ScrnInfoPtr pScrn, int num, BoxPtr pbox,
			CARD32 sourceOffs, CARD32 destOffs,
			int srcPitch, int dstPitch)
{
#ifdef SISVRAMQ
	SISPtr pSiS = SISPTR(pScrn);
	int    width, height;
	SiS_Packet12_SBB MyPacketSBB;

	if(!(pSiS->ChipFlags & SiSCF_HaveStrBB)) return FALSE;

	memset(&MyPacketSBB, 0, sizeof(MyPacketSBB));
	MyPacketSBB.P12_Header0 = SIS_PACKET12_HEADER0;
	MyPacketSBB.P12_Header1 = SIS_PACKET12_HEADER1;
	MyPacketSBB.P12_Null1 = SIS_NIL_CMD;
	MyPacketSBB.P12_Null2 = SIS_NIL_CMD;

	MyPacketSBB.P12_SrcAddr = sourceOffs + FBOFFSET;
	MyPacketSBB.P12_SrcPitch = srcPitch;

	MyPacketSBB.P12_DstAddr = destOffs + FBOFFSET;
	MyPacketSBB.P12_DstPitch = dstPitch;
	MyPacketSBB.P12_DstHeight = 0x0fff;

	MyPacketSBB.P12_Command = STRETCH_BITBLT		|
         			  SRCVIDEO			|
         			  PATFG				|
         			  SBB_DEFAULT_ROP		|
         			  pSiS->SiS310_AccelDepth	|
         			  SBB_DIR_X_INC			|
         			  SBB_DIR_Y_INC;

	switch(pSiS->Reflect) {
	case 1:
	   MyPacketSBB.P12_Command |= SBB_Y_ZERO_IS_TOP;
	   break;
	case 2:
	   MyPacketSBB.P12_Command |= SBB_X_ZERO_IS_LEFT;
	   break;
        }

	while(num--) {
	   width =  pbox->x2 - pbox->x1;
	   height = pbox->y2 - pbox->y1;

	   if(width <= 0 || height <= 0) goto mycontinue1;

	   MyPacketSBB.P12_SrcX = pbox->x1;
	   MyPacketSBB.P12_SrcY = pbox->y1;

	   MyPacketSBB.P12_RectWidth  =
		MyPacketSBB.P12_SourceWidth =
		MyPacketSBB.P12_CalcedWidth = width;

	   MyPacketSBB.P12_LowerWidth = width << 1;

	   /* MyPacketSBB.P12_WidthDiff = 0; - is zero from memset */

	   MyPacketSBB.P12_RectHeight =
		MyPacketSBB.P12_SourceHeight =
		MyPacketSBB.P12_CalcedHeight = height;

	   MyPacketSBB.P12_LowerHeight = height << 1;

	   /* MyPacketSBB.P12_HeightDiff = 0;  - is zero from memset */

	   switch(pSiS->Reflect) {
	   case 1:	/* x */
	      MyPacketSBB.P12_DstX = pScrn->virtualX - pbox->x1 - 1;
	      MyPacketSBB.P12_DstY = MyPacketSBB.P12_SrcY;
	      break;
	   case 2:	/* y */
	      MyPacketSBB.P12_DstX = MyPacketSBB.P12_SrcX;
	      MyPacketSBB.P12_DstY = pScrn->virtualY - pbox->y1 - 1;
	      break;
	   case 3:	/* x + y */
	      MyPacketSBB.P12_DstX = pScrn->virtualX - pbox->x1 - 1;
	      MyPacketSBB.P12_DstY = pScrn->virtualY - pbox->y1 - 1;
	      break;
	   default:
	      return FALSE;
	   }

	   SISWriteBlitPacket(pSiS, (CARD32*)&MyPacketSBB);

mycontinue1:
	   pbox++;
	}

	return TRUE;
#else	/* VRAMQ */
	return FALSE;
#endif
}

#if 0
#ifdef SISVRAMQ
static __inline__ CARD32
SiSFToDW(float val)
{
	union {
	   float f;
	   CARD32 r;
	} temp;
	temp.f = val;
	return temp.r;
}

static void
SiSWriteData(SISPtr pSiS, CARD32 idx1, CARD32 val1, CARD32 idx2, CARD32 val2)
{
	CARD32 ttt = SiSGetSwWP();
	pointer tt = (char *)pSiS->cmdQueueBase + ttt;
	SIS_WQINDEX(0) = (CARD32)(SIS_SPKC3D_HEADER + idx1);
	SIS_WQINDEX(1) = val1;
	SIS_WQINDEX(2) = (CARD32)(SIS_SPKC3D_HEADER + idx2);
	SIS_WQINDEX(3) = val2;
	SISSiSUpdateQueue(pSiS, ttt, tt);
}

static void
SiSWriteSingleData(SISPtr pSiS, CARD32 idx1, CARD32 val1)
{
	CARD32 ttt = SiSGetSwWP();
	pointer tt = (char *)pSiS->cmdQueueBase + ttt;
	SIS_WQINDEX(0) = (CARD32)(SIS_SPKC3D_HEADER + idx1);
	SIS_WQINDEX(1) = val1;
	SIS_WQINDEX(2) = (CARD32)(SIS_NIL3D_CMD);
	SIS_WQINDEX(3) = (CARD32)(SIS_NIL3D_CMD);
	SISSiSUpdateQueue(pSiS, ttt, tt);
}

static void
SiSEndOfPrimitive(SISPtr pSiS)
{
	CARD32 ttt = SiSGetSwWP();
	pointer tt = (char *)pSiS->cmdQueueBase + ttt;
	SIS_WQINDEX(0) = (CARD32)(SIS_SPKC3D_HEADER + 0x8d60);
	SIS_WQINDEX(1) = 0x00000000;
	SIS_WQINDEX(2) = (CARD32)(SIS_SPKC3D_HEADER + 0x8d64);
	SIS_WQINDEX(3) = 0xffffffff;
	SISSiSUpdateQueue(pSiS, ttt, tt);
}

static void
SiSWriteSingleDataFire(SISPtr pSiS, CARD32 idx1, CARD32 val1)
{
	CARD32 ttt = SiSGetSwWP();
	pointer tt = (char *)pSiS->cmdQueueBase + ttt;
	SIS_WQINDEX(0) = (CARD32)(SIS_SPKC3D_HEADER + idx1);
	SIS_WQINDEX(1) = val1;
	SIS_WQINDEX(2) = (CARD32)(SIS_NIL3D_CMD);
	SIS_WQINDEX(3) = (CARD32)(SIS_NIL3D_CMD);
	if(pSiS->NeedFlush) SiSFlush(3)
	ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
	SiSSetHwWP(ttt);
}

static void
SiSSVertex(SISPtr pSiS, int num, int x, int y, int tx, int ty)
{
#if 1
#define VERTBASE 0x8800	/* RGS */
#define VERTMULT 0x40
#define VERTTEX  0x20
#else
#define VERTBASE 0x88c0 /* RGX */
#define VERTMULT 0x38
#define VERTTEX  0x18
#endif
	CARD32 ttt = SiSGetSwWP();
	pointer tt = (char *)pSiS->cmdQueueBase + ttt;
	SIS_WQINDEX(0) = (CARD32)(SIS_SPKC3D_HEADER + VERTBASE +  0 + (num * VERTMULT));
	SIS_WQINDEX(1) = SiSFToDW(x);
	SIS_WQINDEX(2) = (CARD32)(SIS_SPKC3D_HEADER + VERTBASE +  4 + (num * VERTMULT));
	SIS_WQINDEX(3) = SiSFToDW(y);
	ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
	tt = (char *)pSiS->cmdQueueBase + ttt;
	SIS_WQINDEX(0) = (CARD32)(SIS_SPKC3D_HEADER + VERTBASE +  8 + (num * VERTMULT));
	SIS_WQINDEX(1) = SiSFToDW(0);
	SIS_WQINDEX(2) = (CARD32)(SIS_SPKC3D_HEADER + VERTBASE + 12 + (num * VERTMULT));
	SIS_WQINDEX(3) = SiSFToDW(1);
	ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
	tt = (char *)pSiS->cmdQueueBase + ttt;
	SIS_WQINDEX(0) = (CARD32)(SIS_SPKC3D_HEADER + VERTBASE + VERTTEX + 0 + (num * VERTMULT));
	SIS_WQINDEX(1) = SiSFToDW(tx);
	SIS_WQINDEX(2) = (CARD32)(SIS_SPKC3D_HEADER + VERTBASE + VERTTEX + 4 + (num * VERTMULT));
	SIS_WQINDEX(3) = SiSFToDW(ty);
	ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
	SiSSetSwWP(ttt);
}
#endif /* VRAMQUEUE */
#endif /* 0 */

static Bool
SISAccelerateRotate90(ScrnInfoPtr pScrn, int num, BoxPtr pbox,
		CARD32 sourceOffs, CARD32 destOffs,
		int srcPitch, int dstPitch)
{
#ifdef SISVRAMQ
	SISPtr pSiS = SISPTR(pScrn);
	int    width, height, height2, height3;
	int    dstX, dstY, bpp = pSiS->CurrentLayout.bytesPerPixel;
	SiS_Packet1 MyPacket1;
	CARD32 srcbase, ttt;
	pointer tt;

#if 0
	ErrorF("Rot90 %dx%d - %dx%d (%dx%d)\n", pbox->x1, pbox->y1, pbox->x2, pbox->y2, pbox->x2-pbox->x1, pbox->y2-pbox->y1);
#endif

	memset(&MyPacket1, 0, sizeof(MyPacket1));
	MyPacket1.P1_Header0 = SIS_PACKET1_HEADER0;
	MyPacket1.P1_Header1 = SIS_PACKET1_HEADER1;
	MyPacket1.P1_Null1 = SIS_NIL_CMD;
	MyPacket1.P1_Null2 = SIS_NIL_CMD;

	MyPacket1.P1_SrcPitch = bpp;

	/* MyPacket1.P1_SrcX = MyPacket1.P1_SrcY = 0;  - is zero by memset */

	MyPacket1.P1_DstAddr = destOffs + FBOFFSET;
	MyPacket1.P1_DstPitch = dstPitch;
	MyPacket1.P1_DstHeight = 0x0fff;

	MyPacket1.P1_Command = SRCVIDEO			|
         		       PATFG			|
         		       SBB_DEFAULT_ROP		|
         		       pSiS->SiS310_AccelDepth;

	MyPacket1.P1_RectWidth = 1;

	while(num--) {
	   pbox->x1 &= ~15;
	   width =  pbox->x2 - pbox->x1;
	   height = pbox->y2 - pbox->y1;

	   if(width <= 0 || height <= 0) goto mycontinue2;

	   MyPacket1.P1_RectHeight = width;

	   MyPacket1.P1_SrcAddr = srcbase =
		sourceOffs + FBOFFSET + (pbox->y1 * srcPitch) + (pbox->x1 * bpp);

	   MyPacket1.P1_DstX = dstX = pScrn->virtualX - pbox->y1;
	   MyPacket1.P1_DstY = dstY = pbox->x1;

	   SISWriteBlitPacket3(pSiS, (CARD32*)&MyPacket1);

	   height--;

	   pSiS->CommandReg = MyPacket1.P1_Command;

	   if(!pSiS->NeedFlush) {

	      height2 = height >> 1;

	      ttt = SiSGetSwWP();

	      while(height2--) {

		 srcbase += srcPitch;
		 dstX--;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + DST_Y, (dstX << 16) | dstY, SIS_SPKC_HEADER + SRC_ADDR, srcbase)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

		 srcbase += srcPitch;
		 dstX--;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg, SIS_SPKC_HEADER + DST_Y, (dstX << 16) | dstY)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		 SIS_WQSIMPLE(SIS_SPKC_HEADER + SRC_ADDR, srcbase, SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		 SiSSetHwWP(ttt);

	      }

	      if(height & 1) {

		 srcbase += srcPitch;
		 dstX--;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + DST_Y, (dstX << 16) | dstY, SIS_SPKC_HEADER + SRC_ADDR, srcbase)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		 SIS_WQSIMPLE(SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg, SIS_NIL_CMD, SIS_NIL_CMD)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		 SiSSetHwWP(ttt);

	      }

	   } else {

	      ttt = SiSGetSwWP();

	      if(height > 12) {

		 height2 = height >> 1;

		 height -= height2;

		 while(height2--) {

		    srcbase += srcPitch;
		    dstX--;

		    SIS_WQSIMPLE(SIS_SPKC_HEADER + DST_Y, (dstX << 16) | dstY, SIS_SPKC_HEADER + SRC_ADDR, srcbase)
		    ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

		    srcbase += srcPitch;
		    dstX--;

		    SIS_WQSIMPLE(SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg, SIS_SPKC_HEADER + DST_Y, (dstX << 16) | dstY)
		    ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		    SIS_WQSIMPLE(SIS_SPKC_HEADER + SRC_ADDR, srcbase, SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg)
		    if(!height2) SiSFlush(3)
		    ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

		 }

		 SiSSetHwWP(ttt);

	      }

	      height2 = height >> 1;

	      height3 = height & 1;

	      while(height2--) {

		 srcbase += srcPitch;
		 dstX--;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + DST_Y, (dstX << 16) | dstY, SIS_SPKC_HEADER + SRC_ADDR, srcbase)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

		 srcbase += srcPitch;
		 dstX--;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg, SIS_SPKC_HEADER + DST_Y, (dstX << 16) | dstY)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		 SIS_WQSIMPLE(SIS_SPKC_HEADER + SRC_ADDR, srcbase, SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg)
		 if(!height2 && !height3) SiSFlush(3)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

	      }

	      if(height3) {

		 srcbase += srcPitch;
		 dstX--;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + DST_Y, (dstX << 16) | dstY, SIS_SPKC_HEADER + SRC_ADDR, srcbase)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		 SIS_WQSIMPLE(SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg, SIS_NIL_CMD, SIS_NIL_CMD)
		 SiSFlush(3)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

	      }

	      SiSSetHwWP(ttt);

	   }

mycontinue2:
	   pbox++;
	}

	/*(pSiS->SyncAccel)(pScrn);*/

	return TRUE;
#endif
#if defined(SISVRAMQ) && 0
	SISPtr pSiS = SISPTR(pScrn);
	CARD32 primitive, enable1, enable2, dest0, dest0msk, dest0base, r8b64, r8b68;
	CARD32 tex0fmt, tex0loc, tex0dim, tex0base, tex0pitch, r8d68;
	int dstx, dsty, height, width;
	int i, temp;

	ErrorF("Engine status %x %x\n", SIS_MMIO_IN32(pSiS->IOBase, 0x8af8), SIS_MMIO_IN32(pSiS->IOBase, 0x8afc));

	/* Note: dstPitch = bytes; srcPitch = bytes (both are width * bpp) */

	primitive = (1 << 31) |  /* cull ccw 				8ae8 */
		    (1 << 28) |  /* shading mode */
		    (0 << 26) |  /* tex 0 from tex a */
		    (0 << 16) |  /* disable copy w to z */
		    (0 << 15) |  /* disable vertex cache */
		    (1 << 14) |  /* enable clear g/l engine */
		    (1 << 13) |  /* enable g/l engine */
		    (0 <<  8) |  /* fire after writing to fire */
		    (0 <<  6) |  /* AGP: cmd buffer mode */
		    (0 <<  5) |  /* Line: without last pixel */
		    (8 <<  0);   /* (triangle lists) */

	enable1 = (0 << 23) |	/* L2 tex cache disable 		8b00 */
		  (0 << 22) |	/* stencil test disable */
		  (1 << 21) |	/* clear tex cache */
		  (0 << 20) |	/* z write disable */
		  (0 << 19) |	/* z test disabled */
		  (0 << 18) |	/* triangle alignment disable */
		  (0 << 17) |	/* alpha test disabled */
		  (0 << 16) |	/* cubic mapping disabled */
		  (1 << 15) |	/* disable tag ram test */
		  (1 << 12) |	/* using 1 texture */
		  (0 << 11) |	/* backface culling disabled */
		  (1 << 10) |	/* texture mapping enabled */
		  (0 <<  9) |   /* texture perspective correction disabled */
		  (0 <<  8) |	/* bump map disabled */
		  (0 <<  7) |	/* L1 tex cache disabled */
		  (0 <<  6) |	/* keep tex data */
		  (0 <<  5) |	/* no fog perspective */
		  (0 <<  4) |	/* no specula */
		  (0 <<  3) |	/* fog disabled */
		  (0 <<  2) |	/* fog test disabled */
		  (0 <<  1) |	/* blending disabled */
		  (0 <<  0);	/* dithering disabled */

	enable2 = 0;		/* everything disabled			8b04 */

	dest0   = (1 << 31) |	/* skip alpha blending 			8b20 */
		  (0xc /*1100b*/ << 24) | /* dest0(l) raster operation */
		  (((pScrn->bitsPerPixel == 32) ? 0x34 /*0110100b*/ : 0x11 /*0010001b*/) << 16) | /* dest format */
		  ((dstPitch >> 2) << 0);	/* dest0(l) pitch */

	dest0msk= 0xffffffff;	/* write all bits 			8b24 */

	dest0base=(destOffs + FBOFFSET) >> 1;			/*	8b28 */

	r8b64   = (0x17 /*10111b*/ << 27) |	/* y precision 23 bit 		8b64 */
		  (0 << 13)	 |	/* top clipping */
		  (4095 <<  0);		/* bottom clipping */

	r8b68   = (0 << 13) 	 |	/* left clipping value 		8b68 */
		  (4088 <<  0);		/* right clipping value */

	tex0fmt = (((pScrn->bitsPerPixel == 32) ? 0x74 /*01110100b*/ : 0x31 /*01100001b*/) << 24) | /* 8ba0 */
		  (0x03 /*00000011b*/ << 16);	/* wrap around u and v */

	tex0loc = 0;			/* in local memory (and all others 0) 	     8ba4 */

	tex0dim = (0 /*00b*/ << 30) |		/*					     8ba8 */
		  (pbox->x2 - pbox->x1) |	/* tex width */
		  (pbox->y2 - pbox->y1);	/* tex height */

	tex0base =sourceOffs + FBOFFSET;				/*	     8bb8 */

	tex0pitch= ((srcPitch / 8) << 16) |	/* srcPitch = D[24:16] * 2 ^ D[28:25] --- 8be8*/
		   (8 << 25);			/* assuming that pitch is always modulo 8 */

	r8d68 = (0 << 28) |
		(0 << 12) |   /* Read status register A selection */
		(1 <<  8) |   /* Read status register B selection */
		(0 <<  0);

        /* Set up everything */
        SiSWriteSingleDataFire(pSiS, 0x8d68, r8d68);
        SiSWriteData(pSiS, 0x8ae8, primitive, 0x8b00, enable1);
        SiSWriteData(pSiS, 0x8b04, enable2,   0x8b20, dest0);
        SiSWriteData(pSiS, 0x8b24, dest0msk,  0x8b28, dest0base);
        SiSWriteData(pSiS, 0x8b64, r8b64,     0x8b68, r8b68);
        SiSWriteData(pSiS, 0x8ba0, tex0fmt,   0x8ba4, tex0loc);
        SiSWriteData(pSiS, 0x8ba8, tex0dim,   0x8bb8, tex0base);
        SiSWriteSingleDataFire(pSiS, 0x8be8, tex0pitch);

	while(num--) {
	   dstx   = pScrn->virtualX - pbox->y1;
	   dsty   = pbox->x1;
	   width  = pbox->y2 - pbox->y1;
	   height = pbox->x2 - pbox->x1;

	   /* Triangle 1 */
	   SiSSVertex(pSiS, 0, dstx,             dsty,              pbox->x1,     pbox->y1);
	   SiSSVertex(pSiS, 1, dstx + width - 1, dsty,              pbox->x2 - 1, pbox->y1);
	   SiSSVertex(pSiS, 2, dstx + width - 1, dsty + height - 1, pbox->x2 - 1, pbox->y2 - 1);
	   SiSEndOfPrimitive(pSiS);
	   /* Fire */
	   SiSWriteSingleDataFire(pSiS, 0x8afc, 1);

	   /* Triangle 2 */
	   SiSSVertex(pSiS, 0, dstx,             dsty,              pbox->x1,     pbox->y1);
	   SiSSVertex(pSiS, 1, dstx,             dsty + height - 1, pbox->x1,     pbox->y2 - 1);
	   SiSSVertex(pSiS, 2, dstx + width - 1, dsty + height - 1, pbox->x2 - 1, pbox->y2 - 1);
	   SiSEndOfPrimitive(pSiS);
	   /* Fire */
	   SiSWriteSingleDataFire(pSiS, 0x8afc, 1);

	   inSISIDXREG(SISSR, 0x1e, temp);
	   ErrorF("Engine status %x %x (%x)\n", SIS_MMIO_IN32(pSiS->IOBase, 0x8af8), SIS_MMIO_IN32(pSiS->IOBase, 0x8afc), temp);
	   for(i = 0x8800; i < 0x88c0; i+=4) {
	      ErrorF("Engine register %x: %x\n", i, SIS_MMIO_IN32(pSiS->IOBase, i));
	   }
	   ErrorF("----------------------------------\n");
	}

	return TRUE;
#else	/* VRAMQ */
	return FALSE;
#endif
}

static Bool
SISAccelerateRotate270(ScrnInfoPtr pScrn, int num, BoxPtr pbox,
		CARD32 sourceOffs, CARD32 destOffs,
		int srcPitch, int dstPitch)
{
#ifdef SISVRAMQ
	SISPtr pSiS = SISPTR(pScrn);
	int    width, width2, width3, height;
	int    srcX, srcY, bpp = pSiS->CurrentLayout.bytesPerPixel;
	SiS_Packet1 MyPacket1;
	CARD32 destbase, ttt;
	pointer tt;

	memset(&MyPacket1, 0, sizeof(MyPacket1));
	MyPacket1.P1_Header0 = SIS_PACKET1_HEADER0;
	MyPacket1.P1_Header1 = SIS_PACKET1_HEADER1;
	MyPacket1.P1_Null1 = SIS_NIL_CMD;
	MyPacket1.P1_Null2 = SIS_NIL_CMD;

	MyPacket1.P1_SrcAddr = sourceOffs + FBOFFSET;
	MyPacket1.P1_SrcPitch = srcPitch;

	MyPacket1.P1_DstPitch = bpp;
	MyPacket1.P1_DstHeight = 0x0fff;

	/* MyPacket1.P1_DstX = MyPacket1.P1_DstY = 0; -- is zero by memset */

	MyPacket1.P1_Command = SRCVIDEO			|
         		       PATFG			|
         		       SBB_DEFAULT_ROP		|
         		       pSiS->SiS310_AccelDepth;

	MyPacket1.P1_RectWidth = 1;

	while(num--) {
	   pbox->y1 &= ~15;
	   width =  pbox->x2 - pbox->x1;
	   height = pbox->y2 - pbox->y1;

	   if(width <= 0 || height <= 0) goto mycontinue3;

	   MyPacket1.P1_SrcX = srcX = pbox->x1;
	   MyPacket1.P1_SrcY = srcY = pbox->y1;

	   MyPacket1.P1_RectHeight = height;

	   MyPacket1.P1_DstAddr = destbase =
	   	destOffs + FBOFFSET + ((pScrn->virtualY - pbox->x1 - 1) * dstPitch) + (pbox->y1 * bpp);

	   SISWriteBlitPacket3(pSiS, (CARD32*)&MyPacket1);

	   width--;

	   pSiS->CommandReg = MyPacket1.P1_Command;

	   if(!pSiS->NeedFlush) {

	      width2 = width >> 1;

	      ttt = SiSGetSwWP();

	      while(width2--) {

		 destbase -= dstPitch;
		 srcX++;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + SRC_Y, (srcX << 16) | srcY, SIS_SPKC_HEADER + DST_ADDR, destbase)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

		 destbase -= dstPitch;
	         srcX++;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg, SIS_SPKC_HEADER + SRC_Y, (srcX << 16) | srcY)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		 SIS_WQSIMPLE(SIS_SPKC_HEADER + DST_ADDR, destbase, SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

		 SiSSetHwWP(ttt);

	      }

	      if(width & 1) {

		 destbase -= dstPitch;
		 srcX++;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + SRC_Y, (srcX << 16) | srcY, SIS_SPKC_HEADER + DST_ADDR, destbase)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		 SIS_WQSIMPLE(SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg, SIS_NIL_CMD, SIS_NIL_CMD)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

		 SiSSetHwWP(ttt);

	      }

	   } else {

	      ttt = SiSGetSwWP();

	      if(width > 12) {

		 width2 = width >> 1;

		 width -= width2;

		 while(width2--) {

		    destbase -= dstPitch;
		    srcX++;

		    SIS_WQSIMPLE(SIS_SPKC_HEADER + SRC_Y, (srcX << 16) | srcY, SIS_SPKC_HEADER + DST_ADDR, destbase)
		    ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

		    destbase -= dstPitch;
		    srcX++;

		    SIS_WQSIMPLE(SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg, SIS_SPKC_HEADER + SRC_Y, (srcX << 16) | srcY)
		    ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		    SIS_WQSIMPLE(SIS_SPKC_HEADER + DST_ADDR, destbase, SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg)
		    if(!width2) SiSFlush(3)
		    ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

	         }

	         SiSSetHwWP(ttt);

	      }

	      width2 = width >> 1;

	      width3 = width & 1;

	      while(width2--) {

		 destbase -= dstPitch;
		 srcX++;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + SRC_Y, (srcX << 16) | srcY, SIS_SPKC_HEADER + DST_ADDR, destbase)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

		 destbase -= dstPitch;
		 srcX++;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg, SIS_SPKC_HEADER + SRC_Y, (srcX << 16) | srcY)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		 SIS_WQSIMPLE(SIS_SPKC_HEADER + DST_ADDR, destbase, SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg)
		 if(!width2 && !width3) SiSFlush(3)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

	      }

	      if(width3) {

		 destbase -= dstPitch;
		 srcX++;

		 SIS_WQSIMPLE(SIS_SPKC_HEADER + SRC_Y, (srcX << 16) | srcY, SIS_SPKC_HEADER + DST_ADDR, destbase)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);
		 SIS_WQSIMPLE(SIS_SPKC_HEADER + COMMAND_READY, pSiS->CommandReg, SIS_NIL_CMD, SIS_NIL_CMD)
		 SiSFlush(3)
		 ttt = SISSiSUpdateQueue(pSiS, ttt, tt);

	      }

	      SiSSetHwWP(ttt);

	   }

mycontinue3:
	   pbox++;
	}

	/*(pSiS->SyncAccel)(pScrn);*/

	return TRUE;
#else	/* VRAMQ */
	return FALSE;
#endif
}
#endif /* SIS_INCL_RRROT */

/* For DGA usage */

static void
SiSDGAFillRect(ScrnInfoPtr pScrn, int x, int y, int w, int h, int color)
{
	SiSSetupForSolidFill(pScrn, color, GXcopy, ~0);
	SiSSubsequentSolidFillRect(pScrn, x, y, w, h);
}

static void
SiSDGABlitRect(ScrnInfoPtr pScrn, int srcx, int srcy, int dstx, int dsty, int w, int h, int color)
{
	/* Don't need xdir, ydir */
	SiSSetupForScreenToScreenCopy(pScrn, 0, 0, GXcopy, (CARD32)~0, color);
	SiSSubsequentScreenToScreenCopy(pScrn, srcx, srcy, dstx, dsty, w, h);
}

/* Initialisation */

Bool
SiS315AccelInit(ScreenPtr pScreen)
{
	ScrnInfoPtr     pScrn = xf86Screens[pScreen->myNum];
	SISPtr          pSiS = SISPTR(pScrn);
#ifdef SIS_USE_XAA
	XAAInfoRecPtr   infoPtr = NULL;
	int		topFB, reservedFbSize, usableFbSize;
	BoxRec          Avail;
#endif /* XAA */

	pSiS->ColorExpandBufferNumber = 0;
	pSiS->PerColorExpandBufferSize = 0;
	pSiS->RenderAccelArray = NULL;
#ifdef SIS_USE_XAA
	pSiS->AccelInfoPtr = NULL;
#endif
#ifdef SIS_USE_EXA
	pSiS->EXADriverPtr = NULL;
	pSiS->exa_scratch = NULL;
#endif

	if((pScrn->bitsPerPixel != 8)  &&
	   (pScrn->bitsPerPixel != 16) &&
	   (pScrn->bitsPerPixel != 32)) {
	   pSiS->NoAccel = TRUE;
	}

	if(!pSiS->NoAccel) {
#ifdef SIS_USE_XAA
	   if(!pSiS->useEXA) {
	      pSiS->AccelInfoPtr = infoPtr = XAACreateInfoRec();
	      if(!infoPtr) pSiS->NoAccel = TRUE;
	   }
#endif
#ifdef SIS_USE_EXA
	   if(pSiS->useEXA) {
#ifdef SISISXORGPOST70
	      if(!(pSiS->EXADriverPtr = exaDriverAlloc()))
#else
	      if(!(pSiS->EXADriverPtr = xnfcalloc(sizeof(ExaDriverRec), 1)))
#endif
	      {
		 pSiS->NoAccel = TRUE;
		 pSiS->NoXvideo = TRUE; /* No fbmem manager -> no xv */
	      }
	   }
#endif
	}

	if(!pSiS->NoAccel) {

	   SiSInitializeAccelerator(pScrn);

	   pSiS->InitAccel = SiSInitializeAccelerator;
	   pSiS->SyncAccel = SiSSyncAccel;
	   pSiS->FillRect  = SiSDGAFillRect;
	   pSiS->BlitRect  = SiSDGABlitRect;

#ifdef SIS_INCL_RRROT
	   pSiS->AccelerateReflect = SISAccelerateReflect;
	   pSiS->AccelerateRotate90 = SISAccelerateRotate90;
	   pSiS->AccelerateRotate270 = SISAccelerateRotate270;
#endif

#ifdef SIS_USE_XAA	/* ----------------------- XAA ----------------------- */
	   if(!pSiS->useEXA) {

	      infoPtr->Flags = LINEAR_FRAMEBUFFER |
			       OFFSCREEN_PIXMAPS |
			       PIXMAP_CACHE;

	      /* sync */
	      infoPtr->Sync = SiSSync;

	      /* BitBlt */
	      infoPtr->SetupForScreenToScreenCopy = SiSSetupForScreenToScreenCopy;
	      infoPtr->SubsequentScreenToScreenCopy = SiSSubsequentScreenToScreenCopy;
	      infoPtr->ScreenToScreenCopyFlags = NO_PLANEMASK | TRANSPARENCY_GXCOPY_ONLY;

	      /* solid fills */
	      infoPtr->SetupForSolidFill = SiSSetupForSolidFill;
	      infoPtr->SubsequentSolidFillRect = SiSSubsequentSolidFillRect;
	      infoPtr->SolidFillFlags = NO_PLANEMASK;

	      /* solid line */
	      infoPtr->SetupForSolidLine = SiSSetupForSolidLine;
	      infoPtr->SubsequentSolidTwoPointLine = SiSSubsequentSolidTwoPointLine;
	      infoPtr->SubsequentSolidHorVertLine = SiSSubsequentSolidHorzVertLine;
	      infoPtr->SolidLineFlags = NO_PLANEMASK;

	      /* dashed line */
	      infoPtr->SetupForDashedLine = SiSSetupForDashedLine;
	      infoPtr->SubsequentDashedTwoPointLine = SiSSubsequentDashedTwoPointLine;
	      infoPtr->DashPatternMaxLength = 64;
	      infoPtr->DashedLineFlags = NO_PLANEMASK |
					 LINE_PATTERN_MSBFIRST_LSBJUSTIFIED;

	      /* 8x8 mono pattern fill */
	      infoPtr->SetupForMono8x8PatternFill = SiSSetupForMonoPatternFill;
	      infoPtr->SubsequentMono8x8PatternFillRect = SiSSubsequentMonoPatternFill;
	      infoPtr->Mono8x8PatternFillFlags = NO_PLANEMASK |
						 HARDWARE_PATTERN_SCREEN_ORIGIN |
						 HARDWARE_PATTERN_PROGRAMMED_BITS |
						 BIT_ORDER_IN_BYTE_MSBFIRST;

#ifdef SISVRAMQ
	      /* 8x8 color pattern fill (MMIO support not implemented) */
	      infoPtr->SetupForColor8x8PatternFill = SiSSetupForColor8x8PatternFill;
	      infoPtr->SubsequentColor8x8PatternFillRect = SiSSubsequentColor8x8PatternFillRect;
	      infoPtr->Color8x8PatternFillFlags = NO_PLANEMASK |
						  HARDWARE_PATTERN_SCREEN_ORIGIN |
						  NO_TRANSPARENCY;
#endif

#if defined(RENDER) && defined(INCL_RENDER)
	      /* Render */
	      SiSCalcRenderAccelArray(pScrn);

	      if(pSiS->RenderAccelArray) {
	         pSiS->AccelLinearScratch = NULL;

#ifdef SISNEWRENDER
		 infoPtr->SetupForCPUToScreenAlphaTexture2 = SiSSetupForCPUToScreenAlphaTexture;
		 infoPtr->CPUToScreenAlphaTextureDstFormats = (pScrn->bitsPerPixel == 16) ?
				SiSDstTextureFormats16 : SiSDstTextureFormats32;
#else
		 infoPtr->SetupForCPUToScreenAlphaTexture = SiSSetupForCPUToScreenAlphaTexture;
#endif
		 infoPtr->SubsequentCPUToScreenAlphaTexture = SiSSubsequentCPUToScreenTexture;
		 infoPtr->CPUToScreenAlphaTextureFormats = SiSAlphaTextureFormats;
		 infoPtr->CPUToScreenAlphaTextureFlags = XAA_RENDER_NO_TILE;

#ifdef SISNEWRENDER
		 infoPtr->SetupForCPUToScreenTexture2 = SiSSetupForCPUToScreenTexture;
		 infoPtr->CPUToScreenTextureDstFormats = (pScrn->bitsPerPixel == 16) ?
				SiSDstTextureFormats16 : SiSDstTextureFormats32;
#else
		 infoPtr->SetupForCPUToScreenTexture = SiSSetupForCPUToScreenTexture;
#endif
		 infoPtr->SubsequentCPUToScreenTexture = SiSSubsequentCPUToScreenTexture;
		 infoPtr->CPUToScreenTextureFormats = SiSTextureFormats;
		 infoPtr->CPUToScreenTextureFlags = XAA_RENDER_NO_TILE;

		 xf86DrvMsg(pScrn->scrnIndex, X_INFO, "RENDER acceleration enabled\n");
	      }
#endif /* RENDER && INCL_RENDER */

#ifdef SISDUALHEAD
	      if(pSiS->DualHeadMode) {
		 infoPtr->RestoreAccelState = SiSRestoreAccelState;
	      }
#endif
	   }  /* !EXA */
#endif /* XAA */

#ifdef SIS_USE_EXA	/* ----------------------- EXA ----------------------- */
	   if(pSiS->useEXA) {

	      int obase = 0;

#ifdef SISISXORGPOST70
	      pSiS->EXADriverPtr->exa_major = 2;
	      pSiS->EXADriverPtr->exa_minor = 0;
#endif

	      /* data */
#ifdef SISISXORGPOST70
	      pSiS->EXADriverPtr->memoryBase = pSiS->FbBase;
	      pSiS->EXADriverPtr->memorySize = pSiS->maxxfbmem;
#else
	      pSiS->EXADriverPtr->card.memoryBase = pSiS->FbBase;
	      pSiS->EXADriverPtr->card.memorySize = pSiS->maxxfbmem;
#endif

#ifdef SIS_INCL_RRROT
	      if(pSiS->SupportRRRotation && pSiS->SupportRRRotation90270) {
	         int mymax = max(pScrn->displayWidth, pScrn->virtualY);
	         obase = mymax * mymax * (pScrn->bitsPerPixel >> 3);
#ifdef SISISXORGPOST70
	         if(pSiS->EXADriverPtr->memorySize <= obase)
#else
	         if(pSiS->EXADriverPtr->card.memorySize <= obase)
#endif
	         {
	            xf86DrvMsg(pScrn->scrnIndex, X_INFO,
			"Not enough video RAM for 90/270 degrees screen rotation.\n");
		    pSiS->SupportRRRotation90270 = FALSE;
		    obase = 0;
	         }
	      }
#endif

	      if(!obase) {
	         obase = pScrn->displayWidth * pScrn->virtualY * (pScrn->bitsPerPixel >> 3);
	      }

#ifdef SISISXORGPOST70
	      pSiS->EXADriverPtr->offScreenBase = obase;
	      if(pSiS->EXADriverPtr->memorySize > pSiS->EXADriverPtr->offScreenBase) {
		 pSiS->EXADriverPtr->flags = EXA_OFFSCREEN_PIXMAPS;
#else
	      pSiS->EXADriverPtr->card.offScreenBase = obase;
	      if(pSiS->EXADriverPtr->card.memorySize > pSiS->EXADriverPtr->card.offScreenBase) {
		 pSiS->EXADriverPtr->card.flags = EXA_OFFSCREEN_PIXMAPS;
#endif
	      } else {
		 pSiS->NoXvideo = TRUE;
#ifdef SIS_INCL_RRROT
		 pSiS->SupportRRRotation = FALSE;
#endif
		 xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
			"Not enough video RAM for offscreen memory manager. Xv disabled\n");
	      }
#ifdef SISISXORGPOST70
	      pSiS->EXADriverPtr->pixmapOffsetAlign = 16;	/* src/dst: double quad word boundary */
	      pSiS->EXADriverPtr->pixmapPitchAlign = 4;	/* pitch:   double word boundary      */
	      pSiS->EXADriverPtr->maxX = 4095;
	      pSiS->EXADriverPtr->maxY = 4095;
#else
	      pSiS->EXADriverPtr->card.pixmapOffsetAlign = 16;	/* src/dst: double quad word boundary */
	      pSiS->EXADriverPtr->card.pixmapPitchAlign = 4;	/* pitch:   double word boundary      */
	      pSiS->EXADriverPtr->card.maxX = 4095;
	      pSiS->EXADriverPtr->card.maxY = 4095;
#endif

#ifdef SISISXORGPOST70
	      /* Sync */
	      pSiS->EXADriverPtr->WaitMarker = SiSEXASync;

	      /* Solid fill */
	      pSiS->EXADriverPtr->PrepareSolid = SiSPrepareSolid;
	      pSiS->EXADriverPtr->Solid = SiSSolid;
	      pSiS->EXADriverPtr->DoneSolid = SiSDoneSolid;

	      /* Copy */
	      pSiS->EXADriverPtr->PrepareCopy = SiSPrepareCopy;
	      pSiS->EXADriverPtr->Copy = SiSCopy;
	      pSiS->EXADriverPtr->DoneCopy = SiSDoneCopy;

	      /* Composite */
#ifdef SIS_HAVE_COMPOSITE
	      SiSCalcRenderAccelArray(pScrn);
	      if(pSiS->RenderAccelArray) {
		 pSiS->EXADriverPtr->CheckComposite = SiSCheckComposite;
		 pSiS->EXADriverPtr->PrepareComposite = SiSPrepareComposite;
		 pSiS->EXADriverPtr->Composite = SiSComposite;
		 pSiS->EXADriverPtr->DoneComposite = SiSDoneComposite;
	      }
#endif

	      /* Upload, download to/from Screen */
	      pSiS->EXADriverPtr->UploadToScreen = SiSUploadToScreen;
	      pSiS->EXADriverPtr->DownloadFromScreen = SiSDownloadFromScreen;

#else
	      /* Sync */
	      pSiS->EXADriverPtr->accel.WaitMarker = SiSEXASync;

	      /* Solid fill */
	      pSiS->EXADriverPtr->accel.PrepareSolid = SiSPrepareSolid;
	      pSiS->EXADriverPtr->accel.Solid = SiSSolid;
	      pSiS->EXADriverPtr->accel.DoneSolid = SiSDoneSolid;

	      /* Copy */
	      pSiS->EXADriverPtr->accel.PrepareCopy = SiSPrepareCopy;
	      pSiS->EXADriverPtr->accel.Copy = SiSCopy;
	      pSiS->EXADriverPtr->accel.DoneCopy = SiSDoneCopy;

	      /* Composite */
#ifdef SIS_HAVE_COMPOSITE
	      SiSCalcRenderAccelArray(pScrn);
	      if(pSiS->RenderAccelArray) {
		 pSiS->EXADriverPtr->accel.CheckComposite = SiSCheckComposite;
		 pSiS->EXADriverPtr->accel.PrepareComposite = SiSPrepareComposite;
		 pSiS->EXADriverPtr->accel.Composite = SiSComposite;
		 pSiS->EXADriverPtr->accel.DoneComposite = SiSDoneComposite;
	      }
#endif

	      /* Upload, download to/from Screen */
	      pSiS->EXADriverPtr->accel.UploadToScreen = SiSUploadToScreen;
	      pSiS->EXADriverPtr->accel.DownloadFromScreen = SiSDownloadFromScreen;
#endif /* POST70 */

	   }
#endif

	}  /* NoAccel */

	/* Init framebuffer memory manager */

	/* Traditional layout:
	 *   |-----------------++++++++++++++++++++^************==========~~~~~~~~~~~~|
	 *   |  UsableFbSize    ColorExpandBuffers |  DRI-Heap   HWCursor  CommandQueue
	 * FbBase                                topFB
	 *   +-------------maxxfbmem---------------+
	 *
	 * On SiS76x with UMA+LFB:
	 * |UUUUUUUUUUUUUUU--------------++++++++++++++++++++^==========~~~~~~~~~~~~|
	 *     DRI heap    |UsableFbSize  ColorExpandBuffers | HWCursor  CommandQueue
	 *  (in UMA and   FbBase                           topFB
	 *   eventually    +---------- maxxfbmem ------------+
	 *  beginning of
	 *      LFB)
	 */

#ifdef SIS_USE_XAA
	if(!pSiS->useEXA) {

#ifdef SIS_INCL_RRROT
	   int backupVirtX, backupVirtY;
#endif

	   topFB = pSiS->maxxfbmem; /* relative to FbBase */

	   reservedFbSize = pSiS->ColorExpandBufferNumber * pSiS->PerColorExpandBufferSize;

	   usableFbSize = topFB - reservedFbSize;

	   Avail.x1 = 0;
	   Avail.y1 = 0;
	   Avail.x2 = pScrn->displayWidth;
	   Avail.y2 = (usableFbSize / (pScrn->displayWidth * pScrn->bitsPerPixel / 8)) - 1;

	   if(Avail.y2 < 0) Avail.y2 = 32767;

	   if(Avail.y2 < pScrn->currentMode->VDisplay) {
	      xf86DrvMsg(pScrn->scrnIndex, X_ERROR,
			"Not enough video RAM for accelerator. "
			"%dKB needed, %dKB available\n",
			((((pScrn->displayWidth * pScrn->bitsPerPixel / 8)   /* +8 for make it sure */
			     * pScrn->currentMode->VDisplay) + reservedFbSize) / 1024) + 8,
			pSiS->maxxfbmem/1024);
	      pSiS->NoAccel = TRUE;
	      pSiS->NoXvideo = TRUE;
#ifdef SIS_INCL_RRROT
	      pSiS->SupportRRRotation = FALSE;
#endif
	      XAADestroyInfoRec(pSiS->AccelInfoPtr);
	      pSiS->AccelInfoPtr = NULL;
	      return FALSE;   /* Don't even init fb manager */
	   }

	   xf86DrvMsg(pScrn->scrnIndex, X_INFO,
		   "Framebuffer from (%d,%d) to (%d,%d)\n",
		   Avail.x1, Avail.y1, Avail.x2 - 1, Avail.y2 - 1);

#ifdef SIS_INCL_RRROT
	   /* For RandR 90/270 degree rotation, we need a square framebuffer
	    * area. Fake this to the FBManager now so that it starts its
	    * pixmap cache above this area.
	    * The fb manager uses virtualX and not displayWidth, so we
	    * need to make these identical.
	    */
	   backupVirtX = pScrn->virtualX;
	   backupVirtY = pScrn->virtualY;
	   if(pSiS->SupportRRRotation && pSiS->SupportRRRotation90270) {
	      if(Avail.y2 < pScrn->displayWidth) {
		 pSiS->SupportRRRotation90270 = FALSE;
		 xf86DrvMsg(pScrn->scrnIndex, X_INFO,
			"Not enough video RAM for 90/270 degrees screen rotation.\n");
	      } else {
		 pScrn->virtualX = pScrn->displayWidth;
		 pScrn->virtualY = max(pScrn->displayWidth, pScrn->virtualY);
	      }
	   }
#endif

	   xf86InitFBManager(pScreen, &Avail);

#ifdef SIS_INCL_RRROT
	   pScrn->virtualX = backupVirtX;
	   pScrn->virtualY = backupVirtY;
#endif

	   if(!pSiS->NoAccel) {
	      return XAAInit(pScreen, infoPtr);
	   }
	} /* !EXA */
#endif /* XAA */

#ifdef SIS_USE_EXA
	if(pSiS->useEXA) {

	   if(!pSiS->NoAccel) {

	      if(!exaDriverInit(pScreen, pSiS->EXADriverPtr)) {
		 pSiS->NoAccel = TRUE;
		 pSiS->NoXvideo = TRUE; /* No fbmem manager -> no xv */
#ifdef SIS_INCL_RRROT
		 pSiS->SupportRRRotation = FALSE;
#endif
		 return FALSE;
	      }

	      /* Reserve locked offscreen scratch area of 128K for glyph data */
	      pSiS->exa_scratch = exaOffscreenAlloc(pScreen, 128 * 1024, 16, TRUE,
						SiSScratchSave, pSiS);
	      if(pSiS->exa_scratch) {
		 pSiS->exa_scratch_next = pSiS->exa_scratch->offset;
#ifdef SISISXORGPOST70
		 pSiS->EXADriverPtr->UploadToScratch = SiSUploadToScratch;
#else
		 pSiS->EXADriverPtr->accel.UploadToScratch = SiSUploadToScratch;
#endif
	      }

	   } else {

	      pSiS->NoXvideo = TRUE; /* No fbmem manager -> no xv */
#ifdef SIS_INCL_RRROT
	      pSiS->SupportRRRotation = FALSE;
#endif

	   }

	}
#endif /* EXA */

	return TRUE;
}





/*
 * SiS hardware cursor handling
 *
 * Copyright (C) 2001-2005 by Thomas Winischhofer, Vienna, Austria.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1) Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2) Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3) The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:   Thomas Winischhofer <thomas@winischhofer.net>
 *
 * Idea based on code by Can-Ru Yeou, SiS Inc.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sis.h"

#include "cursorstr.h"

#define SIS_NEED_inSISREG
#define SIS_NEED_outSISREG
#define SIS_NEED_inSISIDXREG
#define SIS_NEED_outSISIDXREG
#define SIS_NEED_orSISIDXREG
#define SIS_NEED_andSISIDXREG
#define SIS_NEED_MYMMIO
#define SIS_NEED_MYFBACCESS
#include "sis_regs.h"
#include "sis_cursor.h"

extern void    SISWaitRetraceCRT1(ScrnInfoPtr pScrn);
extern void    SISWaitRetraceCRT2(ScrnInfoPtr pScrn);

/* Preface statement: All routines in this file are being executed
 * asynchronously if SilkenMouse support is enabled (which it is by
 * default). We need to restore the hardware state.
 */

/*******************************************/
/*                 Helpers                 */
/*******************************************/

#ifdef SIS_INCL_RRROT
static const unsigned char rot180array[256] = {
	0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0, 0x10, 0x90, 0x50, 0xd0,
	0x30, 0xb0, 0x70, 0xf0, 0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
	0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8, 0x04, 0x84, 0x44, 0xc4,
	0x24, 0xa4, 0x64, 0xe4, 0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
	0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec, 0x1c, 0x9c, 0x5c, 0xdc,
	0x3c, 0xbc, 0x7c, 0xfc, 0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
	0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2, 0x0a, 0x8a, 0x4a, 0xca,
	0x2a, 0xaa, 0x6a, 0xea, 0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
	0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6, 0x16, 0x96, 0x56, 0xd6,
	0x36, 0xb6, 0x76, 0xf6, 0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
	0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe, 0x01, 0x81, 0x41, 0xc1,
	0x21, 0xa1, 0x61, 0xe1, 0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
	0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9, 0x19, 0x99, 0x59, 0xd9,
	0x39, 0xb9, 0x79, 0xf9, 0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
	0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5, 0x0d, 0x8d, 0x4d, 0xcd,
	0x2d, 0xad, 0x6d, 0xed, 0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
	0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3, 0x13, 0x93, 0x53, 0xd3,
	0x33, 0xb3, 0x73, 0xf3, 0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
	0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb, 0x07, 0x87, 0x47, 0xc7,
	0x27, 0xa7, 0x67, 0xe7, 0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
	0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef, 0x1f, 0x9f, 0x5f, 0xdf,
	0x3f, 0xbf, 0x7f, 0xff
};
#endif

static void
SiSUploadMonoCursor(SISPtr pSiS, Bool ds, UChar *src, UChar *dst)
{
    UChar *finaldest = dst;
    UChar *finalsrc = src;
    int i;
#ifdef SIS_INCL_RRROT
    UChar *tempsrc;
    Uchar *tempdst;
    int j, k, l, m, n, wh;

    if(!pSiS->Rotate && !pSiS->Reflect && (!pSiS->RRRotate || (pSiS->RRRotate == RR_Rotate_0))) {

       /* Nothing */

    } else if(pSiS->RRRotate == RR_Rotate_90 || pSiS->Rotate == 1) {

       wh = ds ? 4 : 8;
       tempdst = pSiS->CursorScratch + 1024;
       memset(tempdst, 0, 1024);

       tempsrc = src;
       for(i = 0, m = 1, n = 7; i < 64; i++) {
          for(l = 0, k = 0; l < wh; l++) {
             for(j = 128; j != 0; j >>= 1, k += 16) {
                if((*tempsrc) & j)       tempdst[k + n] |= m;
                if((*(tempsrc + 8)) & j) tempdst[k + n + 8] |= m;
             }
             tempsrc++;
          }
          if(ds) tempsrc += 4;
          tempsrc += 8;
          m <<= 1;
          if(m > 128) {
             m = 1;
             n--;
          }
       }

       finalsrc = pSiS->CursorScratch + 1024;

    } else if(pSiS->RRRotate == RR_Rotate_270 || pSiS->Rotate == -1) {

       wh = ds ? 4 : 8;
       tempdst = pSiS->CursorScratch + 1024;
       memset(tempdst, 0, 1024);

       tempsrc = src;
       for(i = 0, m = 128, n = 0; i < 64; i++) {
          for(l = 0, k = ((wh * 8) - 1) * 16; l < wh; l++) {
             for(j = 128; j != 0; j >>= 1, k -= 16) {
                if((*tempsrc) & j)       tempdst[k + n] |= m;
                if((*(tempsrc + 8)) & j) tempdst[k + n + 8] |= m;
             }
             tempsrc++;
          }
          if(ds) tempsrc += 4;
          tempsrc += 8;
          m >>= 1;
          if(m == 0) {
             m = 128;
             n++;
          }
       }

       finalsrc = pSiS->CursorScratch + 1024;

    } else if(pSiS->RRRotate == RR_Rotate_180 || pSiS->Reflect == 3) {

       wh = ds ? 32 : 64;
       tempsrc = src;
       tempdst = pSiS->CursorScratch + 1024 + (wh * 16) - 1 - 8;

       for(i = 0; i < wh; i++) {
          for(j = 0; j < 8; j++) {
             sisfbwriteb((tempdst + 8), rot180array[sisfbreadb((tempsrc + 8))]);
             sisfbwritebdec(tempdst, rot180array[sisfbreadbinc(tempsrc)]);
          }
          tempsrc += 8;
          tempdst -= 8;
       }

       finalsrc = pSiS->CursorScratch + 1024;

    } else if(pSiS->Reflect == 1) {

       wh = ds ? 32 : 64;
       tempsrc = src;
       tempdst = pSiS->CursorScratch + 1024 + 8 - 1;

       for(i = 0; i < wh; i++) {
          for(j = 0; j < 8; j++) {
             sisfbwriteb((tempdst + 8), rot180array[sisfbreadb((tempsrc + 8))]);
             sisfbwritebdec(tempdst, rot180array[sisfbreadbinc(tempsrc)]);
          }
          tempsrc += 8;
          tempdst += 16 + 8;
       }

       finalsrc = pSiS->CursorScratch + 1024;

    } else if(pSiS->Reflect == 2) {

       CARD32 *tempsrc2 = (CARD32 *)src;
       CARD32 *tempdst2;

       wh = ds ? 32 : 64;

       tempdst = pSiS->CursorScratch + 1024 + ((wh - 1) * 16);
       tempdst2 = (CARD32 *)tempdst;

       for(i = 0; i < wh; i++) {
          sisfbwritelinc(tempdst2, sisfbreadlinc(tempsrc2));
          sisfbwritelinc(tempdst2, sisfbreadlinc(tempsrc2));
          sisfbwritelinc(tempdst2, sisfbreadlinc(tempsrc2));
          sisfbwritelinc(tempdst2, sisfbreadlinc(tempsrc2));
          tempdst2 -= (32 / 4);
       }

       finalsrc = pSiS->CursorScratch + 1024;

    }
#endif

    if(ds) {
       for(i = 0; i < 32; i++) {
	  SiSMemCopyToVideoRam(pSiS, finaldest + (32 * i), finalsrc + (16 * i), 16);
	  SiSMemCopyToVideoRam(pSiS, finaldest + (32 * i) + 16, finalsrc + (16 * i), 16);
       }
    } else {
       SiSMemCopyToVideoRam(pSiS, finaldest, finalsrc, 1024);
    }
}

static void
SiSUploadColorCursor(SISPtr pSiS, Bool ds, UChar *src, CARD32 *dst, int widthheight)
{
#ifdef SIS_INCL_RRROT
    CARD32 *mysrc, *mydest;
    int i, j, k;

    /* ds (doublesize) only relevant for rotation 90, 270 here; otherwise
     * already handled.
     */

    if(!pSiS->Rotate && !pSiS->Reflect && (!pSiS->RRRotate || (pSiS->RRRotate == RR_Rotate_0))) {

       SiSMemCopyToVideoRam(pSiS, (UChar *)dst, src, widthheight * 4 * widthheight);

    } else if(pSiS->RRRotate == RR_Rotate_90 || pSiS->Rotate == 1) {

       mysrc = (CARD32 *)src;

       if(ds) {
          for(i = 0, k = widthheight - 1; i < widthheight; i++, k--) {
             for(j = 0; j < (widthheight * widthheight); j += (widthheight << 1)) {
                dst[j + k] = dst[j + widthheight + k] = *mysrc++;
             }
             mysrc += (widthheight / 2);
          }
       } else {
          for(i = 0, k = widthheight - 1; i < widthheight; i++, k--) {
             for(j = 0; j < (widthheight * widthheight); j += widthheight) {
                dst[j + k] = *mysrc++;
             }
          }
       }

    } else if(pSiS->RRRotate == RR_Rotate_270 || pSiS->Rotate == -1) {

       mysrc = (CARD32*)src;

       if(ds) {
          for(i = 0, k = 0; i < widthheight; i++, k++) {
             for(j = (widthheight - 1) * widthheight; j >= 0; j -= (widthheight << 1)) {
                dst[j + k] = dst[j - widthheight + k] = *mysrc++;
             }
             mysrc += (widthheight / 2);
          }
       } else {
          for(i = 0, k = 0; i < widthheight; i++, k++) {
             for(j = ((widthheight - 1) * widthheight); j >= 0; j -= widthheight) {
                dst[j + k] = *mysrc++;
             }
          }
       }

    } else if(pSiS->RRRotate == RR_Rotate_180 || pSiS->Reflect == 3) {

       mysrc = (CARD32*)src;
       mydest = dst + (widthheight * widthheight) - 1;

       for(i = 0; i < (widthheight * widthheight); i++) {
          *mydest-- = *mysrc++;
       }

    } else if(pSiS->Reflect == 1) {  /* X */

       mysrc = (CARD32*)src;
       mydest = dst + widthheight - 1;

       for(i = 0; i < widthheight; i++) {
          for(j = 0; j < widthheight; j++) {
             *mydest-- = *mysrc++;
          }
          mydest += (widthheight * 2);
       }

    } else if(pSiS->Reflect == 2) {  /* Y */

       mysrc = (CARD32*)src;
       mydest = dst + ((widthheight - 1) * widthheight);

       for(i = 0; i < widthheight; i++) {
          for(j = 0; j < widthheight; j++) {
             *mydest++ = *mysrc++;
          }
          mydest -= (widthheight * 2);
       }

    }
#else
    SiSMemCopyToVideoRam(pSiS, (UChar *)dst, src, widthheight * 4 * widthheight);
#endif
}

/* Helper function for Xabre to convert mono image to ARGB */
/* The Xabre's cursor engine for CRT2 is buggy and can't
 * handle mono cursors. We therefore convert the mono image
 * to ARGB.
 */
static void
SiSXConvertMono2ARGB(SISPtr pSiS)
{
   UChar  *src = pSiS->CurMonoSrc;
   CARD32 *dest = (CARD32 *)pSiS->CursorScratch;
   CARD8  chunk, mask;
   CARD32 fg = pSiS->CurFGCol | 0xff000000;
   CARD32 bg = pSiS->CurBGCol | 0xff000000;
   int i, j, k;

   if(!dest || !src) return;

   for(i = 0; i < 64; i++) {
      for(j = 0; j < 8; j++) {
         chunk = sisfbreadb(src + 8); /* *(src + 8);  */
	 mask = sisfbreadbinc(src);   /* *src++; */
	 for(k = 128; k != 0; k >>= 1) {
	    if(mask & k)       sisfbwritelinc(dest, 0x00000000);  /* *dest++ = 0x00000000; */
	    else if(chunk & k) sisfbwritelinc(dest, fg);          /* *dest++ = fg; */
	    else               sisfbwritelinc(dest, bg);          /* *dest++ = bg; */
	 }
      }
      src += 8;
   }

   SiSUploadColorCursor(pSiS, pSiS->CursorDoubleSize, (UChar *)pSiS->CursorScratch,
   						(CARD32 *)pSiS->CurARGBDest, 64);
}

#ifdef SISDUALHEAD
static void
UpdateHWCursorStatus(SISPtr pSiS)
{
    int i, offs = 0;

    if(pSiS->SecondHead) offs = 8;

    for(i = 0; i < 8; i++) {
       pSiS->HWCursorBackup[offs + i] = SIS_MMIO_IN32(pSiS->IOBase, 0x8500 + ((offs + i) << 2));
    }
}
#endif

#ifdef SIS_INCL_RRROT
static void
SiSGetRealCursorPos(SISPtr pSiS, int *x, int *y, int crt)
{
    int width, height, oldx, oldy;

    if(!pSiS->Rotate && !pSiS->Reflect && (!pSiS->RRRotate || (pSiS->RRRotate == RR_Rotate_0)))
       return;

#ifdef SISMERGED
    if(pSiS->MergedFB) {
       if(crt == 1) {
          width = CDMPTR->CRT1->HDisplay;
          height = CDMPTR->CRT1->VDisplay;
       } else {
          width = CDMPTR->CRT2->HDisplay;
          height = CDMPTR->CRT2->VDisplay;
       }
    } else {
#endif
       width = pSiS->CurrentLayout.mode->HDisplay;
       height = pSiS->CurrentLayout.mode->VDisplay;
#ifdef SISMERGED
    }
#endif

    oldx = (*x);
    oldy = (*y);

    if(pSiS->RRRotate == RR_Rotate_90 || pSiS->Rotate == 1) {

       *x = width - oldy - pSiS->CursorW;
       *y = oldx;

    } else if(pSiS->RRRotate == RR_Rotate_270 || pSiS->Rotate == -1) {

       *x = oldy;
       *y = height - oldx - pSiS->CursorH;

    } else if(pSiS->RRRotate == RR_Rotate_180 || pSiS->Reflect == 3) {

       *x = width - oldx - pSiS->CursorW;
       *y = height - oldy - pSiS->CursorH;

    } else if(pSiS->Reflect == 1) {

       *x = width - oldx - pSiS->CursorW;

    } else if(pSiS->Reflect == 2) {

       *y = height - oldy - pSiS->CursorH;

    }

}
#endif

/*******************************************/
/*   Old series (5597/5598/6326/530/620)   */
/*******************************************/

static Bool
SiSUseHWCursor(ScreenPtr pScreen, CursorPtr pCurs)
{
    ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
    SISPtr pSiS = SISPTR(pScrn);

    /* TODO: Doublescan? Interlace? */

    if(pSiS->sis6326tvumode)
       return FALSE;

    return TRUE;
}

static void
SiSHideCursor(ScrnInfoPtr pScrn)
{
    SISPtr pSiS = SISPTR(pScrn);
    UChar  sridx, cridx;

    /* Beware: This is executed asynchronously. */

    sridx = inSISREG(SISSR); cridx = inSISREG(SISCR);

#ifdef UNLOCK_ALWAYS
    sisSaveUnlockExtRegisterLock(pSiS, NULL, NULL);
#endif

    andSISIDXREG(SISSR, 0x06, 0xBF);

    outSISREG(SISSR, sridx); outSISREG(SISCR, cridx);
}

static void
SiSShowCursor(ScrnInfoPtr pScrn)
{
    SISPtr pSiS = SISPTR(pScrn);
    UChar  sridx, cridx;

    /* Beware: This is executed asynchronously. */

    sridx = inSISREG(SISSR); cridx = inSISREG(SISCR);

#ifdef UNLOCK_ALWAYS
    sisSaveUnlockExtRegisterLock(pSiS, NULL, NULL);
#endif

    orSISIDXREG(SISSR, 0x06, 0x40);

    outSISREG(SISSR, sridx); outSISREG(SISCR, cridx);
}

static void
SiSSetCursorPosition(ScrnInfoPtr pScrn, int x, int y)
{
    SISPtr pSiS = SISPTR(pScrn);
    DisplayModePtr mode = pSiS->CurrentLayout.mode;
    UChar  x_preset = 0;
    UChar  y_preset = 0;
    UChar  sridx, cridx;

    /* Beware: This is executed asynchronously. */
#ifdef SIS_INCL_RRROT
    SiSGetRealCursorPos(pSiS, &x, &y, 0);
#endif

    sridx = inSISREG(SISSR); cridx = inSISREG(SISCR);

#ifdef UNLOCK_ALWAYS
    sisSaveUnlockExtRegisterLock(pSiS, NULL, NULL);
#endif

    if(mode->Flags & V_INTERLACE)     y /= 2;
    else if(mode->Flags & V_DBLSCAN)  y *= 2;

    if(x < 0) {
       x_preset = (-x);
       if(x_preset > 63) x_preset = 63;
       x = 0;
    }

    if(y < 0) {
       y_preset = (-y);
       if(y_preset > 63) y_preset = 63;
       y = 0;
    }

    outSISIDXREG(SISSR, 0x1A, (x & 0xff));
    outSISIDXREG(SISSR, 0x1B, ((x >> 8) & 0xff));

    outSISIDXREG(SISSR, 0x1D, (y & 0xff));
    setSISIDXREG(SISSR, 0x1E, 0xF8, ((y >> 8) & 0x07));

    outSISIDXREG(SISSR, 0x1C, x_preset);
    outSISIDXREG(SISSR, 0x1F, y_preset);

    outSISREG(SISSR, sridx); outSISREG(SISCR, cridx);
}

static void
SiSSetCursorColors(ScrnInfoPtr pScrn, int bg, int fg)
{
    SISPtr pSiS = SISPTR(pScrn);
    UChar  sridx, cridx;

    /* Beware: This is executed asynchronously. */

    sridx = inSISREG(SISSR); cridx = inSISREG(SISCR);

#ifdef UNLOCK_ALWAYS
    sisSaveUnlockExtRegisterLock(pSiS, NULL, NULL);
#endif

    outSISIDXREG(SISSR, 0x14, ((bg & 0x00FF0000) >> (16+2)));
    outSISIDXREG(SISSR, 0x15, ((bg & 0x0000FF00) >> (8+2)));
    outSISIDXREG(SISSR, 0x16, ((bg & 0x000000FF) >> 2));
    outSISIDXREG(SISSR, 0x17, ((fg & 0x00FF0000) >> (16+2)));
    outSISIDXREG(SISSR, 0x18, ((fg & 0x0000FF00) >> (8+2)));
    outSISIDXREG(SISSR, 0x19, ((fg & 0x000000FF) >> 2));

    outSISREG(SISSR, sridx); outSISREG(SISCR, cridx);
}

static void
SiSLoadCursorImage(ScrnInfoPtr pScrn, UChar *src)
{
    SISPtr pSiS = SISPTR(pScrn);
    DisplayModePtr mode = pSiS->CurrentLayout.mode;
    ULong  cursor_addr;
    UChar  sridx, cridx;
    Bool doublesize = FALSE;

    /* Beware: This is executed asynchronously. */

    sridx = inSISREG(SISSR); cridx = inSISREG(SISCR);

#ifdef UNLOCK_ALWAYS
    sisSaveUnlockExtRegisterLock(pSiS, NULL, NULL);
#endif

    cursor_addr = pScrn->videoRam - 1;

    if(mode->Flags & V_DBLSCAN) doublesize = TRUE;

    SiSUploadMonoCursor(pSiS, doublesize, src, (UChar *)pSiS->RealFbBase + (cursor_addr * 1024));

    pSiS->CursorW = 64;
    pSiS->CursorH = doublesize ? 32 : 64;

    /* copy D[21:18] into the top bits of SR38 */
    setSISIDXREG(SISSR, 0x38, 0x0f, ((cursor_addr & 0xF00) >> 4));

    if(pSiS->Chipset == PCI_CHIP_SIS530) {
       /* store the D[22] to SR3E */
       if(cursor_addr & 0x1000) {
          orSISIDXREG(SISSR, 0x3E, 0x04);
       } else {
          andSISIDXREG(SISSR, 0x3E, ~0x04);
       }
    }

    /* set HW cursor pattern, use pattern 0xF */
    /* disable the hardware cursor side pattern */
    setSISIDXREG(SISSR, 0x1E, 0xF7, 0xF0);

    outSISREG(SISSR, sridx); outSISREG(SISCR, cridx);
}

/*******************************************/
/*     Common for 300 series and later     */
/*******************************************/

static Bool
SiSNewUseHWCursor(ScreenPtr pScreen, CursorPtr pCurs)
{
    ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
    SISPtr pSiS = SISPTR(pScrn);
    int size = pCurs->bits->height;

    /* Beware: This is executed asynchronously. */

    if(pSiS->MiscFlags & MISC_NOMONOHWCURSOR)
       return FALSE;

    if(pSiS->MiscFlags & MISC_CURSORMAXHALF) {
#ifdef SIS_INCL_RRROT
       if(pSiS->Rotate || pSiS->RRRotate & (RR_Rotate_90 | RR_Rotate_270))
          size = pCurs->bits->width;
#endif
       if(size > 32)
	  return FALSE;
    }

#ifdef SISMERGED
    pSiS->CurHotX = pCurs->bits->xhot;
    pSiS->CurHotY = pCurs->bits->yhot;
#endif

    return TRUE;
}

#if (XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,99,0,0)) && defined(ARGB_CURSOR) && defined(SIS_ARGB_CURSOR)
static Bool
SiSUseHWCursorARGB(ScreenPtr pScreen, CursorPtr pCurs)
{
    ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
    SISPtr pSiS = SISPTR(pScrn);
    int size = pCurs->bits->height;
    int maxsize;

    /* Beware: This is executed asynchronously. */

    if(pSiS->MiscFlags & MISC_NORGBHWCURSOR)
       return FALSE;

    switch(pSiS->VGAEngine) {
       case SIS_300_VGA:
	  maxsize = 32;
	  break;
       case SIS_315_VGA:
	  maxsize = 64;
	  break;
       default:
	  return FALSE;
    }

    if((size > maxsize) || (pCurs->bits->width > maxsize))
       return FALSE;

    if(pSiS->MiscFlags & MISC_CURSORMAXHALF) {
#ifdef SIS_INCL_RRROT
       if(pSiS->Rotate || pSiS->RRRotate & (RR_Rotate_90 | RR_Rotate_270))
          size = pCurs->bits->width;
#endif
       if(size > (maxsize / 2))
          return FALSE;
    }

#ifdef SISMERGED
    pSiS->CurHotX = pCurs->bits->xhot;
    pSiS->CurHotY = pCurs->bits->yhot;
#endif

    return TRUE;
}
#endif

#ifdef SISMERGED
#ifdef SIS_INCL_RRROT
static int
SiSGetHotspotOffset(SISPtr pSiS, int max)
{

    if(pSiS->RRRotate == RR_Rotate_90 || pSiS->Rotate == 1) {

       return pSiS->CurHotX;

    } else if(pSiS->RRRotate == RR_Rotate_270 || pSiS->Rotate == -1) {

       return max - pSiS->CurHotX;

    } else if(pSiS->RRRotate == RR_Rotate_180 || pSiS->Reflect >= 2) {

       return max - pSiS->CurHotY;

    }

    return pSiS->CurHotY;
}
#endif

static void
SiSSetCursorPositionMerged(ScrnInfoPtr pScrn1, int x, int y)
{
    SISPtr  pSiS = SISPTR(pScrn1);
    ScrnInfoPtr    pScrn2 = pSiS->CRT2pScrn;
    DisplayModePtr mode1 = CDMPTR->CRT1;
    DisplayModePtr mode2 = CDMPTR->CRT2;
    UShort  x1_preset = 0, x2_preset = 0;
    UShort  y1_preset = 0, y2_preset = 0;
    UShort  maxpreset;
    int     x1, y1, x2, y2;
    Bool    curoff1 = FALSE, curoff2 = FALSE;
    int     v1display = mode1->VDisplay;
    int     v2display = mode2->VDisplay;

    /* Beware: This is executed asynchronously. */

    x += pScrn1->frameX0;
    y += pScrn1->frameY0;

    x1 = x - pSiS->CRT1frameX0;
    y1 = y - pSiS->CRT1frameY0;

    x2 = x - pScrn2->frameX0;
    y2 = y - pScrn2->frameY0;
#ifdef SIS_INCL_RRROT
    SiSGetRealCursorPos(pSiS, &x1, &y1, 1);
    SiSGetRealCursorPos(pSiS, &x2, &y2, 2);
#endif

    maxpreset = 63;
    if((pSiS->VGAEngine == SIS_300_VGA) && (pSiS->UseHWARGBCursor)) maxpreset = 31;

    if(mode1->Flags & V_DBLSCAN) {
       y1 *= 2;
       v1display *= 2;
       if((pSiS->MiscFlags & MISC_CURSORMAXHALF) && (!(pSiS->MiscFlags & MISC_CURSORDOUBLESIZE))) {
#ifdef SIS_INCL_RRROT
	  y1 += SiSGetHotspotOffset(pSiS, maxpreset + 1);
#else
	  y1 += pSiS->CurHotY;
#endif
       }
    }
    if(mode2->Flags & V_DBLSCAN) {
       y2 *= 2;
       v2display *= 2;
       if((pSiS->MiscFlags & MISC_CURSORMAXHALF) && (!(pSiS->MiscFlags & MISC_CURSORDOUBLESIZE))) {
#ifdef SIS_INCL_RRROT
	  y2 += SiSGetHotspotOffset(pSiS, maxpreset + 1);
#else
	  y2 += pSiS->CurHotY;
#endif
       }
    }

    if(x1 < 0) {
       x1_preset = (-x1);
       if(x1_preset > maxpreset) curoff1 = TRUE;
       x1 = 0;
    }
    if(y1 < 0) {
       y1_preset = (-y1);
       if(y1_preset > maxpreset) curoff1 = TRUE;
       y1 = 0;
    }
    if(x2 < 0) {
       x2_preset = (-x2);
       if(x2_preset > maxpreset) curoff2 = TRUE;
       x2 = 0;
    }
    if(y2 < 0) {
       y2_preset = (-y2);
       if(y2_preset > maxpreset) curoff2 = TRUE;
       y2 = 0;
    }

    /* Work around bug in cursor engine if y or x > display */
    if(curoff1 || (y1 > v1display) || (x1 > mode1->HDisplay)) {
       y1 = 2000; y1_preset = 0;
    } else if(pSiS->MiscFlags & MISC_INTERLACE) {
       y1 /= 2; /* Not preset! */
    }

    /* CRT2 is never interlace */
    if(curoff2 || (y2 > v2display) || (x2 > mode2->HDisplay)) {
       y2 = 2000; y2_preset = 0;
    }

    if(pSiS->VGAEngine == SIS_300_VGA) {
       sis300SetCursorPositionX(x1, x1_preset)
       sis300SetCursorPositionY(y1, y1_preset)
       sis301SetCursorPositionX(x2 + 13, x2_preset)
       sis301SetCursorPositionY(y2, y2_preset)
    } else {
       sis310SetCursorPositionX(x1, x1_preset)
       sis310SetCursorPositionY(y1, y1_preset)
       sis301SetCursorPositionX310(x2 + 17, x2_preset)
       sis301SetCursorPositionY310(y2, y2_preset)
    }
}
#endif

/*******************************************/
/*                300 series               */
/*******************************************/

static void
SiS300HideCursor(ScrnInfoPtr pScrn)
{
    SISPtr pSiS = SISPTR(pScrn);

    /* Beware: This is executed asynchronously. */

#ifdef SISDUALHEAD
    if(!pSiS->DualHeadMode || pSiS->SecondHead || pSiS->ForceCursorOff) {
#endif
       sis300DisableHWCursor()
       sis300SetCursorPositionY(2000, 0)
#ifdef SISDUALHEAD
    }
    if(!pSiS->DualHeadMode || !pSiS->SecondHead || pSiS->ForceCursorOff) {
#endif
       if(pSiS->VBFlags & CRT2_ENABLE) {
          sis301DisableHWCursor()
	  sis301SetCursorPositionY(2000, 0)
       }
#ifdef SISDUALHEAD
    }
#endif
}

static void
SiS300ShowCursor(ScrnInfoPtr pScrn)
{
    SISPtr pSiS = SISPTR(pScrn);

    /* Beware: This is executed asynchronously. */

#ifdef SISDUALHEAD
    if(!pSiS->DualHeadMode || pSiS->SecondHead) {
#endif
       if(pSiS->UseHWARGBCursor) {
	  sis300EnableHWARGBCursor()
       } else {
	  sis300EnableHWCursor()
       }
#ifdef SISDUALHEAD
    }
    if(!pSiS->DualHeadMode || !pSiS->SecondHead) {
#endif
       if(pSiS->VBFlags & CRT2_ENABLE) {
          if(pSiS->UseHWARGBCursor) {
	     sis301EnableHWARGBCursor()
	  } else {
	     sis301EnableHWCursor()
	  }
       }
#ifdef SISDUALHEAD
    }
#endif
}

static void
SiS300SetCursorPosition(ScrnInfoPtr pScrn, int x, int y)
{
    SISPtr pSiS = SISPTR(pScrn);
    DisplayModePtr mode = pSiS->CurrentLayout.mode;
    UShort x_preset = 0;
    UShort y_preset = 0;
    UShort maxpreset = (pSiS->UseHWARGBCursor) ? 31 : 63;

    /* Beware: This is executed asynchronously. */

#ifdef SISMERGED
    if(pSiS->MergedFB) {
       SiSSetCursorPositionMerged(pScrn, x, y);
       return;
    }
#endif

#ifdef SIS_INCL_RRROT
    SiSGetRealCursorPos(pSiS, &x, &y, 0);
#endif

    if(mode->Flags & V_DBLSCAN) y *= 2;
    /* Cursor engine does not support interlace */

    if(x < 0) {
       x_preset = (-x);
       if(x_preset > maxpreset) x_preset = maxpreset;
       x = 0;
    }
    if(y < 0) {
       y_preset = (-y);
       if(y_preset > maxpreset) y_preset = maxpreset;
       y = 0;
    }

#ifdef SISDUALHEAD
    if(!pSiS->DualHeadMode || pSiS->SecondHead) {
#endif
       sis300SetCursorPositionX(x, x_preset)
       sis300SetCursorPositionY(y, y_preset)
#ifdef SISDUALHEAD
    }
    if(!pSiS->DualHeadMode || !pSiS->SecondHead) {
#endif
       if(pSiS->VBFlags & CRT2_ENABLE) {
	  sis301SetCursorPositionX(x + 13, x_preset)
	  sis301SetCursorPositionY(y, y_preset)
       }
#ifdef SISDUALHEAD
    }
#endif
}

static void
SiS300SetCursorColors(ScrnInfoPtr pScrn, int bg, int fg)
{
    SISPtr pSiS = SISPTR(pScrn);

    /* Beware: This is executed asynchronously. */

    if(pSiS->UseHWARGBCursor) return;

#ifdef SISDUALHEAD
    if(!pSiS->DualHeadMode || pSiS->SecondHead) {
#endif
       sis300SetCursorBGColor(bg)
       sis300SetCursorFGColor(fg)
#ifdef SISDUALHEAD
    }
    if(!pSiS->DualHeadMode || !pSiS->SecondHead) {
#endif
       if(pSiS->VBFlags & CRT2_ENABLE) {
          sis301SetCursorBGColor(bg)
          sis301SetCursorFGColor(fg)
       }
#ifdef SISDUALHEAD
    }
#endif
}

static void
SiS300LoadCursorImage(ScrnInfoPtr pScrn, UChar *src)
{
    SISPtr pSiS = SISPTR(pScrn);
    ULong  cursor_addr;
    CARD32 status1 = 0, status2 = 0;
    UChar  *dest = pSiS->RealFbBase;
    Bool   sizedouble = FALSE;
#ifdef SISDUALHEAD
    SISEntPtr pSiSEnt = pSiS->entityPrivate;
#endif

    /* Beware: This is executed asynchronously. */

    if(pSiS->MiscFlags & MISC_CURSORDOUBLESIZE)
       sizedouble = TRUE;

    cursor_addr = pScrn->videoRam - pSiS->cursorOffset - (pSiS->CursorSize/1024);  /* 1K boundary */

#ifdef SISDUALHEAD
    /* Use the global FbBase in DHM */
    if(pSiS->DualHeadMode) dest = pSiSEnt->RealFbBase;
#endif

    SiSUploadMonoCursor(pSiS, sizedouble, src, (UChar *)dest + (cursor_addr * 1024));

    pSiS->CursorW = 64;
    pSiS->CursorH = sizedouble ? 32 : 64;

#ifdef SISDUALHEAD
    if(pSiS->DualHeadMode) {
       UpdateHWCursorStatus(pSiS);
    }
#endif

    if(pSiS->UseHWARGBCursor) {
       if(pSiS->VBFlags & DISPTYPE_CRT1) {
	  status1 = sis300GetCursorStatus;
	  sis300DisableHWCursor()
	  if(pSiS->VBFlags & CRT2_ENABLE) {
	     status2 = sis301GetCursorStatus;
	     sis301DisableHWCursor()
	  }
	  SISWaitRetraceCRT1(pScrn);
	  sis300SwitchToMONOCursor();
	  if(pSiS->VBFlags & CRT2_ENABLE) {
	     SISWaitRetraceCRT2(pScrn);
	     sis301SwitchToMONOCursor();
	  }
       }
    }
    sis300SetCursorAddress(cursor_addr);
    if(status1) {
       sis300SetCursorStatus(status1)
    }

    if(pSiS->VBFlags & CRT2_ENABLE) {
       if((pSiS->UseHWARGBCursor) && (!pSiS->VBFlags & DISPTYPE_CRT1)) {
	  status2 = sis301GetCursorStatus;
	  sis301DisableHWCursor()
	  SISWaitRetraceCRT2(pScrn);
	  sis301SwitchToMONOCursor();
       }
       sis301SetCursorAddress(cursor_addr)
       if(status2) {
          sis301SetCursorStatus(status2)
       }
    }

    pSiS->UseHWARGBCursor = FALSE;
}

#if (XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,99,0,0)) && defined(ARGB_CURSOR) && defined(SIS_ARGB_CURSOR)
static void
SiS300LoadCursorImageARGB(ScrnInfoPtr pScrn, CursorPtr pCurs)
{
    SISPtr pSiS = SISPTR(pScrn);
    int cursor_addr, i, j, maxheight = 32;
    CARD32 *src = pCurs->bits->argb, *p;
    CARD32 *pb, *dest, *finaldest;
#define MYSISPTRTYPE CARD32
    int srcwidth = pCurs->bits->width;
    int srcheight = pCurs->bits->height;
    CARD32 temp, status1 = 0, status2 = 0;
    Bool sizedouble = FALSE;
#ifdef SIS_INCL_RRROT
    Bool isrot = FALSE;
#endif
#ifdef SISDUALHEAD
    SISEntPtr pSiSEnt = pSiS->entityPrivate;
#endif

    /* Beware: This is executed asynchronously. */

    if(pSiS->MiscFlags & MISC_CURSORDOUBLESIZE)
       sizedouble = TRUE;

#ifdef SIS_INCL_RRROT
    if(pSiS->Rotate || pSiS->RRRotate & (RR_Rotate_90 | RR_Rotate_270))
       isrot = TRUE;
#endif

    cursor_addr = pScrn->videoRam - pSiS->cursorOffset - ((pSiS->CursorSize/1024) * 2);

    if(srcwidth > 32)  srcwidth = 32;
    if(srcheight > 32) srcheight = 32;

#ifdef SISDUALHEAD
    if (pSiS->DualHeadMode)
	finaldest = (MYSISPTRTYPE *)((UChar *)pSiSEnt->RealFbBase + (cursor_addr * 1024));
    else
#endif
        finaldest = (MYSISPTRTYPE *)((UChar *)pSiS->RealFbBase + (cursor_addr * 1024));

    if(sizedouble) {
#ifdef SIS_INCL_RRROT
       if(isrot) {
          if(srcwidth > 16) srcwidth = 16;
       } else {
#endif
          if(srcheight > 16) srcheight = 16;
          maxheight = 16;
#ifdef SIS_INCL_RRROT
       }
#endif
    }

    dest = (CARD32 *)pSiS->CursorScratch;
    for(i = 0; i < srcheight; i++) {
       p = src;
       pb = dest;
       src += pCurs->bits->width;
       for(j = 0; j < srcwidth; j++) {
          temp = *p++;
          if(pSiS->OptUseColorCursorBlend) {
             if(temp & 0xffffff) {
                if((temp & 0xff000000) > pSiS->OptColorCursorBlendThreshold) {
		   temp &= 0x00ffffff;
		} else {
		   temp = 0xff111111;
		}
	     } else temp = 0xff000000;
	  } else {
	     if(temp & 0xffffff) temp &= 0x00ffffff;
	     else temp = 0xff000000;
	  }
	  sisfbwritelinc(dest, temp); /* *dest++ = temp; */
       }
       if(srcwidth < 32) {
          for(; j < 32; j++) {
	     sisfbwritelinc(dest, 0xff000000); /* *dest++ = 0xff000000; */
	  }
       }
#ifdef SIS_INCL_RRROT
       if(sizedouble && !isrot) {
#else
       if(sizedouble) {
#endif
          for(j = 0; j < 32; j++) {
             sisfbwritelinc(dest, sisfbreadlinc(pb)); /* *dest++ = *pb++; */
          }
       }
    }

    if(srcheight < maxheight) {
       for(; i < maxheight; i++) {
          for(j = 0; j < 32; j++) {
	     sisfbwritelinc(dest, 0xff000000); /* *dest++ = 0xff000000; */
	  }
#ifdef SIS_INCL_RRROT
	  if(sizedouble && !isrot) {
#else
	  if(sizedouble) {
#endif
	     for(j = 0; j < 32; j++) {
	        sisfbwritelinc(dest, 0xff000000); /* *dest++ = 0xff000000; */
	     }
	  }
       }
    }

    SiSUploadColorCursor(pSiS, sizedouble, pSiS->CursorScratch, finaldest, 32);

    pSiS->CursorW = 32;
    pSiS->CursorH = sizedouble ? 16 : 32;

#ifdef SISDUALHEAD
    if(pSiS->DualHeadMode) {
       UpdateHWCursorStatus(pSiS);
    }
#endif

    if(!pSiS->UseHWARGBCursor) {
       if(pSiS->VBFlags & DISPTYPE_CRT1) {
	  status1 = sis300GetCursorStatus;
	  sis300DisableHWCursor()
	  if(pSiS->VBFlags & CRT2_ENABLE) {
	     status2 = sis301GetCursorStatus;
	     sis301DisableHWCursor()
	  }
	  SISWaitRetraceCRT1(pScrn);
	  sis300SwitchToRGBCursor();
	  if(pSiS->VBFlags & CRT2_ENABLE) {
	     SISWaitRetraceCRT2(pScrn);
	     sis301SwitchToRGBCursor();
	  }
       }
    }

    sis300SetCursorAddress(cursor_addr);
    if(status1) {
       sis300SetCursorStatus(status1)
    }

    if(pSiS->VBFlags & CRT2_ENABLE) {
       if((!pSiS->UseHWARGBCursor) && (!(pSiS->VBFlags & DISPTYPE_CRT1))) {
	  status2 = sis301GetCursorStatus;
	  sis301DisableHWCursor()
	  SISWaitRetraceCRT2(pScrn);
	  sis301SwitchToRGBCursor();
       }
       sis301SetCursorAddress(cursor_addr)
       if(status2) {
          sis301SetCursorStatus(status2)
       }
    }

    pSiS->UseHWARGBCursor = TRUE;
}
#endif

/*******************************************/
/*              315 and later              */
/*******************************************/

static void
SiS310HideCursor(ScrnInfoPtr pScrn)
{
    SISPtr pSiS = SISPTR(pScrn);

    /* Beware: This is executed asynchronously. */

    pSiS->HWCursorIsVisible = FALSE;

#ifdef SISDUALHEAD
    if(!pSiS->DualHeadMode || pSiS->SecondHead || pSiS->ForceCursorOff) {
#endif
       sis310DisableHWCursor()
       sis310SetCursorPositionY(2000, 0)
#ifdef SISDUALHEAD
    }
    if(!pSiS->DualHeadMode || !pSiS->SecondHead || pSiS->ForceCursorOff) {
#endif
       if(pSiS->VBFlags2 & VB2_VIDEOBRIDGE) {
	  sis301DisableHWCursor310()
	  sis301SetCursorPositionY310(2000, 0)
       }
#ifdef SISDUALHEAD
    }
#endif
}

static void
SiS310ShowCursor(ScrnInfoPtr pScrn)
{
    SISPtr  pSiS = SISPTR(pScrn);

    /* Beware: This is executed asynchronously. */

    if(pSiS->HideHWCursor) {
       SiS310HideCursor(pScrn);
       /* sic! And AFTER HideCursor()! */
       pSiS->HWCursorIsVisible = TRUE;
       return;
    }

    pSiS->HWCursorIsVisible = TRUE;

#ifdef SISDUALHEAD
    if(!pSiS->DualHeadMode || pSiS->SecondHead) {
#endif
       if(pSiS->UseHWARGBCursor) {
	  sis310EnableHWARGBCursor()
       } else {
	  sis310EnableHWCursor()
       }
#ifdef SISDUALHEAD
    }
    if(!pSiS->DualHeadMode || !pSiS->SecondHead) {
#endif
       if(pSiS->VBFlags & CRT2_ENABLE) {
          if(pSiS->ChipFlags & SiSCF_CRT2HWCKaputt) {
	     sis301EnableHWCursor330()
	  } else {
	     if(pSiS->UseHWARGBCursor) {
	        sis301EnableHWARGBCursor310()
	     } else {
	        sis301EnableHWCursor310()
	     }
	  }
       }
#ifdef SISDUALHEAD
    }
#endif
}

static void
SiS310SetCursorPosition(ScrnInfoPtr pScrn, int x, int y)
{
    SISPtr pSiS = SISPTR(pScrn);
    DisplayModePtr mode = pSiS->CurrentLayout.mode;
    UShort x_preset = 0;
    UShort y_preset = 0;
    int y1;

    /* Beware: This is executed asynchronously. */

#ifdef SISMERGED
    if(pSiS->MergedFB) {
       SiSSetCursorPositionMerged(pScrn, x, y);
       return;
    }
#endif

#ifdef SIS_INCL_RRROT
    SiSGetRealCursorPos(pSiS, &x, &y, 0);
#endif

    if(mode->Flags & V_DBLSCAN) y *= 2;

    if(x < 0) {
       x_preset = (-x);
       if(x_preset > 63) x_preset = 63;
       x = 0;
    }
    if(y < 0) {
       y_preset = (-y);
       if(y_preset > 63) y_preset = 63;
       y = 0;
    }

    y1 = y;
    if(pSiS->MiscFlags & MISC_INTERLACE) y1 /= 2;

#ifdef SISDUALHEAD
    if(!pSiS->DualHeadMode || pSiS->SecondHead) {
#endif
       sis310SetCursorPositionX(x, x_preset)
       sis310SetCursorPositionY(y1, y_preset)
#ifdef SISDUALHEAD
    }
    if(!pSiS->DualHeadMode || !pSiS->SecondHead) {
#endif
       if(pSiS->VBFlags & CRT2_ENABLE) {
          sis301SetCursorPositionX310(x + 17, x_preset)
          sis301SetCursorPositionY310(y, y_preset)
       }
#ifdef SISDUALHEAD
    }
#endif
}

static void
SiS310SetCursorColors(ScrnInfoPtr pScrn, int bg, int fg)
{
    SISPtr pSiS = SISPTR(pScrn);

    /* Beware: This is executed asynchronously. */

    if(pSiS->UseHWARGBCursor) return;

#ifdef SISDUALHEAD
    if(!pSiS->DualHeadMode || pSiS->SecondHead) {
#endif
       sis310SetCursorBGColor(bg)
       sis310SetCursorFGColor(fg)
#ifdef SISDUALHEAD
    }
    if(!pSiS->DualHeadMode || !pSiS->SecondHead) {
#endif
       if(pSiS->VBFlags & CRT2_ENABLE) {
	  if(pSiS->ChipFlags & SiSCF_CRT2HWCKaputt) {
	     if((fg != pSiS->CurFGCol) || (bg != pSiS->CurBGCol)) {
	        pSiS->CurFGCol = fg;
	        pSiS->CurBGCol = bg;
	        SiSXConvertMono2ARGB(pSiS);
	     }
	  } else {
	     sis301SetCursorBGColor310(bg)
	     sis301SetCursorFGColor310(fg)
	  }
       }
#ifdef SISDUALHEAD
    }
#endif
}

static void
SiS310LoadCursorImage(ScrnInfoPtr pScrn, UChar *src)
{
    SISPtr pSiS = SISPTR(pScrn);
    ULong cursor_addr, cursor_addr2 = 0;
    CARD32 status1 = 0, status2 = 0;
    UChar *dest = pSiS->RealFbBase;
    Bool  sizedouble = FALSE;
    int bufnum;
#ifdef SISDUALHEAD
    SISEntPtr pSiSEnt = pSiS->entityPrivate;

    /* Beware: This is executed asynchronously. */

    if(pSiS->DualHeadMode) {
       pSiSEnt->HWCursorMBufNum ^= 1;
       bufnum = 1 << pSiSEnt->HWCursorMBufNum;
    } else {
#endif
       pSiS->HWCursorMBufNum ^= 1;
       bufnum = 1 << pSiS->HWCursorMBufNum;
#ifdef SISDUALHEAD
    }
#endif

    if(pSiS->MiscFlags & MISC_CURSORDOUBLESIZE)
       sizedouble = TRUE;

#ifdef SISDUALHEAD
    /* Use the global FbBase in DHM */
    if(pSiS->DualHeadMode) dest = pSiSEnt->RealFbBase;
#endif

    if(pSiS->ChipFlags & SiSCF_CRT2HWCKaputt) {
       cursor_addr = pScrn->videoRam - pSiS->cursorOffset - (pSiS->CursorSize/1024);
    } else {
       cursor_addr = pScrn->videoRam - pSiS->cursorOffset - ((pSiS->CursorSize/1024) * bufnum);
    }

    SiSUploadMonoCursor(pSiS, sizedouble, src, (UChar *)dest + (cursor_addr * 1024));

    pSiS->CursorW = 64;
    pSiS->CursorH = sizedouble ? 32 : 64;

#ifdef SISDUALHEAD
    if(pSiS->DualHeadMode) {
       UpdateHWCursorStatus(pSiS);
    }
#endif

    if(pSiS->ChipFlags & SiSCF_CRT2HWCKaputt) {

       /* Convert Mono image to color image */
       cursor_addr2 = pScrn->videoRam - pSiS->cursorOffset - ((pSiS->CursorSize/1024) * 2);

       pSiS->CurMonoSrc = (UChar *)dest + (cursor_addr * 1024);
       pSiS->CurARGBDest = (CARD32 *)((UChar *)dest + (cursor_addr2 * 1024));
       pSiS->CursorDoubleSize = sizedouble;

       SiSXConvertMono2ARGB(pSiS);

       if(pSiS->UseHWARGBCursor) {
	  if(pSiS->VBFlags & DISPTYPE_CRT1) {
	     status1 = sis310GetCursorStatus;
	     sis310DisableHWCursor();
	     SISWaitRetraceCRT1(pScrn);
	     sis310SwitchToMONOCursor();
	  }
       }

    } else {

       if(pSiS->UseHWARGBCursor) {
	  if(pSiS->VBFlags & DISPTYPE_CRT1) {
	     status1 = sis310GetCursorStatus;
	     sis310DisableHWCursor()
	     if(pSiS->VBFlags & CRT2_ENABLE) {
	        status2 = sis301GetCursorStatus310;
	        sis301DisableHWCursor310()
	     }
	     SISWaitRetraceCRT1(pScrn);
	     sis310SwitchToMONOCursor();
	     if(pSiS->VBFlags & CRT2_ENABLE)  {
	        SISWaitRetraceCRT2(pScrn);
	        sis301SwitchToMONOCursor310();
	     }
	  }
       } else if(pSiS->Chipset == PCI_CHIP_SIS315H) {
	  if(pSiS->VBFlags & DISPTYPE_CRT1) {
	     SISWaitRetraceCRT1(pScrn);
	  }
       }
    }

    sis310SetCursorAddress(cursor_addr);
    if(status1) {
       sis310SetCursorStatus(status1)
    }

    if(pSiS->VBFlags & CRT2_ENABLE) {
       if(pSiS->ChipFlags & SiSCF_CRT2HWCKaputt) {
          sis301SetCursorAddress310(cursor_addr2)
       } else {
	  if((pSiS->UseHWARGBCursor) && (!(pSiS->VBFlags & DISPTYPE_CRT1))) {
	     status2 = sis301GetCursorStatus310;
	     sis301DisableHWCursor310()
	     SISWaitRetraceCRT2(pScrn);
	     sis301SwitchToMONOCursor310();
	  }
	  sis301SetCursorAddress310(cursor_addr)
	  if(status2) {
	     sis301SetCursorStatus310(status2)
	  }
       }
    }

    pSiS->UseHWARGBCursor = FALSE;
}

#if (XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,99,0,0)) && defined(ARGB_CURSOR) && defined(SIS_ARGB_CURSOR)
static void
SiS310LoadCursorImageARGB(ScrnInfoPtr pScrn, CursorPtr pCurs)
{
    SISPtr pSiS = SISPTR(pScrn);
    int cursor_addr, i, j, maxheight = 64;
    CARD32 *src = pCurs->bits->argb, *p, *pb, *dest, *finaldest;
    int srcwidth = pCurs->bits->width;
    int srcheight = pCurs->bits->height;
    CARD32 status1 = 0, status2 = 0;
    Bool sizedouble = FALSE;
#ifdef SIS_INCL_RRROT
    Bool isrot = FALSE;
#endif
    int bufnum;
#ifdef SISDUALHEAD
    SISEntPtr pSiSEnt = pSiS->entityPrivate;
#endif

    /* Beware: This is executed asynchronously. */

    if(pSiS->MiscFlags & MISC_CURSORDOUBLESIZE)
       sizedouble = TRUE;

#ifdef SIS_INCL_RRROT
    if(pSiS->Rotate || pSiS->RRRotate & (RR_Rotate_90 | RR_Rotate_270))
       isrot = TRUE;
#endif

#ifdef SISDUALHEAD
    if(pSiS->DualHeadMode) {
       pSiSEnt->HWCursorCBufNum ^= 1;
       bufnum = 1 << pSiSEnt->HWCursorCBufNum;
    } else {
#endif
       pSiS->HWCursorCBufNum ^= 1;
       bufnum = 1 << pSiS->HWCursorCBufNum;
#ifdef SISDUALHEAD
    }
#endif

    if(pSiS->ChipFlags & SiSCF_CRT2HWCKaputt) {
       cursor_addr = pScrn->videoRam - pSiS->cursorOffset - ((pSiS->CursorSize/1024) * 2);
    } else {
       cursor_addr = pScrn->videoRam - pSiS->cursorOffset - ((pSiS->CursorSize/1024) * (2 + bufnum));
    }

    if(srcwidth > 64)  srcwidth = 64;
    if(srcheight > 64) srcheight = 64;

#ifdef SISDUALHEAD
    if(pSiS->DualHeadMode)
       finaldest = (CARD32 *)((UChar *)pSiSEnt->RealFbBase + (cursor_addr * 1024));
    else
#endif
       finaldest = (CARD32 *)((UChar *)pSiS->RealFbBase + (cursor_addr * 1024));

    if(sizedouble) {
#ifdef SIS_INCL_RRROT
       if(isrot) {
          if(srcwidth > 32) srcwidth = 32;
       } else {
#endif
          if(srcheight > 32) srcheight = 32;
          maxheight = 32;
#ifdef SIS_INCL_RRROT
       }
#endif
    }

    dest = (CARD32 *)pSiS->CursorScratch;
    for(i = 0; i < srcheight; i++) {
       p = src;
       pb = dest;
       src += pCurs->bits->width;
       for(j = 0; j < srcwidth; j++) sisfbwritelpinc(dest, p); /* *dest++ = *p++; */
       if(srcwidth < 64) {
          for(; j < 64; j++) sisfbwritelinc(dest, 0);  /* *dest++ = 0; */
       }
#ifdef SIS_INCL_RRROT
       if(sizedouble && !isrot) {
#else
       if(sizedouble) {
#endif
          for(j = 0; j < 64; j++) {
             sisfbwritelinc(dest, sisfbreadlinc(pb)); /* *dest++ = *pb++; */
          }
       }
    }
    if(srcheight < maxheight) {
       for(; i < maxheight; i++) {
	  for(j = 0; j < 64; j++) sisfbwritelinc(dest, 0); /* *dest++ = 0; */
#ifdef SIS_INCL_RRROT
	  if(sizedouble && !isrot) {
#else
	  if(sizedouble) {
#endif
	     for(j = 0; j < 64; j++) sisfbwritelinc(dest, 0); /* *dest++ = 0; */
	  }
       }
    }

    SiSUploadColorCursor(pSiS, sizedouble, pSiS->CursorScratch, finaldest, 64);

    pSiS->CursorW = 64;
    pSiS->CursorH = sizedouble ? 32 : 64;

#ifdef SISDUALHEAD
    if(pSiS->DualHeadMode) {
       UpdateHWCursorStatus(pSiS);
    }
#endif

    if(pSiS->ChipFlags & SiSCF_CRT2HWCKaputt) {
       if(!pSiS->UseHWARGBCursor) {
          if(pSiS->VBFlags & DISPTYPE_CRT1) {
	     status1 = sis310GetCursorStatus;
	     sis310DisableHWCursor()
	  }
	  SISWaitRetraceCRT1(pScrn);
	  sis310SwitchToRGBCursor();
       }
    } else {
       if(!pSiS->UseHWARGBCursor) {
          if(pSiS->VBFlags & DISPTYPE_CRT1) {
	     status1 = sis310GetCursorStatus;
	     sis310DisableHWCursor()
	     if(pSiS->VBFlags & CRT2_ENABLE) {
	        status2 = sis301GetCursorStatus310;
	        sis301DisableHWCursor310()
	     }
	  }
	  SISWaitRetraceCRT1(pScrn);
	  sis310SwitchToRGBCursor();
	  if(pSiS->VBFlags & CRT2_ENABLE)  {
	     SISWaitRetraceCRT2(pScrn);
	     sis301SwitchToRGBCursor310();
	  }
       }
    }

    sis310SetCursorAddress(cursor_addr);
    if(status1) {
       sis310SetCursorStatus(status1)
    }

    if(pSiS->VBFlags & CRT2_ENABLE) {
       if(pSiS->ChipFlags & SiSCF_CRT2HWCKaputt) {
          sis301SetCursorAddress310(cursor_addr)
       } else {
          if((!pSiS->UseHWARGBCursor) && (!(pSiS->VBFlags & DISPTYPE_CRT1))) {
	     status2 = sis301GetCursorStatus310;
	     sis301DisableHWCursor310()
	     SISWaitRetraceCRT2(pScrn);
	     sis301SwitchToRGBCursor310();
	  }
          sis301SetCursorAddress310(cursor_addr)
	  if(status2) {
	     sis301SetCursorStatus310(status2)
	  }
       }
    }

    pSiS->UseHWARGBCursor = TRUE;
}
#endif

/*******************************************/
/*                 Set up                  */
/*******************************************/

Bool
SiSHWCursorInit(ScreenPtr pScreen)
{
    ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
    SISPtr pSiS = SISPTR(pScrn);
    xf86CursorInfoPtr infoPtr;

    if(!(pSiS->CursorScratch = xcalloc(1, max(2048, pSiS->CursorSize))))
       return FALSE;

    if(!(infoPtr = xf86CreateCursorInfoRec())) {
       xfree(pSiS->CursorScratch);
       pSiS->CursorScratch = NULL;
       return FALSE;
    }

    pSiS->CursorInfoPtr = infoPtr;
    pSiS->UseHWARGBCursor = FALSE;

    switch(pSiS->VGAEngine)  {
      case SIS_300_VGA:
	infoPtr->MaxWidth  = 64;
	infoPtr->MaxHeight = 64;
	infoPtr->UseHWCursor = SiSNewUseHWCursor;
	infoPtr->ShowCursor = SiS300ShowCursor;
	infoPtr->HideCursor = SiS300HideCursor;
	infoPtr->SetCursorPosition = SiS300SetCursorPosition;
	infoPtr->SetCursorColors = SiS300SetCursorColors;
	infoPtr->LoadCursorImage = SiS300LoadCursorImage;
#if (XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,99,0,0)) && defined(ARGB_CURSOR) && defined(SIS_ARGB_CURSOR)
	if(pSiS->OptUseColorCursor) {
	   infoPtr->UseHWCursorARGB = SiSUseHWCursorARGB;
	   infoPtr->LoadCursorARGB = SiS300LoadCursorImageARGB;
	}
#endif
	infoPtr->Flags =
		HARDWARE_CURSOR_TRUECOLOR_AT_8BPP |
		HARDWARE_CURSOR_INVERT_MASK |
		HARDWARE_CURSOR_BIT_ORDER_MSBFIRST |
		HARDWARE_CURSOR_AND_SOURCE_WITH_MASK |
		HARDWARE_CURSOR_SWAP_SOURCE_AND_MASK |
		HARDWARE_CURSOR_SOURCE_MASK_INTERLEAVE_64;

	if(pSiS->ChipFlags & SiSCF_NoCurHide) {
	   infoPtr->Flags |= HARDWARE_CURSOR_UPDATE_UNHIDDEN;
	}
	break;

      case SIS_315_VGA:
	infoPtr->MaxWidth  = 64;
	infoPtr->MaxHeight = 64;
	infoPtr->UseHWCursor = SiSNewUseHWCursor;
	infoPtr->ShowCursor = SiS310ShowCursor;
	infoPtr->HideCursor = SiS310HideCursor;
	infoPtr->SetCursorPosition = SiS310SetCursorPosition;
	infoPtr->SetCursorColors = SiS310SetCursorColors;
	infoPtr->LoadCursorImage = SiS310LoadCursorImage;
#if (XF86_VERSION_CURRENT >= XF86_VERSION_NUMERIC(4,2,99,0,0)) && defined(ARGB_CURSOR) && defined(SIS_ARGB_CURSOR)
	if(pSiS->OptUseColorCursor) {
	   infoPtr->UseHWCursorARGB = SiSUseHWCursorARGB;
	   infoPtr->LoadCursorARGB = SiS310LoadCursorImageARGB;
	}
#endif
	infoPtr->Flags =
		HARDWARE_CURSOR_TRUECOLOR_AT_8BPP |
		HARDWARE_CURSOR_INVERT_MASK |
		HARDWARE_CURSOR_BIT_ORDER_MSBFIRST |
		HARDWARE_CURSOR_AND_SOURCE_WITH_MASK |
		HARDWARE_CURSOR_SWAP_SOURCE_AND_MASK |
		HARDWARE_CURSOR_SOURCE_MASK_INTERLEAVE_64;

	if(pSiS->ChipFlags & SiSCF_NoCurHide) {
	    infoPtr->Flags |= HARDWARE_CURSOR_UPDATE_UNHIDDEN;
	}
	break;

      default:
	infoPtr->MaxWidth  = 64;
	infoPtr->MaxHeight = 64;
	infoPtr->UseHWCursor = SiSUseHWCursor;
	infoPtr->SetCursorPosition = SiSSetCursorPosition;
	infoPtr->ShowCursor = SiSShowCursor;
	infoPtr->HideCursor = SiSHideCursor;
	infoPtr->SetCursorColors = SiSSetCursorColors;
	infoPtr->LoadCursorImage = SiSLoadCursorImage;
	infoPtr->Flags =
		HARDWARE_CURSOR_TRUECOLOR_AT_8BPP |
		HARDWARE_CURSOR_INVERT_MASK |
		HARDWARE_CURSOR_BIT_ORDER_MSBFIRST |
		HARDWARE_CURSOR_AND_SOURCE_WITH_MASK |
		HARDWARE_CURSOR_NIBBLE_SWAPPED |
		HARDWARE_CURSOR_SOURCE_MASK_INTERLEAVE_1;
	break;
    }

    return (xf86InitCursor(pScreen, infoPtr));
}


